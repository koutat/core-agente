# AGENTE FACTURACIÓN ELECTRONICA #


### INTRODUCCION ###

* El objetivo del proyecto es la emisión de comprobantes electrónicos al  Api obtenidos directamente de la base de datosde los  sistemas del cliente. para el funcionamiento de la aplicación es necesario tener todos los componentes debidamente instalados y configurados.
* 0.5
* [Repositorio core](https://gitlab.com/lenin.cruzado/Core-Agente)

### REQUISITOS ###

* NODE 4.4.7
* DBMARIA 5.6.7
* ELECTRON 1.2.5

### INSTALACION ###

* clonar el proyecto:
    `` git clone  https://gitlab.com/lenin.cruzado/Core-Agente'``
* eliminar la extension .dist a los archivos dentro de la carpeta config seguidamente ingresar los datos correspodientes
* descargar las dependencias:
    `` npm install``
* mapear la base de datos:
    ``npm run mapear``
 
### Empaquetar  y Generar Distribuibles ###
* Empaquetar todos los recursos en archivos asar:
    `` npm run package '``
* Generar Ejecutables y/o Instaladores
    `` npm run build ``
* Finalmente encotrara 3 archivos para la distrbucion respectiva)