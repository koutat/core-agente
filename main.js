const electron = require('electron');
// Module to control application life.
// const app = electron.app
const {app, Menu, Tray} = require('electron');
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;



// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;
let presentationWindow;
let childWindow;
let willQuitApp = false;

function createMainWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({width: 800, height: 600});
  // mainWindow.closable = false;
  // and load the index.html of the app.
  mainWindow.loadURL(`file://${__dirname}/index.html`);
  // mainWindow.hide();

  // Open the DevTools.
  mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  })
}
function createPresentationWindow () {
  presentationWindow = new BrowserWindow({width: 800, height:600, frame: false});
  presentationWindow.loadURL(`file://${__dirname}/public/views/presentation.html`);
  //presentationWindow.webContents.openDevTools();
  setTimeout(function(){
    presentationWindow.hide();
  }, 5000);
}
function createChildWindow () {

  // Create the browser window.
  childWindow = new BrowserWindow({width: 1024, height: 768});
  // mainWindow.closable = false;
  // and load the index.html of the app.
  childWindow.loadURL(`file://${__dirname}/public/views/logs.html`);
  //childWindow.loadURL("http://localhost:55555/logs");
  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  childWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    childWindow = null;
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
// app.on('ready', createMainWindow)
app.on('ready',function () {
  createMainWindow();
  createPresentationWindow();
  setTimeout(function(){
    createChildWindow();
  }, 5000);


  tray = new Tray(__dirname+'/public/images/tray_icon.png');
  const contextMenu = Menu.buildFromTemplate([
    {label: 'Salir', type: 'normal',role:"quit"}
  ]);
  tray.on('double-click',function () {
      if(childWindow === null)
          createChildWindow();
      else if(childWindow.isVisible())
            childWindow.hide();
      else
            childWindow.show();
  });
  tray.setToolTip('Agente Facturacion.');
  tray.setContextMenu(contextMenu);

});
// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
});

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
});


// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
