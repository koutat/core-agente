/**
 * Created by Luis on 23/05/2016.
 */
var SignedXml = require("xml-crypto").SignedXml;
var select    = require("xml-crypto").xpath;
var dom       = require('xmldom').DOMParser
var forge     = require('node-forge');
var pki       = forge.pki;


/**
 * [FileKeyInfoSunat Implementacion del campo X509Data segun UBL , importante para la libreria node XMl-CRYPTO]
 * @param {String} file [Certificado publico en formato PEM]
 */

function FileKeyInfoSunat(file) {

    this.file = file;

    this.getKeyInfo = function (key, prefix) {

        var prefix = prefix || '';
        prefix     = prefix ? prefix + ":" : prefix;

        var certBodyInB64 = forge.util.encode64(forge.pem.decode(this.file)[0].body);
        var certObj       = pki.certificateFromPem(this.file);
        var xml           = "";
        xml += "<" + prefix + "X509Data>";
        xml += '<' + prefix + 'X509SubjectName>';
        xml += getSubjectName(certObj);
        xml += '</' + prefix + 'X509SubjectName>';
        xml += "<" + prefix + "X509Certificate>";
        xml += certBodyInB64;
        xml += "</" + prefix + "X509Certificate>";
        xml += "</" + prefix + "X509Data>";
        return xml;

    };

    this.getKey = function (keyInfo) {
        return file;
    };

    function getSubjectName(certObj) {
        var subjectFields,
            fields = ['CN', 'OU', 'O', 'L', 'ST', 'C'];

        if (certObj.subject) {
            subjectFields = fields.reduce(function (subjects, fieldName) {
                var certAttr = certObj.subject.getField(fieldName);

                if (certAttr) {
                    subjects.push(fieldName + '=' + certAttr.value);
                }

                return subjects;
            }, []);
        }

        return Array.isArray(subjectFields) ? subjectFields.join(',') : '';
    }
}

/**
 * [Signer Encargado de firmar el documento XML]
 * @param {String} certificado [Certificado publico en formato PEM]
 * @param {String} privateKey  [LLave privada en formato PEM]
 */
function Signer(certificado, privateKey) {
    this.certificado = certificado;
    this.privateKey  = privateKey;
}

Signer.prototype.signXml = function (xml) {

    var sig             = new SignedXml();
    sig.keyInfoProvider = new FileKeyInfoSunat(this.certificado);
    sig.signingKey      = this.privateKey;

    //var xpathReference = util.format( "//*[local-name(.)='%s']" , roots[ this.tipoDocumento ] );

    sig.addReference("/*", ['http://www.w3.org/2000/09/xmldsig#enveloped-signature'], null, null, null, null, true);
    var fs = require('fs');
    fs.writeFileSync("nofirma.xml", xml, "utf-8");
    sig.computeSignature(xml,
        {
            prefix: "ds",
            location: {
                reference: "(//ext:UBLExtension//ext:ExtensionContent)[last()]",
                action: "append"
            },
            attrs: {
                "Id": "SignatureTM"
            }
        }
    );

    var doc       = new dom().parseFromString(sig.getSignatureXml());
    var xmlString = sig.getSignedXml();
    fs.writeFileSync("firma.xml", xmlString, "utf-8");
    var digestValue    = select(doc, "//ds:DigestValue/text()").toString();
    var signatureValue = select(doc, "//ds:SignatureValue/text()").toString();

    var xmlObject = {
        xmlString: xmlString,
        digestValue: digestValue,
        signatureValue: signatureValue
    };

    return xmlObject;
}

module.exports = Signer;