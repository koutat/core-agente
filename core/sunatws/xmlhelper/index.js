/**
 * Created by Luis on 23/05/2016.
 */
var ejs         = require("ejs");
var fs          = require("fs");
var parseString = require("xml2js").parseString;
var async       = require("async");
var Signer      = require("./signer");
var certificado = require("../../configuraciones").certConfig;
var pd          = require("pretty-data").pd;
// var parserXml   = require("./parser").xmlToComprobante;


var templates = {
    "factura" : fs.readFileSync(__dirname + '/xml-comprobantes-templates/factura.xml.ejs' , 'utf-8') ,
    "boleta" : fs.readFileSync(__dirname + '/xml-comprobantes-templates/boleta.xml.ejs' , 'utf-8') ,
    "notacredito" : fs.readFileSync(__dirname + '/xml-comprobantes-templates/notacredito.xml.ejs' , 'utf-8') ,
    "notadebito" : fs.readFileSync(__dirname + '/xml-comprobantes-templates/notadebito.xml.ejs' , 'utf-8') ,
    "resumenboleta" : fs.readFileSync(__dirname + '/xml-comprobantes-templates/resumenboleta.xml.ejs' , 'utf-8') ,
    "comunicacionbaja" : fs.readFileSync(__dirname + '/xml-comprobantes-templates/comunicacionbaja.xml.ejs' , 'utf-8')
};

/**
 * Servicio que construira el documento XML
 * @return {[type]} [description]
 */
function xmlhelper( documento , tipoDocumento ) {
}

/**
 *
 * @param documento Instancia de un comprobante
 * @param tipoDocumento
 * @param callback
 */
xmlhelper.prototype.buildXml = async.asyncify(function buildXmlWrapper( comprobante , emisor ) {
    var tipoComprobante = comprobante.get('tipoComprobante');
    var xml             = ejs.render(templates[ tipoComprobante ] , {
        comprobante : comprobante ,
        emisor : emisor
    } , { rmWhitespace : true });
    var signer          = new Signer(certificado.crt , certificado.key );
    var xmlObject       = signer.signXml(xml);
    return xmlObject;
});
/**
 *
 * @param xmlString XML en crudo
 * @param callback
 */
xmlhelper.prototype.parsearConstancia = function ( xmlString , callback ) {

    var constanciaData = {};
    parseString(xmlString , function ( err , xmlObj ) {
        if ( err ) {
            return callback(err , null);
        }

        constanciaData.idRespuesta  = xmlObj[ "ApplicationResponse" ][ "cbc:ID" ][ 0 ];
        constanciaData.fechaEmision = xmlObj[ "ApplicationResponse" ][ 'cbc:ResponseDate' ] + " " + xmlObj[ "ApplicationResponse" ][ 'cbc:ResponseTime' ]
        constanciaData.descripcion  = xmlObj[ "ApplicationResponse" ][ 'cac:DocumentResponse' ][ 0 ][ 'cac:Response' ][ 0 ][ 'cbc:Description' ][ 0 ];
        constanciaData.codigo       = xmlObj[ "ApplicationResponse" ][ 'cac:DocumentResponse' ][ 0 ][ 'cac:Response' ][ 0 ][ 'cbc:ResponseCode' ][ 0 ];

        var serieNumero       = xmlObj[ "ApplicationResponse" ][ 'cac:DocumentResponse' ][ 0 ][ 'cac:Response' ][ 0 ][ 'cbc:ReferenceID' ][ 0 ].split("-");
        constanciaData.serie  = serieNumero[ 0 ];
        constanciaData.numero = serieNumero[ 1 ];

        var notes            = xmlObj[ "ApplicationResponse" ][ 'cbc:Note' ];
        constanciaData.notas = "";

        if ( notes ) {
            constanciaData.notas = notes.join();
        }

        return callback(null , constanciaData);
    });
};

// xmlhelper.prototype.xmlToComprobante = parserXml;


module.exports = new xmlhelper();