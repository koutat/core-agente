var jsZip = require("jszip");
var async = require("async");


function  zipper() {};

var zipAsync = async.asyncify(function  zipWrapped(nombreBase,xmlString) {
	var nombreArchivo = nombreBase + ".xml";
	var zip           = new jsZip();
	zip.file(nombreArchivo,xmlString);
	var buffer        = zip.generate({compression: "DEFLATE"});
	return buffer;
});

zipper.prototype.zip = function(nombreBase,xmlString,callback) {
	zipAsync(nombreBase,xmlString,function  (err,buffer) {
		if(err){
			return callback(err);
		}
		return callback(null,buffer);
	});

};


var unzipAsync = async.asyncify(function  unzipWrapped(zipString) {
	var xml = new jsZip(zipString,{base64:true}).file(/^.*\.xml$/)[0].asText();
	return xml;
});

zipper.prototype.unzip = function(zipString,callback) {	
	unzipAsync(zipString,function  (err,xml) {
		if(err){
			return callback(err);
		}
		return callback(null,xml);
	});
};


module.exports = new zipper();