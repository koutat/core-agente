var moment = require( "moment" );
var async  = require( "async" );

var utils = {};

/// PORCETANJE IGV

utils.igvPercent = 0.18;
utils.igvValue   = 1.18;

// [MES,DIA]
var feriadosFijos =
	    [
		    "01-01" ,
		    "05-01" ,
		    "06-29" ,
		    "07-28" ,
		    "07-29" ,
		    "08-30" ,
		    "10-08" ,
		    "11-01" ,
		    "12-08" ,
		    "12-25"
	    ];

function getDateEaster( year ) {

	var a = year % 19
	var b = Math.floor( year / 100 )
	var c = year % 100
	var d = Math.floor( b / 4 )
	var e = b % 4
	var f = Math.floor( (b + 8) / 25 )
	var g = Math.floor( (b - f + 1) / 3 )
	var h = (19 * a + b - d - g + 15) % 30
	var i = Math.floor( c / 4 )
	var k = c % 4
	var l = (32 + 2 * e + 2 * i - h - k) % 7
	var m = Math.floor( (a + 11 * h + 22 * l) / 451 )

	var day   = Math.floor( ((h + l - 7 * m + 114) % 31) + 1 );
	var month = Math.floor( (h + l - 7 * m + 114) / 31 );

	month = (month < 10) ? "0" + month : month;
	day   = (day < 10) ? "0" + day : day;

	return [ year , month , day ].join( "-" );

}

// NOTE : REVISAR LOS FERIADOS
/**
 * Halla el numero de dias habiles entre dos fechas
 * @param  {Date} fechaInicial
 * @param  {Date} fechaFinal
 */
utils.diasHabiles = function( fechaInicial , fechaFinal ) {
	var nroDiasHabiles = 0;
	var fechaInicial   = moment( fechaInicial );
	var fechaFinal     = moment( fechaFinal );

	var ini = fechaInicial.clone();
	var fin = fechaFinal.clone();

	//Contamos los dias laborables Lun-Vier
	for( var i = ini ; i <= fin ; i = i.add( 1 , "days" ) ) {
		//0 domingo
		//6 sabado
		//1-5 dias de semana
		var day = i.weekday();

		if( day % 6 ) {
			nroDiasHabiles++;
		}
	}

	var anhoInicial = fechaInicial.year();
	var anhoFinal   = fechaFinal.year();

	fechaInicial = fechaInicial.subtract( 1 , "days" );
	fechaFinal   = fechaFinal.add( 1 , "days" );

	//Feriados
	feriadosFijos.forEach( function( feriado ) {
		var mFeriado;
		mFeriado = moment( [ anhoFinal , feriado ].join( "-" ) );

		if( mFeriado.isBetween( fechaInicial , fechaFinal ) && mFeriado.day() % 6 ) {
			nroDiasHabiles--;
		}
	} );

	//Semana santa
	var easterDate = moment( getDateEaster( anhoFinal ) );
	var tuesday    = easterDate.clone().subtract( 3 , "days" );
	var friday     = easterDate.clone().subtract( 2 , "days" );

	[ tuesday , friday ].forEach( function( feriado ) {
		if( feriado.isBetween( fechaInicial , fechaFinal ) && feriado.day() % 6 ) {
			nroDiasHabiles--;
		}
	} );

	if( anhoInicial !== anhoFinal ) {
		//Feriados
		feriadosFijos.forEach( function( feriado ) {
			var mFeriado;

			mFeriado = moment( [ anhoInicial , feriado ].join( "-" ) );

			if( mFeriado.isBetween( fechaInicial , fechaFinal ) && mFeriado.day() % 6 ) {
				nroDiasHabiles--;
			}
		} );

		//Semana Santa
		var wat        = getDateEaster( anhoFinal );
		var easterDate = moment( getDateEaster( anhoFinal ) );
		var tuesday    = easterDate.subtract( 3 , "days" );
		var friday     = easterDate.subtract( 2 , "days" );

		[ tuesday , friday ].forEach( function( feriado ) {
			if( feriado.isBetween( fechaInicial , fechaFinal ) && feriado.day() % 6 ) {
				nroDiasHabiles--;
			}
		} );
	}

	return nroDiasHabiles;
}

utils.equalsDecimal = function( a , b ) {
	var diff = Math.abs( a - b );
	diff     = parseFloat( diff.toFixed( 2 ) );
	return diff <= 0.01;
};

utils.deleteNullProperties = function( objeto ) {
	for( var i in objeto ) {

		if( objeto[ i ] == null || objeto[ i ] == undefined ) {
			delete objeto[ i ];
		}

		if( Array.isArray( objeto[ i ] ) ) {
			if( objeto[ i ].length < 1 ) {
				delete objeto[ i ]
			} else {
				objeto[ i ].forEach( function( objetoitem ) {
					utils.deleteNullProperties( objetoitem );
				} );
			}
		}

		if( typeof(objeto[ i ]) == 'object' ) {
			utils.deleteNullProperties( objeto[ i ] );
		}
	}

};

var units   = [ 'cero' , 'uno' , 'dos' , 'tres' , 'cuatro' , 'cinco' , 'seis' , 'siete' , 'ocho' , 'nueve' ];
var decs    = [ 'X' , 'y' , 'veinte' , 'treinta' , 'cuarenta' , 'cincuenta' , 'sesenta' , 'setenta' , 'ochenta' , 'noventa' ];
var dieces  = [ 'diez' , 'once' , 'doce' , 'trece' , 'catorce' , 'quince' , 'dieciseis' , 'diecisiete' , 'dieciocho' , 'diecinueve' ];
var cientos = [ 'x' , 'cien' , 'doscientos' , 'trescientos' , 'cuatrocientos' , 'quinientos' , 'seiscientos' , 'setecientos' , 'ochocientos' , 'novecientos' ];

utils.numberToWords = function( num , rec ) {
	num = parseInt( num );
	if( !rec ) {
		rec = false;
	}

	var millones = parseInt( num / 1000000 );
	//Millares
	var millares = parseInt( (num / 1000 ) ) % 1000;
	//Centenas
	var centenas = parseInt( (num / 100 ) ) % 10;
	//Decenas
	var decenas = parseInt( ( num / 10 ) ) % 10;
	//Unidades
	var unidades = num % 10;

	var letras = "";

	if( millones == 1 ) {
		letras = letras.concat( 'un millón' );
	} else if( millones > 1 ) {
		letras = letras.concat( utils.numberToWords( millones , true ) , ' millones' );
	}

	if( millares == 1 ) {
		letras = letras.concat( ' mil' );

	} else if( millares > 0 ) {
		letras = letras.concat( ' ' , utils.numberToWords( millares , true ) , ' mil' );
	}

	if( centenas == 1 ) {
		letras = letras.concat( num % 100 == 0 ? ' cien' : ' ciento' );
	} else if( centenas > 0 ) {
		letras = letras.concat( ' ' , cientos[ centenas ] );
	}

	if( decenas == 1 ) {
		letras   = letras.concat( ' ' , dieces[ num % 10 ] );
		unidades = 0
	} else if( decenas == 2 && unidades > 0 ) {

		letras   = letras.concat( ' veinti' , units[ unidades ] );
		unidades = 0
	} else if( decenas > 1 ) {

		letras = letras.concat( ' ' , decs[ decenas ] );
		if( unidades > 0 ) {
			letras = letras.concat( ' y' );
		}
	}

	if( unidades > 0 ) {
		if( unidades == 1 && rec ) {
			letras = letras.concat( ' ' , 'un' );

		} else {
			letras = letras.concat( ' ' , units[ unidades ] );
		}

	} else if( num == 0 ) {
		letras = letras.concat( units[ 0 ] );
	}

	return letras.toString().trim()

};

utils.complementDecimal = function( number ) {
	var number   = number.toString();
	var complete = (number.indexOf( "." ) == -1) ? number + ".00" : number;
	var dec      = complete.split( "." )[ 1 ].substr( 0 , 2 );
	dec          = (dec.length == 1) ? dec + "0" : dec;
	return dec + "/100";
};

utils.cleanObject = function( objectToClean , fieldsNotAllowed ) {
	var filtered = {};
	async.forEachOf( objectToClean , function( value , key , cb ) {
		if( fieldsNotAllowed.indexOf( key ) == -1 ) {
			filtered[ key ] = value;
		}
		cb()
	} );
	return filtered;
};
/*
Lucho added
 */
function isNumber(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}
function isAlphaNumeric(data) {
	if(data.match(/^[0-9a-z]+$/))
		return true;
	return false;
}
utils.validarReceptorRuc = function (ruc,tipo) {
	if(tipo == '0'){
		return true;
	}else if(tipo == '6'){
		return (isNumber(ruc)) && (ruc.length == 11)
	}else return false;
};
utils.validarReceptorDni = function (dni) {
	return (isNumber(dni)) && (dni.length == 8)
};
utils.validarReceptorOtrosDoc = function (doc) {
	if(isAlphaNumeric(doc) && (doc.length>= 4)){
		return true;
	}else return false;
};


// public function validarReceptorOtrosDocumentosIdentidad($datos){
// 	if (isset($datos->receptor->idReceptor) && isset($datos->receptor->razonSocial) && isset($datos->receptor->tipo)) {
// 		if(!$this->esOtroDocumentoIdentidad($datos->receptor->idReceptor)) {
// 			throw new CustomException("El documento  proporcionado no se muestra correctamente",400,4,'4102');
// 		}
// 		if(!$this->validarTexto($datos->receptor->razonSocial, '/[%<>={}&|\¿¡!?*+"]+/')) {
// 			throw new CustomException("Error en el  nombre y/o apellidos!",400,4,'4100');
// 		}
// 	}else
// 		throw new CustomException( "faltan datos importantes del receptor",400,4,'3015');
// }

module.exports = utils;
