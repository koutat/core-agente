var erroresMsg = require( "../error/index" ).validationMessage;
var util       = require( "util" );

module.exports = function( sequelize , DataTypes ) {
	var ComunicacionBajaItem = sequelize.define( 'ComunicacionBajaItem' ,
		{
			idComunicacionBajaItem : {
				type :          DataTypes.BIGINT ,
				allowNull :     false ,
				primaryKey :    true ,
				autoIncrement : true
			} ,
			descripcion :            {
				type :         DataTypes.STRING ,
				defaultValue : '' ,
				validate :     {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "descripcion" )
					} ,
					len :      {
						args : [ 1 , 100 ] ,
						msg :  erroresMsg.getMessage( "len" , "descripcion" , 1 , 100 )
					}
				}
			} ,
			serie :                  {
				type :         DataTypes.STRING ,
				allowNull :    false ,
				defaultValue : "" ,
				validate :     {
					notEmpty :       {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "serie" )
					} ,
					is :       {
						args : [ "^(F|B)([A-Z0-9]){3}$" ] ,
						msg :  erroresMsg.getMessage( "pattern" , "serie" )
					} ,
					len :      {
						args : [ 4 , 4 ] ,
						msg :  erroresMsg.getMessage( "len2" , 4 )
					} ,
					isAlphanumeric : {
						args : true ,
						msg :  erroresMsg.getMessage( "isAlphanumeric" , "serie" )
					}
				}
			} ,
			numero :                 {
				type :         DataTypes.INTEGER ,
				allowNull :    false ,
				validate :     {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "numero" )
					} ,
					isInt :    {
						args : true ,
						msg :  erroresMsg.getMessage( "isInt" , "numero" )
					} ,
					max :      {
						args : "99999999" ,
						msg :  erroresMsg.getMessage( "max" , "numero" )
					} ,
					min :      {
						args : "1" ,
						msg :  erroresMsg.getMessage( "min" , "numero" )
					}
				}
			} ,
			tipoComprobante :        {
				type :         DataTypes.STRING ,
				allowNull :    false ,
				defaultValue : "" ,
				validate :     {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "tipoComprobante" )
					} ,
					isIn :     {
						args : [ [ "01" , "03" ] ] ,
						msg :  erroresMsg.getMessage( "isIn" , "tipoComprobante" )
					}
				}
			}
		} ,
		{
			tableName :    'clientes_comunicacionbajaitem' ,
			classMethods : {
				associate :        function( models ) {
					ComunicacionBajaItem.belongsTo( models.ComunicacionBaja , {
						foreignKey : 'idComunicacionBaja' ,
						as :         "comunicacionbaja"
					} );
				} ,
				getFieldsNotSave : function() {
					return [ "idComunicacionBajaItem" , "idComunicacionBaja" ];
				}
			} ,
			validate :     {}
		} );
	return ComunicacionBajaItem;
};
