/**
 * Created by Luis on 09/08/2016.
 */
var erroresMsg             = require( "../error/index" ).validationMessage;
var utils                  = require( "../utils" );
var moment                 = require( "moment" );

module.exports = function( sequelize , DataTypes ) {
    var Prueba = sequelize.define( 'Prueba' , {
            id :                 {
                type :          DataTypes.BIGINT ,
                primaryKey :    true ,
                autoIncrement : true
            } ,
            xml :            {
                type : DataTypes.BLOB
            }

        } ,
        {
            tableName :       'prueba',
            classMethods:{
                buscarUno: function () {
                    return Prueba.findOne();
                }
            }

        } );

    return Prueba;
}
;
