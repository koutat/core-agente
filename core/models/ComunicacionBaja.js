var erroresMsg = require( "../error/index" ).validationMessage;
var util       = require( "util" );
var moment     = require( "moment" );
var utils      = require( "../utils" );
module.exports = function( sequelize , DataTypes ) {
	var ComunicacionBaja = sequelize.define( 'ComunicacionBaja' ,
		{
			idComunicacionBaja :    {
				type :          DataTypes.BIGINT ,
				allowNull :     false ,
				primaryKey :    true ,
				autoIncrement : true
			} ,
			fechaEmision :       {
				type :         DataTypes.DATE ,
				defaultValue : DataTypes.NOW ,
				allowNull :    true ,
				get :          function() {
					var value = this.getDataValue( "fechaEmision" );
					if( value ) {
						return moment( value ).format( "YYYY-MM-DD hh:mm:ss" );
					}
				}
			} ,
			fechaEmisionDocs :   {
				type :         DataTypes.DATEONLY ,
				allowNull :    false ,
				validate :     {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "fechaEmisionDocs" )
					} ,
					isDate :   {
						args : true ,
						msg :  erroresMsg.getMessage( "isDate" , "fechaEmisionDocs" )
					} ,
					is :       {
						args : /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/ ,
						msg :  "No cumple con el patron YYYY-MM-DD"
					}

				} ,
				get :          function() {
					var value = this.getDataValue( "fechaEmisionDocs" );
					if( value ) {
						return moment( value ).format( "YYYY-MM-DD" );
					}
				}
			} ,
			fechaEmitido :       {
				type : DataTypes.DATE ,
				get :  function() {
					var value = this.getDataValue( "fechaEmitido" );
					if( value ) {
						return moment( value ).format( "YYYY-MM-DD hh:mm:ss" );
					}
				}
			} ,
			numero :             {
				type :     DataTypes.INTEGER ,
				validate : {
					isInt : {
						args : true ,
						msg :  erroresMsg.getMessage( "isInt" , "numero" )
					} ,
					max :   {
						args : "99999999" ,
						msg :  erroresMsg.getMessage( "max" , "numero" )
					} ,
					min :   {
						args : "1" ,
						msg :  erroresMsg.getMessage( "min" , "numero" )
					}
				}
			} ,
			ticketConstancia :   {
				type : DataTypes.STRING
			} ,
			emitido :            {
				type :         DataTypes.INTEGER ,
				defaultValue : 0
			} ,
			digestValue :        {
				type : DataTypes.STRING
			} ,
			signatureValue :     {
				type : DataTypes.BLOB
			} ,
			xml :                {
				type : DataTypes.BLOB
			} ,
			tipoComprobante :    {
				type : DataTypes.VIRTUAL ,
				get :  function() {
					return "comunicacionbaja"
				}
			} ,
			tipoComprobantePolite : {
				type : DataTypes.VIRTUAL ,
				get :  function() {
					return "Comunicacion de Baja";
				}
			} ,
			codigoComprobante :     {
				type : DataTypes.VIRTUAL ,
				get :  function() {
					return "RA";
				}
			}
		} ,
		{
			tableName :       'clientes_comunicacionbaja' ,
			validate :        {
				isUnique :              function() {
					var self               = this;
					var fechaEmisionDocs = self.get( 'fechaEmisionDocs' ) || null;
					var idEmisorReq      = self.get( 'idEmisor' ) || null;
					var numeroReq        = self.get( "numero" ) || null;
					var idcomunicacionbaja = self.get( "idcomunicacionbaja" ) || null;

					if( !idcomunicacionbaja && numeroReq ) {
						return ComunicacionBaja.findOne( {
								where : {
									fechaEmisionDocs : fechaEmisionDocs ,
									idEmisor :         idEmisorReq ,
									numero :           numeroReq
								}
							} )
							.then( function( lastcomunicacionbaja ) {
								if( lastcomunicacionbaja ) {
									throw new Error( "La comunicacion de baja " + fechaEmisionDocs + "-" + numeroReq + " ya ha sido emitida" );
								}
							} )
							.catch( function( error ) {
								throw error;
							} );
					}
				} ,
				hasItems :              function() {
					var comunicacionbaja = this;
					if( !(comunicacionbaja.get( 'items' ) && Array.isArray( comunicacionbaja.get( 'items' ) ) && comunicacionbaja.get( 'items' ).length > 0) ) {
						throw new Error( erroresMsg.getMessage( "items" , "items" ) )
					}
				} ,
				verificarFechaEmision : function() {

					var self             = this;
					var fechaEmisionDocs = moment( self.fechaEmisionDocs ).add( 1 , 'days' ).startOf( 'day' );
					var now              = moment();

					var diferenciaDias = fechaEmisionDocs.diff( now , 'days' );
					if( Math.abs( diferenciaDias ) > 3 ) {
						throw new Error( "La comunicacion de baja se puede emitir maximo 72 horas contadaras a partir del dia siguiente de la fecha de recepcion de los documentos" );
					}

				} ,
				existenItems :          function() {
					var self = this;

					var items            = self.items;
					var idEmisor = self.idEmisor;
					var fechaEmisionDocs = moment( self.fechaEmisionDocs );

					var map = {
						"01" : sequelize.models.Factura ,
						"03" : sequelize.models.Boleta
					};

					var promises = items.map( function( item ) {
						var serie           = item.serie;
						var numero = item.numero;
						var tipoComprobante = item.tipoComprobante;

						if( map[ tipoComprobante ] ) {
							return map[ tipoComprobante ].findOneBySerieNumeroEmisor( serie , numero , idEmisor )
								.then( function( comprobante ) {
									if( !comprobante ) {
										throw new Error( "El comprobante " + serie + "-" + numero + " no existe" )
									} else {

										if( !comprobante.estaEmitido() ) {
											throw new Error( "El comprobante " + serie + "-" + numero + " no esta aceptado" );
										} else {
											var compDate = moment( comprobante.get( "fechaEmision" ) );
											if( !fechaEmisionDocs.isSame( compDate , "day" ) ) {
												throw new Error( "El comprobante " + serie + "-" + numero + " no pertenece a la misma fecha de emision de los documentos" );
											}

										}
									}
								} )
								.catch( function( error ) {
									throw error;
								} )
						} else {
							throw new Error( "Error en validar los comprobantes" );
						}
					} );

					return sequelize.Promise.all( promises );
				} ,
				uniqueItems :           function() {
					var self     = this;
					var items = self.items;
					var idEmisor = self.idEmisor;

					var mapTypes = {
						"01" : sequelize.models.Factura ,
						"03" : sequelize.models.Boleta
					};

					var promises = items.map( function( item ) {
						var serie  = item.serie;
						var numero = item.numero;
						var type   = item.tipoComprobante;

						return mapTypes[ type ].findOne( {
								where : {
									serie :    serie ,
									numero :   numero ,
									idEmisor : idEmisor
								}
							} )
							.then( function( comprobante ) {
								if( comprobante ) {
									if( comprobante.estaDeBaja() ) {
										throw new Error( "El comprobante " + serie + "-" + numero + " ya esta dado de baja" );
									}
								}
							} )
							.catch( function( error ) {
								throw error;
							} )

					} );

					return sequelize.Promise.all( promises );
				}
			} ,
			hooks :           {
				beforeCreate : []
			} ,
			instanceMethods : {
				getXmlField :                 function() {
					var self = this;
					var id   = self.idComunicacionBaja;

					return ComunicacionBaja.find( {
						where :      { idComunicacionBaja : id } ,
						attributes : { exlclude : [ "xml" ] }
					} ).then( function( comp ) {
							if( !comp || !comp.xml ) {
								return sequelize.Promise.resolve( null );
							}
							return sequelize.Promise.resolve( comp.xml );
						} )
						.catch( function( err ) {
							return sequelize.Promise.reject( err );
						} );
				} ,
				pretty :                      function() {
					var self = this;
					self     = self.get( { plain : true } );
					utils.deleteNullProperties( self );
					return self;
				} ,
				estaEmitido :                 function() {
					var self = this;
					return self.emitido == 1;
				} ,
				getFechaEmisionFormatoSunat : function() {
					var self = this;
					return moment( self.fechaEmision ).format( "YYYY-MM-DD" );
				} ,
				getFechaDocsFormatoSunat :    function() {
					var self = this;
					return moment( self.fechaEmisionDocs ).format( "YYYY-MM-DD" );
				} ,
				getNombreBase :               function() {
					var self = this;
					if( !self.emisor ) {
						throw new Error( "La comunicacion de baja no tiene asociada un emisor" );
					}
					var fechaEmision = moment( self.fechaEmision ).format( "YYYYMMDD" );
					return [ self.get( 'idEmisor' ) , self.get( 'codigoComprobante' ) , fechaEmision , self.get( 'numero' ) ].join( "-" );
				}
			} ,
			classMethods :    {
				associate :                  function( models ) {
					ComunicacionBaja.hasMany( models.ComunicacionBajaItem , {
						foreignKey : 'idComunicacionBaja' ,
						as :         {
							singular : 'item' ,
							plural :   'items'
						}
					} );
					// ComunicacionBaja.belongsTo( models.Usuario , { foreignKey : 'idUsuario' , as : "usuario" } );
					// ComunicacionBaja.belongsTo( models.Emisor , { foreignKey : 'idEmisor' , as : "emisor" } );
					// ComunicacionBaja.belongsTo( models.Constancia , {
					// 	foreignKey : 'idConstancia' ,
					// 	as :         "constancia"
					// } );
				} ,
				getFieldsNotSave :           function() {
					return [ "idComunicacionBaja" , "fechaEmitido" , "emitido" , "digestValue" , "signatureValue" , "xml" ];
				} ,
				getFieldsToQuery :           function() {
					var notSave    = [ "idComunicacionBaja" , "digestValue" , "signatureValue" , "xml" ];
					var attr    = Object.keys( this.attributes );
					var saveFields = attr.filter( function( attribute ) {

						if( notSave.indexOf( attribute ) == -1 ) {
							return true;
						}
					} );
					return saveFields;
				} ,
				findOneBySerieNumeroEmisor : function( serie , numero , idEmisor ) {
					return ComunicacionBaja.findOne( {
						where :      {
							fechaEmisionDocs : serie ,
							numero :           numero ,
							idEmisor :         idEmisor
						} ,
						attributes : {
							exclude : [ "xml" ]
						} ,
						include :    [
							{
								model : sequelize.models.ComunicacionBajaItem ,
								as :    "items"
							}
						]
					} );
				} ,
				findOneWithAllDataById :     function( idComunicacionBaja ) {
					return ComunicacionBaja.findOne( {
						where :      { idComunicacionBaja : idComunicacionBaja } ,
						attributes : {
							exclude : [ "xml" ]
						} ,
						include :    [
							{
								model : sequelize.models.ComunicacionBajaItem ,
								as :    "items"
							} ,
							{
								model :      sequelize.models.Emisor ,
								as :         "emisor" ,
								attributes : [ "idEmisor" , 'razonSocial' , "nombreComercial" ] ,
								include :    [
									{
										model :      sequelize.models.Domicilio ,
										as :         "domicilio" ,
										attributes : [ "departamento" , "provincia" , "distrito" , "ubigeo" , "direccion" ] ,
									} ,
									{
										model :      sequelize.models.UsuarioSunat ,
										as :         "usuariosunat" ,
										attributes : [ "username" , "password" ]
									} ,
									{
										model : sequelize.models.EmisorConfiguracion ,
										as :    "configuracion"
									}
								]
							}
						]
					} );
				} ,
				findAllWithAddData :         function( idEmisor , query ) {
					if( typeof query !== "object" ) {
						throw new Error( "Los querys deben sere objetos" );
					}

					if( !query.where ) {
						throw new Error( "Los querys deben tener al menos el attribute where" );
					}

					query.where.idEmisor = idEmisor;
					query.attributes     = { exclude : [ "xml" ] };
					query.include        = [
						{
							model : sequelize.models.ComunicacionBajaItem ,
							as :    "items"
						}
					];
					return ComunicacionBaja.findAll( query )
				}
			}
		}
	);

	return ComunicacionBaja;
};
