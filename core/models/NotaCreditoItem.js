var unidadesMedida          = require( '../catalogos/unidadesMedida.json' );
var tipoPrecioVentaUnitario = require( '../catalogos/tipoPrecioUnitario.json' ).codigos;
var tipoAfectacionIgv       = require( '../catalogos/tipoAfectacionIgv.json' ).codigos;
var tipoAfectacionIsc       = require( '../catalogos/tipoAfectacionIsc.json' ).codigos;
var erroresMsg              = require( "../error/index" ).validationMessage;
var utils                   = require( "../utils" );

module.exports = function( sequelize , DataTypes ) {
	var NotaCreditoItem = sequelize.define( 'NotaCreditoItem' , {
			idNotaCreditoItem :       {
				type :          DataTypes.BIGINT ,
				allowNull :     false ,
				primaryKey :    true ,
				autoIncrement : true
			} ,
			unidadMedidaCantidad : {
				type :         DataTypes.STRING ,
				allowNull :    false ,
				defaultValue : '' ,
				validate :     {
					notEmpty :       {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "unidadMedidaCantidad" )
					} ,
					isIn :     {
						args : [ unidadesMedida.codigos ] ,
						msg :  erroresMsg.getMessage( "isIn" , "unidadMedidaCantidad" )
					} ,
					isAlphanumeric : {
						args : true ,
						msg :  erroresMsg.getMessage( "isAlphanumeric" , "unidadMedidaCantidad" )
					} ,
					len :            {
						args : [ 1 , 3 ] ,
						msg :  erroresMsg.getMessage( "len" , "unidadMedidaCantidad" , 1 , 3 )
					}
				}
			} ,
			cantidad :             {
				type :         DataTypes.DECIMAL( 12 , 3 ) ,
				allowNull :    false ,
				validate :     {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "cantidad" )
					} ,
					isFloat :  {
						args : true ,
						msg :  erroresMsg.getMessage( "isFloat" , "cantidad" , 1 , 3 )
					} ,
					max :      {
						args : "999999999999.999" ,
						msg :  erroresMsg.getMessage( "max" , "cantidad" )
					} ,
					min :      {
						args : "0.000" ,
						msg :  erroresMsg.getMessage( "min" , "cantidad" )
					}
				}
			} ,
			valorVenta :           {
				type :         DataTypes.DECIMAL( 12 , 2 ) ,
				allowNull :    false ,
				defaultValue : 0 ,
				validate :     {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "valorVenta" )
					} ,
					isFloat :  {
						args : true ,
						msg :  erroresMsg.getMessage( "isFloat" , "valorVenta" , 12 , 2 )
					} ,
					max :      {
						args : "999999999999.99" ,
						msg :  erroresMsg.getMessage( "max" , "valorVenta" )
					} ,
					min :      {
						args : "0.00" ,
						msg :  erroresMsg.getMessage( "min" , "valorVenta" )
					}
				}
			} ,
			montoAfectacionIgv :   {
				type :         DataTypes.DECIMAL( 12 , 2 ) ,
				allowNull :    false ,
				defaultValue : 0 ,
				validate :     {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "montoAfectacionIgv" )
					} ,
					isFloat :  {
						args : true ,
						msg :  erroresMsg.getMessage( "isFloat" , "montoAfectacionIgv" )
					} ,
					max :      {
						args : "999999999999.99" ,
						msg :  erroresMsg.getMessage( "max" , "montoAfectacionIgv" )
					} ,
					min :      {
						args : "0.00" ,
						msg :  erroresMsg.getMessage( "min" , "montoAfectacionIgv" )
					}
				}
			} ,
			tipoAfectacionIgv :    {
				type :         DataTypes.STRING ,
				allowNull :    false ,
				defaultValue : '' ,
				validate :     {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "tipoAfectacionIgv" )
					} ,
					isIn :     {
						args : [ tipoAfectacionIgv ] ,
						msg :  erroresMsg.getMessage( "isIn" , "tipoAfectacionIgv" )
					}
				}
			} ,
			montoAfectacionIsc :   {
				type :      DataTypes.DECIMAL( 12 , 2 ) ,
				allowNull : true ,
				validate :  {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "montoAfectacionIsc" )
					} ,
					isFloat :  {
						args : true ,
						msg :  erroresMsg.getMessage( "isFloat" , "montoAfectacionIsc" , 12 , 2 )
					} ,
					max :      {
						args : "999999999999.99" ,
						msg :  erroresMsg.getMessage( "max" , "montoAfectacionIsc" )
					} ,
					min :      {
						args : "0.00" ,
						msg :  erroresMsg.getMessage( "min" , "montoAfectacionIsc" )
					}
				}
			} ,
			tipoAfectacionIsc :    {
				type :      DataTypes.STRING ,
				allowNull : true ,
				validate :  {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "tipoAfectacionIsc" )
					} ,
					isIn :     {
						args : [ tipoAfectacionIsc ] ,
						msg :  erroresMsg.getMessage( "isIn" , "tipoAfectacionIsc" )
					}
				}
			} ,
			precioVentaUnitario :  {
				type :         DataTypes.DECIMAL( 12 , 2 ) ,
				allowNull :    false ,
				validate :     {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "precioVentaUnitario" )
					} ,
					isFloat :  {
						args : true ,
						msg :  erroresMsg.getMessage( "isFloat" , "precioVentaUnitario" , 12 , 2 )
					} ,
					max :      {
						args : "999999999999.99" ,
						msg :  erroresMsg.getMessage( "max" , "precioVentaUnitario" )
					} ,
					min :      {
						args : "0.00" ,
						msg :  erroresMsg.getMessage( "min" , "precioVentaUnitario" )
					}
				}
			} ,
			tipoPrecioVentaUnitario : {
				type :         DataTypes.STRING ,
				allowNull :    true ,
				defaultValue : "01" ,
				validate :     {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "tipoPrecioVentaUnitario" )
					} ,
					isIn :     {
						args : [ tipoPrecioVentaUnitario ] ,
						msg :  erroresMsg.getMessage( "isIn" , "tipoPrecioVentaUnitario" )
					}
				}
			} ,
			descripcion :             {
				type :         DataTypes.STRING ,
				allowNull :    false ,
				defaultValue : '' ,
				validate :     {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "descripcion" )
					} ,
					len :      {
						args : [ 1 , 250 ] ,
						msg :  erroresMsg.getMessage( "len" , "descripcion" , 1 , 250 )
					}
				}
			} ,
			codigoProducto :          {
				type :      DataTypes.STRING ,
				allowNull : true ,
				validate :  {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "codigoProducto" )
					} ,
					len :      {
						args : [ 1 , 30 ] ,
						msg :  erroresMsg.getMessage( "len" , "codigoProducto" , 1 , 30 )
					}
				}
			} ,
			descuento :               {
				type :      DataTypes.DECIMAL( 12 , 2 ) ,
				allowNull : true ,
				validate :  {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "descuento" )
					} ,
					isFloat :  {
						args : true ,
						msg :  erroresMsg.getMessage( "isFloat" , "descuento" )
					} ,
					max :      {
						args : "999999999999.99" ,
						msg :  erroresMsg.getMessage( "max" , "descuento" )
					} ,
					min :      {
						args : "0.00" ,
						msg :  erroresMsg.getMessage( "min" , "descuento" )
					}
				}
			} ,
			porcentajeDescuento :     {
				type :      DataTypes.DECIMAL( 12 , 2 ) ,
				allowNull : true ,
				validate :  {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "porcentajeDescuento" )
					} ,
					isFloat :  {
						args : true ,
						msg :  erroresMsg.getMessage( "isFloat" , "porcentajeDescuento" )
					} ,
					max :      {
						args : "999999999999.99" ,
						msg :  erroresMsg.getMessage( "max" , "porcentajeDescuento" )
					} ,
					min :      {
						args : "0.00" ,
						msg :  erroresMsg.getMessage( "min" , "porcentajeDescuento" )
					}
				}
			} ,
			valorUnitario :           {
				type :         DataTypes.DECIMAL( 12 , 2 ) ,
				allowNull :    true ,
				defaultValue : 0 ,
				validate :     {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "valorUnitario" )
					} ,
					isFloat :  {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "valorUnitario" , 12 , 2 )
					} ,
					max :      {
						args : "999999999999.99" ,
						msg :  erroresMsg.getMessage( "max" , "valorUnitario" )
					} ,
					min :      {
						args : "0.00" ,
						msg :  erroresMsg.getMessage( "min" , "valorUnitario" )
					}
				}
			} ,
			unidadMedidadHuman :      {
				type : DataTypes.VIRTUAL ,
				get :  function() {
					var self = this;
					return unidadesMedida.human[ self.unidadMedidaCantidad ];
				}
			}
		} ,
		{
			tableName :       'clientes_notacreditoitem' ,
			validate :        {

				calculoIgv :            function() {
					var item       = this;
					var igvPercent = utils.igvPercent;

					var valorVentaItem    = parseFloat( item.get( "valorVenta" ) || 0 );
					var isc            = parseFloat( item.get( "montoAfectacionIsc" ) || 0 );
					var igv            = parseFloat( item.get( "montoAfectacionIgv" ) || 0 );
					var tipoAfectacionIgv = item.get( "tipoAfectacionIgv" ) || "10";

					//Calculo del igv a nivel de item
					if( tipoAfectacionIgv >= 10 && tipoAfectacionIgv < 20 ) {
						var calcIgv = parseFloat( ((valorVentaItem + isc) * igvPercent).toFixed( 2 ) );
						if( !utils.equalsDecimal( calcIgv , igv ) ) {
							throw new Error( "Error en el calculo del IGV del item : " + igv );
						}
					}
				} ,
				validarDependientes :   function() {
					var self                = this;
					var descuento = self.descuento;
					var porcentajeDescuento = self.porcentajeDescuento;
					//Porcentaje descuento
					if (self.descuento && self.porcentajeDescuento) {
						if( descuento != null || porcentajeDescuento != null ) {
							if( !(descuento != null && porcentajeDescuento != null ) ) {
								throw new Error( "Si es que se especifica un descuento a nivel de item , debe especificar los campos porcentajeDescuento y descuento" );
							}
						}
					}	
				} ,
				gratuitoAfectacionIgv : function() {

					var item = this;
					if( item.get( 'tipoPrecioVentaUnitario' ) == '02' ) {
						var tipoAfectacionIgv = parseInt( item.get( "tipoAfectacionIgv" ) );
						if( [ 10 , 20 , 30 ].indexOf( tipoAfectacionIgv ) != -1 ) {
							throw new Error( "Error en el calculo a nivel de item : Si es venta gratuita el tipo de Igv no puede ser 10,20 o 30" );
						}
					}
				} ,
				gratutioValorVenta :    function() {
					var item                    = this;
					var valorVentaItem = parseFloat( item.get( "valorVenta" ) ) || 0.00;
					var tipoPrecioVentaUnitario = item.get( "tipoPrecioVentaUnitario" ) || "01";
					if( valorVentaItem == 0.00 && tipoPrecioVentaUnitario != "02" ) {
						throw new Error( "Si el valorVenta del item es 0 , se considera que es una operacion no onerosa y se debe especificar el tipoPrecioVentaUnitario en 02" );
					}
				}

			} ,
			classMethods :    {
				associate :        function( models ) {
					NotaCreditoItem.belongsTo( models.NotaCredito , { foreignKey : 'idNotaCredito' } );
				} ,
				getFieldsNotSave : function() {
					return [ "idNotaCredito" , "idNotaCreditoItem" ];
				}
			}
			,
			instanceMethods : {
				getHumanUnidadMedida : function() {
					var self = this;
					return unidadesMedida.human[ self.unidadMedidaCantidad ];
				}
			}
		}
	);

	return NotaCreditoItem;
};
