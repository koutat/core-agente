var catalogoTipoMoneda     = require( '../catalogos/tipoMoneda.json' );
var catalogoTipoGuia       = require( '../catalogos/tipoGuia.json' ).codigos;
var catalogoTipoDocumentoR = require( '../catalogos/tipoDocumentoR.json' ).codigos;
var erroresMsg             = require( "../error/index" ).validationMessage;
var utils                  = require( "../utils" );
var moment                 = require( 'moment' );

module.exports = function( sequelize , DataTypes ) {
	var Boleta = sequelize.define( 'Boleta' ,
		{
			idBoleta :                  {
				type :          DataTypes.BIGINT ,
				allowNull :     false ,
				primaryKey :    true ,
				autoIncrement : true
			} ,
			fechaEmision : {
				type :         DataTypes.DATE ,
				defaultValue : DataTypes.NOW ,
				allowNull :    false ,
				get :          function() {
					var value = this.getDataValue( "fechaEmision" );
					if( value ) {
						return moment( value ).format( "YYYY-MM-DD hh:mm:ss" );
					}
				}
			} ,
			serie :        {
				type :         DataTypes.STRING ,
				allowNull :    false ,
				validate :     {
					notEmpty :       {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "serie" )
					} ,
					isAlphanumeric : {
						args : true ,
						msg :  erroresMsg.getMessage( "isAlphanumeric" , "serie" )
					} ,
					len :      {
						args : [ 4 , 4 ] ,
						msg :  erroresMsg.getMessage( "len2" , "serie" , 4 )
					} ,
					is :       {
						args : [ "^B([A-Z0-9]){3}$" ] ,
						msg :  erroresMsg.getMessage( "pattern" , "serie" )
					} ,
					isValidFormat :  function( value , next ) {
						if( value[ 0 ] !== "B" ) {
							return next( "La serie debe empezar con B en el caso de boleta" )
						}
						return next();
					}
				}
			} ,
			numero :       {
				type :      DataTypes.INTEGER ,
				allowNull : false ,
				validate :  {
					isInt : {
						args : true ,
						msg :  erroresMsg.getMessage( "isInt" , "numero" )
					} ,
					max :   {
						args : "99999999" ,
						msg :  erroresMsg.getMessage( "max" , "numero" )
					} ,
					min :   {
						args : "1" ,
						msg :  erroresMsg.getMessage( "min" , "numero" )
					}
				}
			} ,
			receptor :       {
				type :      DataTypes.STRING ,
				allowNull : true,
				validate:{
					validarReceptor : function () {
						var factura = this;
						var receptor = factura.get('receptor');
						receptor = JSON.parse(receptor);
						if(receptor){
							if(receptor.idReceptor && receptor.tipo){
								switch (receptor.tipo){
									// case 6:if(!utils.validarReceptorRuc(receptor.idReceptor))throw new Error("Ruc incorrecto");break;
									case "1":if(!utils.validarReceptorDni(receptor.idReceptor))throw new Error("Dni incorrecto");break;
									case "0":if(!utils.validarReceptorOtrosDoc(receptor.idReceptor))throw new Error("Dni incorrecto");break;
									case "6":if(!utils.validarReceptorOtrosDoc(receptor.idReceptor))throw new Error("Dni incorrecto");break;
									case "7":if(!utils.validarReceptorOtrosDoc(receptor.idReceptor))throw new Error("Dni incorrecto");break;
									case "A":if(!utils.validarReceptorOtrosDoc(receptor.idReceptor))throw new Error("Dni incorrecto");break;
									default : throw  new Error("El tipo de documento del Receptor no cumple con las especificaciones");
								}
							}else throw new Error("Si especifica el campo receptor, debe especificar, el numero de documento y el tipo");
						}
					}
				}
			} ,
			//REVISAR LA POSIBILIDAD DE PERMITIIR NULL
			totalVentaGravada : {
				type :         DataTypes.DECIMAL( 12 , 2 ) ,
				defaultValue : 0 ,
				allowNull :    false ,
				validate :     {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "totalVentaGravada" )
					} ,
					isFloat :  {
						args : true ,
						msg :  erroresMsg.getMessage( "isFloat" , "totalVentaGravada" )
					} ,
					max :      {
						args : "999999999999.99" ,
						msg :  erroresMsg.getMessage( "max" , "totalVentaGravada" )
					} ,
					min :      {
						args : "0.00" ,
						msg :  erroresMsg.getMessage( "min" , "totalVentaGravada" )
					}
				}
			} ,
			//REVISAR LA POSIBILIDAD DE PERMITIIR NULL
			totalVentaInafecta : {
				type :      DataTypes.DECIMAL( 12 , 2 ) ,
				allowNull : true ,
				validate :  {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "totalVentaInafecta" )
					} ,
					isFloat :  {
						args : true ,
						msg :  erroresMsg.getMessage( "isFloat" , "totalVentaInafecta" )
					} ,
					max :      {
						args : "999999999999.99" ,
						msg :  erroresMsg.getMessage( "max" , "totalVentaInafecta" )
					} ,
					min :      {
						args : "0.00" ,
						msg :  erroresMsg.getMessage( "min" , "totalVentaInafecta" )
					}
				}
			} ,
			//REVISAR LA POSIBILIDAD DE PERMITIIR NULL
			totalVentaExonerada : {
				type :      DataTypes.DECIMAL( 12 , 2 ) ,
				allowNull : true ,
				validate :  {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "totalVentaExonerada" )
					} ,
					isFloat :  {
						args : true ,
						msg :  erroresMsg.getMessage( "isFloat" , "totalVentaExonerada" )
					} ,
					max :      {
						args : "999999999999.99" ,
						msg :  erroresMsg.getMessage( "max" , "totalVentaExonerada" )
					} ,
					min :      {
						args : "0.00" ,
						msg :  erroresMsg.getMessage( "min" , "totalVentaExonerada" )
					}
				}
			} ,
			totalVentaGratuita :  {
				type :      DataTypes.DECIMAL( 12 , 2 ) ,
				allowNull : true ,
				validate :  {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "totalVentaGratuita" )
					} ,
					isFloat :  {
						args : true ,
						msg :  erroresMsg.getMessage( "isFloat" , "totalVentaGratuita" )
					} ,
					max :      {
						args : "999999999999.99" ,
						msg :  erroresMsg.getMessage( "max" , "totalVentaGratuita" )
					} ,
					min :      {
						args : "0.00" ,
						msg :  erroresMsg.getMessage( "min" , "totalVentaGratuita" )
					}
				}
			} ,
			sumatoriaIgv :        {
				type :         DataTypes.DECIMAL( 12 , 2 ) ,
				allowNull :    true ,
				defaultValue : 0 ,
				validate :     {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "sumatoriaIgv" )
					} ,
					isFloat :  {
						args : true ,
						msg :  erroresMsg.getMessage( "isFloat" , "sumatoriaIgv" )
					} ,
					max :      {
						args : "999999999999.99" ,
						msg :  erroresMsg.getMessage( "max" , "sumatoriaIgv" )
					} ,
					min :      {
						args : "0.00" ,
						msg :  erroresMsg.getMessage( "min" , "sumatoriaIgv" )
					}
				}
			} ,
			sumatoriaIsc :        {
				type :      DataTypes.DECIMAL( 12 , 2 ) ,
				allowNull : true ,
				validate :  {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "sumatoriaIsc" )
					} ,
					isFloat :  {
						args : true ,
						msg :  erroresMsg.getMessage( "isFloat" , "sumatoriaIsc" )
					} ,
					max :      {
						args : "999999999999.99" ,
						msg :  erroresMsg.getMessage( "max" , "sumatoriaIsc" )
					} ,
					min :      {
						args : "0.00" ,
						msg :  erroresMsg.getMessage( "min" , "sumatoriaIsc" )
					}
				}
			} ,
			sumatoriaOtrosTributos : {
				type :      DataTypes.DECIMAL( 12 , 2 ) ,
				allowNull : true ,
				validate :  {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "sumatoriaOtros" )
					} ,
					isFloat :  {
						args : true ,
						msg :  erroresMsg.getMessage( "isFloat" , "sumatoriaOtros" )
					} ,
					max :      {
						args : "999999999999.99" ,
						msg :  erroresMsg.getMessage( "max" , "sumatoriaOtros" )
					} ,
					min :      {
						args : "0.00" ,
						msg :  erroresMsg.getMessage( "min" , "sumatoriaOtros" )
					}
				}
			} ,
			sumatoriaOtrosCargos :   {
				type :      DataTypes.DECIMAL( 12 , 2 ) ,
				allowNull : true ,
				validate :  {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "sumatoriaOtrosCargos" )
					} ,
					isFloat :  {
						args : true ,
						msg :  erroresMsg.getMessage( "isFloat" , "sumatoriaOtrosCargos" )
					} ,
					max :      {
						args : "999999999999.99" ,
						msg :  erroresMsg.getMessage( "max" , "sumatoriaOtrosCargos" )
					} ,
					min :      {
						args : "0.00" ,
						msg :  erroresMsg.getMessage( "min" , "sumatoriaOtrosCargos" )
					}
				}
			} ,
			totalDescuento :         {
				type :      DataTypes.DECIMAL( 12 , 2 ) ,
				allowNull : true ,
				validate :  {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "totalDescuento" )
					} ,
					isFloat :  {
						args : true ,
						msg :  erroresMsg.getMessage( "isFloat" , "totalDescuento" )
					} ,
					max :      {
						args : "999999999999.99" ,
						msg :  erroresMsg.getMessage( "max" , "totalDescuento" )
					} ,
					min :      {
						args : "0.00" ,
						msg :  erroresMsg.getMessage( "min" , "totalDescuento" )
					}
				}
			} ,
			totalVenta :             {
				type :         DataTypes.DECIMAL( 12 , 2 ) ,
				allowNull :    false ,
				defaultValue : 0 ,
				validate :     {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "totalVenta" )
					} ,
					isFloat :  {
						args : true ,
						msg :  erroresMsg.getMessage( "isFloat" , "totalVenta" )
					} ,
					max :      {
						args : "999999999999.99" ,
						msg :  erroresMsg.getMessage( "max" , "totalVenta" )
					} ,
					min :      {
						args : "0.00" ,
						msg :  erroresMsg.getMessage( "min" , "totalVenta" )
					}
				}
			} ,
			tipoMoneda :             {
				type :         DataTypes.STRING ,
				allowNull :    false ,
				defaultValue : "PEN" ,
				validate :     {
					notEmpty :       {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "tipoMoneda" )
					} ,
					isAlphanumeric : {
						args : true ,
						msg :  erroresMsg.getMessage( "isAlphanumeric" , "tipoMoneda" )
					} ,
					isIn :           {
						args : [ catalogoTipoMoneda.codigos ] ,
						msg :  erroresMsg.getMessage( "isIn" , "tipoMoneda" )
					}
				}
			} ,
			leyendas :               {
				type :         DataTypes.STRING ,
				allowNull :    true ,
				defaultValue : "" ,
				get :          function() {
					var leyendas = this.getDataValue( 'leyendas' );
					if( leyendas ) {
						return leyendas.split( "," );
					} else {
						return [];
					}
				} ,
				set :          function( value ) {
					this.setDataValue( 'leyendas' , value.join( "," ) );
				}
			} ,
			importePercepcion :      {
				type :      DataTypes.DECIMAL( 12 , 2 ) ,
				allowNull : true ,
				validate :  {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "importePercepcion" )
					} ,
					isFloat :  {
						args : true ,
						msg :  erroresMsg.getMessage( "isFloat" , "importePercepcion" )
					} ,
					max :      {
						args : "999999999999.99" ,
						msg :  erroresMsg.getMessage( "max" , "importePercepcion" )
					} ,
					min :      {
						args : "0.00" ,
						msg :  erroresMsg.getMessage( "min" , "importePercepcion" )
					}
				}
			} ,
			porcentajePercepcion :   {
				type :      DataTypes.DECIMAL( 12 , 2 ) ,
				allowNull : true ,
				validate :  {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "porcentajePercepcion" )
					} ,
					isFloat :  {
						args : true ,
						msg :  erroresMsg.getMessage( "isFloat" , "porcentajePercepcion" )
					} ,
					max :      {
						args : "999999999999.99" ,
						msg :  erroresMsg.getMessage( "max" , "porcentajePercepcion" )
					} ,
					min :      {
						args : "0.00" ,
						msg :  erroresMsg.getMessage( "min" , "porcentajePercepcion" )
					}
				}
			} ,
			descuentoGlobal :        {
				type :      DataTypes.DECIMAL( 12 , 2 )  ,
				allowNull : true ,
				validate :  {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "descuentoGlobal" )
					} ,
					isFloat :  {
						args : true ,
						msg :  erroresMsg.getMessage( "isFloat" , "descuentoGlobal" )
					} ,
					max :      {
						args : "999999999999.99" ,
						msg :  erroresMsg.getMessage( "max" , "descuentoGlobal" )
					} ,
					min :      {
						args : "0.00" ,
						msg :  erroresMsg.getMessage( "min" , "descuentoGlobal" )
					}
				}
			} ,
			porcentajeDescuentoGlobal : {
				type :      DataTypes.DECIMAL( 12 , 2 ) ,
				allowNull : true ,
				validate :  {
					notEmpty : {
						args : true ,
						msg :  erroresMsg.getMessage( "notEmpty" , "porcentajeDescuentoGlobal" )
					} ,
					isFloat :  {
						args : true ,
						msg :  erroresMsg.getMessage( "isFloat" , "porcentajeDescuentoGlobal" )
					} ,
					max :      {
						args : "999999999999.99" ,
						msg :  erroresMsg.getMessage( "max" , "porcentajeDescuentoGlobal" )
					} ,
					min :      {
						args : "0.00" ,
						msg :  erroresMsg.getMessage( "min" , "porcentajeDescuentoGlobal" )
					}
				}
			} ,
			guiasRelacionada :          {
				type :      DataTypes.STRING ,
				allowNull : true ,
				get :       function() {
					if( this.getDataValue( 'guiasRelacionada' ) ) {
						return JSON.parse( this.getDataValue( 'guiasRelacionada' ) );
					} else {
						return [];
					}
				} ,
				set :       function( value ) {
					this.setDataValue( 'guiasRelacionada' , JSON.stringify( value ) );
				} ,

				validate : {
					guiaValidation : function( value ) {
						var posibleArray = JSON.parse( value );

						if( !Array.isArray( posibleArray ) || !posibleArray.length > 0 ) {
							throw new Error( "Guias Relacionada : Debe ser un array no vacio" );
						}

						var values = [];

						var types = {
							"09" : 0 ,
							"31" : 0
						};

						posibleArray.forEach( function( item ) {

							var serie  = item.serie || null;
							var numero = item.numero || null;
							var tipo   = item.tipo || null;

							if( Object.keys( item ).length != 3 ) {
								throw new Error( "Guias Relacionada : Solo estan permitidos los campos tipo,numero y serie" );
							}

							if( !serie || !numero || !tipo ) {
								throw new Error( "Guias Relacionada : Debe tener los campos serie,numero y tipo necesariamente" );
							}

							if( !sequelize.Validator.isIn( tipo , catalogoTipoGuia ) ) {
								throw new Error( "Guias Relacionada : Solo se admiten guias de remision remitente o transportista" )
							}

							if( !sequelize.Validator.is( serie , "^G?([A-Z0-9]){3}$" ) ) {
								throw new Error( "Guias Relacionada : Serie no cumple con el estandar" );
							}

							if( !sequelize.Validator.isNumeric( numero ) || !sequelize.Validator.min( numero , 1 ) || !sequelize.Validator.max( numero , 99999999 ) ) {
								throw new Error( "Guias Relacionada : Numero no cumple con el estandar" );
							}

							var id = [ serie , numero , tipo ].join( "-" );

							if( values.indexOf( id ) == -1 ) {
								values.push( id );
							} else {
								throw new Error( "Guias Relacionada : No debe existir guias repetidas" );
							}

							types[ tipo ]++;
						} );

						if( types[ "09" ] * types[ "31" ] != 0 ) {
							throw new Error( "Guias Relacionada : No se puede indicar Guia de remision de remitente y Guia de remision de transportista en el mismo documento " );
						}
					}
				}
			} ,

			docRelacionada :        {
				type :      DataTypes.STRING ,
				allowNull : true ,
				get :       function() {
					if( this.getDataValue( 'docRelacionada' ) ) {
						return JSON.parse( this.getDataValue( 'docRelacionada' ) );
					} else {
						return [];
					}

				} ,
				set :       function( value ) {
					this.setDataValue( 'docRelacionada' , JSON.stringify( value ) );
				} ,
				validate :  {
					docValidation : function( value ) {
						var posibleArray = JSON.parse( value );

						if( !Array.isArray( posibleArray ) || !posibleArray.length > 0 ) {
							throw new Error( "Documentos Relacionados : Debe ser un array no vacio" );
						}

						var values = [];

						posibleArray.forEach( function( item ) {

							var numero = item.numero || null;
							var tipo   = item.tipo || null;

							if( Object.keys( item ).length != 2 ) {
								throw new Error( "Documentos Relacionados : Debe tener solo los campos de numero y tipo" );
							}

							if( !numero || !tipo ) {
								throw new Error( "Documentos Relacionados : Debe tener los campos serie,numero y tipo necesariamente" );
							}

							if( !sequelize.Validator.isIn( tipo , catalogoTipoDocumentoR ) ) {
								throw new Error( "Documentos Relacionados : Solo se pueden emitir los tipos 04,05,99 y 01" )
							}

							if( !sequelize.Validator.is( numero , "^[A-Z0-9\-]+$" ) ) {
								throw new Error( "Documentos Relacionados : Numero no cumple con el estandar" );
							}

							var id = [ numero , tipo ].join( "-" );

							if( values.indexOf( id ) == -1 ) {
								values.push( id );
							} else {
								throw new Error( "Documentos Relacionados : No debe existir documentos repetidos" );
							}

						} );

					}
				}
			} ,
			fechaEmitido : {
				type : DataTypes.DATE ,
				get :  function() {
					var value = this.getDataValue( "fechaEmitido" );
					if( value ) {
						return moment( value ).format( "YYYY-MM-DD hh:mm:ss" );
					}
				}
			} ,
			emitido :        {
				type :         DataTypes.INTEGER ,
				defaultValue : 0
			} ,
			enviado :      {
				type :         DataTypes.INTEGER ,
				defaultValue : 0
			} ,
			rechazado :      {
				type :         DataTypes.INTEGER ,
				defaultValue : 0
			} ,
			motivoRechazo :      {
				type :         DataTypes.STRING ,
				defaultValue : null
			} ,
			baja :           {
				type :         DataTypes.INTEGER ,
				defaultValue : 0
			} ,
			digestValue :    {
				type : DataTypes.STRING
			} ,
			signatureValue : {
				type : DataTypes.BLOB
			} ,
			xml :            {
				type : DataTypes.BLOB
			} ,
			adicional : {
				type : DataTypes.TEXT,
				get : function () {
					var value = this.getDataValue("adicional");
					if ( value ) {
						return JSON.parse(this.getDataValue('adicional'));
					}else{
						return {};
					}
				},
				set : function(value){
					this.setDataValue('adicional' , JSON.stringify(value));
				},
				validate : {
					adicionalValidation : function (value) {

						if(value && !sequelize.Validator.isJSON(value)){
							throw new Error("Adicional : El campo debe tener el formato JSON");
						}
					}
				}
			} ,
			tipoComprobante : {
				type : DataTypes.VIRTUAL ,
				get :  function() {
					return "boleta"
				}
			} ,
			tipoComprobantePolite : {
				type : DataTypes.VIRTUAL ,
				get :  function() {
					return "Boleta";
				}
			} ,
			codigoComprobante :     {
				type : DataTypes.VIRTUAL ,
				get :  function() {
					return "03"
				}
			} ,
			importeLetras :         {
				type : DataTypes.VIRTUAL ,
				get :  function() {
					var self         = this;
					var importeVenta = self.totalVenta;
					if( !importeVenta ) {
						return null;
					}
					var words        = [ utils.numberToWords( importeVenta ).toUpperCase() , "Y" , utils.complementDecimal( importeVenta ) ].join( " " );
					return words;
				}
			} ,
			tipoMonedaHuman :       {
				type : DataTypes.VIRTUAL ,
				get :  function() {
					var self       = this;
					var tipoMoneda = self.tipoMoneda;
					if( !tipoMoneda ) {
						return null;
					}
					return catalogoTipoMoneda.human[ tipoMoneda ].letras;
				}
			} ,
			tipoMonedaSimbolo :     {
				type : DataTypes.VIRTUAL ,
				get :  function() {
					var self       = this;
					var tipoMoneda = self.tipoMoneda;
					if( !tipoMoneda ) {
						return null;
					}
					return catalogoTipoMoneda.human[ tipoMoneda ].simbolo;
				}
			}
		} ,
		{
			tableName :       'clientes_boleta' ,
			validate :        {
				// difEmisorReceptor :   function() {
				// 	var comprobante = this;
				// 	if( comprobante.get( 'idEmisor' ) == comprobante.get( 'idReceptor' ) ) {
				// 		throw new Error( "No se puede enviar un comprobante a si mismo" );
				// 	}
				// } ,
				hasItems :            function() {
					var boleta = this;
					if( !(boleta.get( 'items' ) && Array.isArray( boleta.get( 'items' ) ) && boleta.get( 'items' ).length > 0) ) {
						throw new Error( erroresMsg.getMessage( "items" , "items" ) )
					}
				} ,
				checkReceptor :       function() {
					var boleta = this;
					var receptor = boleta.get('receptor');
					receptor = JSON.parse(receptor);
					if( boleta.get( "totalVenta" ) > 700.00 && !receptor.idReceptor ) {
						throw new Error( "Si el totalVenta supera el monto de 700.00 , debe incluir el campo receptor" );
					}
				} ,
				validarDependientes : function() {
					var comprobante = this;

					//Percepcion
					var importePercepcion    = comprobante.importePercepcion;
					var porcentajePercepcion = comprobante.porcentajePercepcion;

					if( importePercepcion != null && importePercepcion > 0 || porcentajePercepcion != null && porcentajePercepcion > 0 ) {
						if( !(importePercepcion != null && importePercepcion > 0 && porcentajePercepcion > 0 && porcentajePercepcion != null) ) {
							throw new Error( "Si se especifica la percepcion , debe especificar los campos porcentajePercepcion e importePercepcion" );
						}
					}

					//Descuento

					var descuentoGlobal     = comprobante.descuentoGlobal;
					var totalDescuentos = comprobante.totalDescuento;
					var porcentajeDscGlobal = comprobante.porcentajeDescuentoGlobal;
					if (comprobante.descuentoGlobal && comprobante.porcentajeDescuentoGlobal && comprobante.totalDescuento) {
						if( descuentoGlobal != null && descuentoGlobal > 0 || totalDescuentos != null && totalDescuentos > 0 || porcentajeDscGlobal != null && porcentajeDscGlobal > 0 ) {
							if( !(descuentoGlobal != null && descuentoGlobal > 0 && totalDescuentos != null && totalDescuentos > 0 && porcentajeDscGlobal != null && porcentajeDscGlobal > 0) ) {
								throw new Error( "Si se especifica el descuento global , debe especificar los campos descuentoGlobal,porcentajeDescuentoGlobal y totalDescuento " );
							}
						}
					}		
				} ,
				calcTotales :         function() {
					var boleta = this;

					var totalVentaGravada   = parseFloat( boleta.get( 'totalVentaGravada' ) || 0 );
					var totalVentaInafecta = parseFloat( boleta.get( 'totalVentaInafecta' ) || 0 );
					var totalVentaExonerada = parseFloat( boleta.get( 'totalVentaExonerada' ) || 0 );
					var totalVentaGratuita  = parseFloat( boleta.get( 'totalVentaGratuita' ) || 0 );

					var sumatoriaIgv           = parseFloat( boleta.get( 'sumatoriaIgv' ) || 0 );
					var sumatoriaIsc = parseFloat( boleta.get( 'sumatoriaIsc' ) || 0 );
					var sumatoriaOtrosCargos = parseFloat( boleta.get( 'sumatoriaOtrosCargos' ) || 0 );
					var sumatoriaOtrosTributos = parseFloat( boleta.get( 'sumatoriaOtrosTributos' ) || 0 );
					var sumatoriaAnticipos     = parseFloat(boleta.getTotalAnticipos() || 0);

					var totalVenta = parseFloat( boleta.get( 'totalVenta' ) || 0.00 );

					var porcentajeDescuentoGlobal = parseFloat( boleta.get( "porcentajeDescuentoGlobal" ) || 0.00 );
					var descuentoGlobal           = parseFloat( boleta.get( "descuentoGlobal" ) || 0.00 );

					var totalDescuento = parseFloat( boleta.get( "totalDescuento" ) || 0.00 );
					// Calculo de la sumatoria de IGV
					var calcTotalIgv = parseFloat( (utils.igvPercent * (totalVentaGravada + sumatoriaIsc)).toFixed( 2 ) );
					if(sumatoriaAnticipos > 0){
						calcTotalIgv -= parseFloat(((sumatoriaAnticipos/utils.igvValue)*utils.igvPercent).toFixed(2)); 
					}
					if(sumatoriaIgv > 0){
						if( !utils.equalsDecimal( calcTotalIgv , sumatoriaIgv ) ) {
							throw new Error( "Error en el calculo del IGV  : " + sumatoriaIgv );
						}
					}	
					var calcTotalVenta = parseFloat( (totalVentaGravada + totalVentaExonerada + totalVentaInafecta + sumatoriaIgv + sumatoriaIsc + sumatoriaOtrosCargos + sumatoriaOtrosTributos ).toFixed( 2 ) );

					//Calculo del importe total de venta en base a los subtotales
					if( !utils.equalsDecimal( calcTotalVenta , totalVenta ) ) {
						throw new Error( "Error en el calculo del total venta  : " + totalVenta );
					}

					var items = boleta.get( 'items' ) || [];
					//Calculo de subtotales
					var objTotales = items.reduce( function( objPrev , item ) {
						var tipoAfectacionIgv       = item.get( 'tipoAfectacionIgv' ) || 10;
						var montoAfectacionIgv = item.get( 'montoAfectacionIgv' ) || 0.00;
						var montoAfectacionIsc = item.get( "montoAfectacionIsc" ) || 0.00;
						var tipoPrecioVentaUnitario = item.get( 'tipoPrecioVentaUnitario' ) || "01";
						var precioVentaUnitario     = item.get( 'precioVentaUnitario' ) || 0.00;
						var cantidad                = item.get( 'cantidad' ) || 0.00;
						var descuentoItem           = item.get( "descuento" ) || 0.00;

						objPrev.sumDescuentos += parseFloat( descuentoItem );
						objPrev.sumIgv += parseFloat( montoAfectacionIgv );
						objPrev.sumIsc += parseFloat( montoAfectacionIsc );

						if( tipoPrecioVentaUnitario == '01' ) {
							switch( true ) {
								case (tipoAfectacionIgv < 20 ):
									objPrev.calcTotalVentaGravada += parseFloat( item.get( 'valorVenta' ) );
									break;
								case (tipoAfectacionIgv == 20 ):
									objPrev.calcTotalVentaExonerada += parseFloat( item.get( 'valorVenta' ) );
									break;
								case (tipoAfectacionIgv < 40 ):
									objPrev.calcTotalVentaInafecta += parseFloat( item.get( 'valorVenta' ) );
									break;
								case (tipoAfectacionIgv == 40 ):
									objPrev.calcTotalVentaInafecta += parseFloat(item.get('valorVenta'));
									break;
							}
						} else {
							objPrev.calcTotalVentaGratuita += parseFloat( precioVentaUnitario * cantidad );
						}

						return objPrev;
					} , {
						calcTotalVentaGravada :   0.00 ,
						calcTotalVentaInafecta :  0.00 ,
						calcTotalVentaExonerada : 0.00 ,
						calcTotalVentaGratuita :  0.00 ,
						sumDescuentos :           0.00 ,
						sumIgv :                  0.00 ,
						sumIsc :                  0.00
					} );

					objTotales.calcTotalVentaGravada -= (porcentajeDescuentoGlobal) * 0.01 * objTotales.calcTotalVentaGravada;
					objTotales.calcTotalVentaExonerada -= (porcentajeDescuentoGlobal) * 0.01 * objTotales.calcTotalVentaExonerada;
					objTotales.calcTotalVentaInafecta -= (porcentajeDescuentoGlobal) * 0.01 * objTotales.calcTotalVentaInafecta;
					objTotales.sumIgv -= (porcentajeDescuentoGlobal) * 0.01 * objTotales.sumIgv;
					objTotales.sumIsc -= (porcentajeDescuentoGlobal) * 0.01 * objTotales.sumIsc;

					//Anticipos
					objTotales.calcTotalVentaGravada -= (sumatoriaAnticipos/utils.igvValue);
					//Calc Sum IGV ITEM
					if( !utils.equalsDecimal( objTotales.sumIgv , sumatoriaIgv ) ) {
						throw new Error( "Error en el calculo de IGV , sumatoria de Items no es la misma : "+sumatoriaIgv);
					}

					//calc sum ISC ITEM
					if( !utils.equalsDecimal( objTotales.sumIsc , sumatoriaIsc ) ) {
						throw new Error( "Error en el calculo de ISC , sumatoria de Items no es la misma " );
					}

					//calc sum totalVentaGravada
					if( !utils.equalsDecimal( objTotales.calcTotalVentaGravada , totalVentaGravada ) ) {
						throw new Error( "Error en el calculo de totalVentaGravada, sumatoria de Items no es la misma " );
					}

					//calc sum totalVentaExonerada
					if( !utils.equalsDecimal( objTotales.calcTotalVentaExonerada , totalVentaExonerada ) ) {

						throw new Error( "Error en el calculo de totalVentaExonerada , sumatoria de Items no es la misma " );
					}

					//calc sum totalVentaInafecta
					if( !utils.equalsDecimal( objTotales.calcTotalVentaInafecta , totalVentaInafecta ) ) {
						throw new Error( "Error en el calculo de totalVentaInafecta , sumatoria de Items no es la misma " );
					}

					//cacl sum totalVentaGratuita
					if( !utils.equalsDecimal( objTotales.calcTotalVentaGratuita , totalVentaGratuita ) ) {
						throw new Error( "Error en el calculo de totalVentaGratuita , sumatoria de items no es la misma" );
					}

					var calcTotalDescuentos = objTotales.sumDescuentos + descuentoGlobal;

					// if( !utils.equalsDecimal( calcTotalDescuentos , totalDescuento ) ) {
					// 	throw new Error( "Error en el calculo de totalDescuento , sumatoria de descuento por item y descuento global no concuerda" );
					// }

				}
				,
				isUnique :            function() {
					// var self        = this;
					// var serieReq = self.get( 'serie' ) || null;
					// var idEmisorReq = self.get( 'idEmisor' ) || null;
					// var numeroReq   = self.get( "numero" ) || null;
					// var idboleta    = self.get( "idBoleta" ) || null;
                    //
					// if( !idboleta && numeroReq ) {
					// 	return Boleta.findOne( {
					// 			where : {
					// 				serie :    serieReq ,
					// 				idEmisor : idEmisorReq ,
					// 				numero :   numeroReq
					// 			}
					// 		} )
					// 		.then( function( lastboleta ) {
					// 			if( lastboleta ) {
					// 				throw new Error( "La boleta " + serieReq + "-" + numeroReq + " ya ha sido emitida" );
					// 			}
					// 		} )
					// 		.catch( function( error ) {
					// 			throw error;
					// 		} );
					// }
				}

			}
			,
			hooks :           {
				beforeCreate : [
					function( boleta , options ) {

						var codigoLeyendaVentaGratuita = 1002;
						var codigoLeyendaPercepcion    = 2000;

						//Vemos ventas gratuitas
						var totalVenta = boleta.totalVenta;
						var leyendas   = [];

						if( totalVenta != null && totalVenta == 0 ) {
							leyendas.push( codigoLeyendaVentaGratuita );
						}

						//Vemos la percepcion
						var importePercepcion = boleta.importePercepcion;
						if( importePercepcion != null && importePercepcion > 0.00 ) {
							leyendas.push( codigoLeyendaPercepcion );
						}

						boleta.set( "leyendas" , leyendas );
					}
				]
			}
			,
			instanceMethods : {
				getXmlField :          function() {
					var self = this;
					var id   = self.idBoleta;

					return Boleta.find( {
						where :      { idBoleta : id } ,
						attributes : { exlclude : [ "xml" ] }
					} ).then( function( comp ) {
							if( !comp || !comp.xml ) {
								return sequelize.Promise.resolve( null );
							}
							return sequelize.Promise.resolve( comp.xml );
						} )
						.catch( function( err ) {
							return sequelize.Promise.reject( err );
						} );
				} ,
				estaDeBaja :           function() {
					var self = this;
					return self.baja == 1;
				} ,
				pretty :               function() {
					var self = this;
					self     = self.get( { plain : true } );
					utils.deleteNullProperties( self );
					return self;
				}
				,
				estaEmitido :          function() {
					var self = this;
					return self.emitido == 1;
				}
				,
				getImporteLetras :     function() {
					var self         = this;
					var importeVenta = self.totalVenta;
					var words        = [ utils.numberToWords( importeVenta ).toUpperCase() , "Y" , utils.complementDecimal( importeVenta ) ].join( " " );
					return words;
				}
				,
				getMonedaLetras :      function() {
					var self       = this;
					var tipoMoneda = self.tipoMoneda;
					return catalogoTipoMoneda.human[ tipoMoneda ].letras;
				}
				,
				getMonedaSimbolo :     function() {
					var self       = this;
					var tipoMoneda = self.tipoMoneda;
					return catalogoTipoMoneda.human[ tipoMoneda ].simbolo;
				}
				,
				getFechaFormatoSunat : function() {
					var self = this;
					return moment( self.fechaEmision ).format( "YYYY-MM-DD" );
				}
				,
				getNombreBase :        function() {
					var self = this;
					if( !self.emisor ) {
						throw new Error( "La boleta no tiene asociada un emisor" );
					}
					return [ self.get( 'idEmisor' ) , self.get( 'codigoComprobante' ) , self.get( 'serie' ) , self.get( 'numero' ) ].join( "-" );
				}
				,
				getPdf417Code :        function() {
					var self         = this;
					var tipoReceptor = self.receptor.tipo || "-";
					var idReceptor   = self.receptor.idReceptor || "-";
					var text         = [
						self.get( 'idEmisor' ) ,
						self.get( 'codigoComprobante' ) ,
						self.get( 'serie' ) ,
						self.get( 'numero' ) ,
						self.get( 'sumatoriaIgv' ) ,
						self.get( 'totalVenta' ) ,
						self.getFechaFormatoSunat() ,
						tipoReceptor ,
						idReceptor ,
						self.get( 'digestValue' ) ,
						self.get( 'signatureValue' ) ].join( "|" );

					return text;
				}
				,
				getQrCode :            function() {
					var self         = this;
					var tipoReceptor = (self.receptor) ? self.receptor.tipo : "-";
					var idReceptor   = (self.receptor) ? self.receptor.idReceptor : "-";
					var text         = [
						self.get( 'idEmisor' ) ,
						self.get( 'codigoComprobante' ) ,
						self.get( 'serie' ) ,
						self.get( 'numero' ) ,
						self.get( 'sumatoriaIgv' ) ,
						self.get( 'totalVenta' ) ,
						self.getFechaFormatoSunat() ,
						tipoReceptor ,
						idReceptor ,
					].join( "|" );

					return text;
				},
				getTotalAnticipos : function(){
					var self = this;
					if(self.adicional && self.adicional.documentosPrepagados && Array.isArray(self.adicional.documentosPrepagados)){
						var total = self.adicional.documentosPrepagados.reduce(function(prev,curr){
							if(curr.monto!=null){
								return prev+parseFloat(curr.monto);
							}
						},0);
						return total;
					}else{
						return 0;
					}
				}
			}
			,
			classMethods :    {
				associate :                  function( models ) {
					Boleta.hasMany( models.BoletaItem , {
						foreignKey : 'idBoleta' ,
						as :         { singular : 'item' , plural : 'items' }
					} );
					// Boleta.hasOne( models.BoletaExtra , { foreignKey : "idBoleta" , as : "extra" } );
					// Boleta.belongsTo( models.Usuario , { foreignKey : 'idUsuario' , as : "usuario" } );
					// Boleta.belongsTo( models.Emisor , { foreignKey : 'idEmisor' , as : "emisor" } );
					// Boleta.belongsTo( models.Receptor , { foreignKey : 'idReceptor' , as : "receptor" } );
					// Boleta.belongsTo( models.Constancia , { foreignKey : 'idConstancia' , as : "constancia" } );
				}
				,
				getFieldsNotSave :           function() {
					return [ "idBoleta" , "fechaEmitido" , "emitido" , "digestValue" , "signatureValue" , "baja" ];
				} ,
				getFieldsToQuery :           function() {
					var notSave    = [ "idBoleta" , "leyendas" , "digestValue" , "signatureValue" , "guiasRelacionada" , "docRelacionada" ];
					var attr    = Object.keys( this.attributes );
					var saveFields = attr.filter( function( attribute ) {

						if( notSave.indexOf( attribute ) == -1 ) {
							return true;
						}
					} );
					return saveFields;
				} ,
				findOneWithAllDataById :     function( idBoleta ) {
					return Boleta.findOne( {
						where :   { idBoleta : idBoleta } ,
						include : [
							{
								model : sequelize.models.BoletaItem ,
								as :    "items"
							} ,
							// {
							// 	model : sequelize.models.BoletaExtra ,
							// 	as :    "extra"
							// } ,
							// {
							// 	model :      sequelize.models.Emisor ,
							// 	as :         "emisor" ,
							// 	attributes : [ "idEmisor" , 'razonSocial' , "nombreComercial" ] ,
							// 	include :    [
							// 		{
							// 			model :      sequelize.models.Domicilio ,
							// 			as :         "domicilio" ,
							// 			attributes : [ "departamento" , "provincia" , "distrito" , "direccion" , "ubigeo" ]
							// 		} ,
							// 		{
							// 			model : sequelize.models.EmisorConfiguracion ,
							// 			as :    "configuracion"
							// 		} ,
							// 		{
							// 			model :      sequelize.models.UsuarioSunat ,
							// 			as :         "usuariosunat" ,
							// 			attributes : [ "username" , "password" ]
							// 		}
							// 	]
							// } ,
							// {
							// 	model :      sequelize.models.Receptor ,
							// 	attributes : [ "idReceptor" , 'tipo' , "razonSocial" ] ,
							// 	as :         "receptor"
							// }
						]
					} );
				},
				findOneNoEmitedWithAllData :     function() {
					return Boleta.findOne( {
						where :      { enviado : false,rechazado : false },
						include :    [
							{
								model : sequelize.models.BoletaItem ,
								as :    "items"
							}
						]
					} );
				} ,
				findAllNoEmitedWithAllData :     function() {
					return Boleta.findAll( {
						where :      { enviado : true,rechazado : true },
						include :    [
							{
								model : sequelize.models.BoletaItem ,
								as :    "items"
							}
						]
					} );
				} ,
				findAllTodayReportWithAllData :     function() {
					var startday = new Date();
					startday.setHours(0,0,0,0);
					return Boleta.findAll( {
						where :       ['fechaEmision > ?', startday] ,
						// attributes : {
						// 	// exclude : [ "xml" ]
						// } ,
						attributes: ['fechaEmision','serie', 'numero','emitido','enviado','rechazado','motivoRechazo']
					} );
				}
				// findOneBySerieNumeroEmisor : function( serie , numero , idEmisor ) {
				// 	return Boleta.findOne( {
				// 		where :   {
				// 			serie :    serie ,
				// 			numero :   numero ,
				// 			idEmisor : idEmisor
				// 		} ,
				// 		include : [
				// 			{
				// 				model : sequelize.models.BoletaItem ,
				// 				as :    "items"
				// 			} ,
				// 			{
				// 				model : sequelize.models.BoletaExtra ,
				// 				as :    "extra"
				// 			}
				// 		]
				// 	} );
				// } ,
				// findOneById :                function( idBoleta , idEmisor ) {
				// 	return Boleta.findOne( {
				// 		where :   {
				// 			idboleta : idBoleta
				// 		} ,
				// 		include : [
				// 			{
				// 				model : sequelize.models.BoletaExtra ,
				// 				as :    "extra"
				// 			} ,
				// 			{
				// 				model : sequelize.models.BoletaItem ,
				// 				as :    "items"
				// 			} ,
				// 			{
				// 				model : sequelize.models.Receptor ,
				// 				as :    "receptor"
				// 			}
				// 		]
				// 	} );
				// } ,
				// findAllWithAddData :         function( idEmisor , query ) {
				// 	if( typeof query !== "object" ) {
				// 		throw new Error( "Los querys deben sere objetos" );
				// 	}
                //
				// 	if( !query.where ) {
				// 		throw new Error( "Los querys deben tener al menos el attribute where" );
				// 	}
                //
				// 	query.where.idEmisor = idEmisor;
				// 	query.include        = [
				// 		{
				// 			model : sequelize.models.BoletaExtra ,
				// 			as :    "extra"
				// 		} ,
				// 		{
				// 			model : sequelize.models.BoletaItem ,
				// 			as :    "items"
				// 		} ,
				// 		{
				// 			model : sequelize.models.Receptor ,
				// 			as :    "receptor"
				// 		}
				// 	];
				// 	return Boleta.findAll( query )
				// }
			}
		}
	    )
		;

	return Boleta;
};
