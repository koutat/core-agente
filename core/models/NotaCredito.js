var catalogoTipoMoneda     = require('../catalogos/tipoMoneda.json');
var catalogoTipoGuia       = require('../catalogos/tipoGuia.json').codigos;
var catalogoTipoDocumentoR = require('../catalogos/tipoDocumentoR.json').codigos;
var catalogoNotaCredito    = require('../catalogos/tipoNotaCredito.json').codigos;
var erroresMsg             = require("../error/index").validationMessage;
var utils                  = require("../utils");
var moment                 = require("moment");
var util                   = require("util");
module.exports             = function (sequelize, DataTypes) {

    var NotaCredito = sequelize.define('NotaCredito', {

            idNotaCredito: {
                type: DataTypes.BIGINT,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            fechaEmision: {
                type: DataTypes.DATE,
                defaultValue: DataTypes.NOW,
                allowNull: true,
                get: function () {
                    var value = this.getDataValue("fechaEmision");
                    if (value) {
                        return moment(value).format("YYYY-MM-DD hh:mm:ss");
                    }
                }
            },
            fechaEmitido: {
                type: DataTypes.DATE,
                get: function () {
                    var value = this.getDataValue("fechaEmitido");
                    if (value) {
                        return moment(value).format("YYYY-MM-DD hh:mm:ss");
                    }
                }
            },
            tipoNotaCredito: {
                type: DataTypes.STRING,
                allowNull: false,
                defaultValue: "",
                validate: {
                    notEmpty: {
                        args: true,
                        msg: erroresMsg.getMessage("notEmpty", "tipoNotaCredito")
                    },
                    isIn: {
                        args: [catalogoNotaCredito],
                        msg: erroresMsg.getMessage("isIn", "tipoNotaCredito")
                    }
                }
            },
            tipoComprobanteAfectado: {
                type: DataTypes.STRING,
                allowNull: false,
                defaultValue: "",
                validate: {
                    isIn: {
                        args: [["01", "03"]],
                        msg: erroresMsg.getMessage("isIn", "tipoComprobanteAfectado")
                    },
                    afectado: function () {
                        factura = this;
                        if (factura.serie[0] == 'F')
                            if (factura.tipoComprobanteAfectado != "01")
                                throw new Error("El comprobante afectado no concuerda con la serie proporcionada");
                            else if (factura.serie[0] == 'B')
                                if (factura.tipoComprobanteAfectado != "03")
                                    throw new Error("El comprobante afectado no concuerda con la serie proporcionada");
                    }
                }
            },
            serieAfectado: {
                type: DataTypes.STRING,
                allowNull: false,
                defaultValue: "",
                validate: {

                    notEmpty: {
                        args: true,
                        msg: erroresMsg.getMessage("notEmpty", "serie")
                    },
                    len: {
                        args: [4, 4],
                        msg: erroresMsg.getMessage("len2", "serieAfectado", 4)
                    },
                    isAlphanumeric: {
                        args: true,
                        msg: erroresMsg.getMessage("isAlphanumeric", "serie")
                    },
                    is: {
                        args: ["^(F|B)([A-Z0-9]){3}$"],
                        msg: erroresMsg.getMessage("pattern", "serieAfectado")
                    }

                }
            },
            numeroAfectado: {
                type: DataTypes.STRING,
                allowNull: false,
                defaultValue: "",
                validate: {
                    notEmpty: {
                        args: true,
                        msg: erroresMsg.getMessage("notEmpty", "numeroAfectado")
                    },
                    isInt: {
                        args: true,
                        msg: erroresMsg.getMessage("isInt", "numeroAfectado")
                    },
                    max: {
                        args: "99999999",
                        msg: erroresMsg.getMessage("max", "numeroAfectado")
                    },
                    min: {
                        args: "1",
                        msg: erroresMsg.getMessage("min", "numeroAfectado")
                    }
                }
            },
            serie: {
                type: DataTypes.STRING,
                allowNull: false,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: erroresMsg.getMessage("notEmpty", "serie")
                    },
                    isAlphanumeric: {
                        args: true,
                        msg: erroresMsg.getMessage("isAlphanumeric", "serie")
                    },
                    len: {
                        args: [4, 4],
                        msg: erroresMsg.getMessage("len2", "serie", 4)
                    },
                    is: {
                        args: ["^(F|B)([A-Z0-9]){3}$"],
                        msg: erroresMsg.getMessage("pattern", "serie")
                    }
                }
            },
            numero: {
                type: DataTypes.INTEGER,
                allowNull: false,
                validate: {
                    isInt: {
                        args: true,
                        msg: erroresMsg.getMessage("isInt", "numero")
                    },
                    max: {
                        args: "99999999",
                        msg: erroresMsg.getMessage("max", "numero")
                    },
                    min: {
                        args: "1",
                        msg: erroresMsg.getMessage("min", "numero")
                    }
                }
            },
            receptor: {
                type: DataTypes.STRING,
                allowNull: true,
                validate: {
                    validarReceptor: function () {
                        var factura  = this;
                        var receptor = factura.get('receptor');
                        receptor     = JSON.parse(receptor);
                        if (receptor) {
                            if (receptor.idReceptor && receptor.tipo) {
                                switch (receptor.tipo) {
                                    // case 6:if(!utils.validarReceptorRuc(receptor.idReceptor))throw new Error("Ruc incorrecto");break;
                                    case "1":
                                        if (!utils.validarReceptorDni(receptor.idReceptor))throw new Error("Dni incorrecto");
                                        break;
                                    case "6":
                                        if (!utils.validarReceptorRuc(receptor.idReceptor, receptor.tipo))throw new Error("Ruc incorrecto");
                                        break;
                                    case "7":
                                        if (!utils.validarReceptorOtrosDoc(receptor.idReceptor))throw new Error("documento incorrecto");
                                        break;
                                    case "A":
                                        if (!utils.validarReceptorOtrosDoc(receptor.idReceptor))throw new Error("documento incorrecto");
                                        break;
                                    default :
                                        throw  new Error("El tipo de documento del Receptor no cumple con las especificaciones");
                                }
                            } else throw new Error("Si especifica el campo receptor, debe especificar, el numero de documento y el tipo");
                        }
                    }
                }
            },
            descripcion: {
                type: DataTypes.STRING,
                allowNull: false,
                defaultValue: '',
                validate: {
                    notEmpty: {
                        args: true,
                        msg: erroresMsg.getMessage("notEmpty", "descripcion")
                    },
                    len: {
                        args: [1, 250],
                        msg: erroresMsg.getMessage("len", "descripcion", 1, 250)
                    }
                }
            },
            //REVISAR LA POSIBILIDAD DE PERMITIIR NULL
            totalVentaGravada: {
                type: DataTypes.DECIMAL(12, 2),
                allowNull: false,
                defaultValue: 0,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: erroresMsg.getMessage("notEmpty", "totalVentaGravada")
                    },
                    isFloat: {
                        args: true,
                        msg: erroresMsg.getMessage("isFloat", "totalVentaGravada")
                    },
                    max: {
                        args: "999999999999.99",
                        msg: erroresMsg.getMessage("max", "totalVentaGravada")
                    },
                    min: {
                        args: "0.00",
                        msg: erroresMsg.getMessage("min", "totalVentaGravada")
                    }
                }
            },
            //REVISAR LA POSIBILIDAD DE PERMITIIR NULL
            totalVentaInafecta: {
                type: DataTypes.DECIMAL(12, 2),
                allowNull: true,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: erroresMsg.getMessage("notEmpty", "totalVentaInafecta")
                    },
                    isFloat: {
                        args: true,
                        msg: erroresMsg.getMessage("isFloat", "totalVentaInafecta")
                    },
                    max: {
                        args: "999999999999.99",
                        msg: erroresMsg.getMessage("max", "totalVentaInafecta")
                    },
                    min: {
                        args: "0.00",
                        msg: erroresMsg.getMessage("min", "totalVentaInafecta")
                    }
                }
            },
            //REVISAR LA POSIBILIDAD DE PERMITIIR NULL
            totalVentaExonerada: {
                type: DataTypes.DECIMAL(12, 2),
                allowNull: true,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: erroresMsg.getMessage("notEmpty", "totalVentaExonerada")
                    },
                    isFloat: {
                        args: true,
                        msg: erroresMsg.getMessage("isFloat", "totalVentaExonerada")
                    },
                    max: {
                        args: "999999999999.99",
                        msg: erroresMsg.getMessage("max", "totalVentaExonerada")
                    },
                    min: {
                        args: "0.00",
                        msg: erroresMsg.getMessage("min", "totalVentaExonerada")
                    }
                }
            },
            //REVISAR SI ES VERDAD QUE SE PUEDE PERMITIR NULL
            sumatoriaIgv: {
                type: DataTypes.DECIMAL(12, 2),
                allowNull: false,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: erroresMsg.getMessage("notEmpty", "sumatoriaIgv")
                    },
                    isFloat: {
                        args: true,
                        msg: erroresMsg.getMessage("isFloat", "sumatoriaIgv")
                    },
                    max: {
                        args: "999999999999.99",
                        msg: erroresMsg.getMessage("max", "sumatoriaIgv")
                    },
                    min: {
                        args: "0.00",
                        msg: erroresMsg.getMessage("min", "sumatoriaIgv")
                    }
                }
            },
            //REVISAR SI ES VERDAD QUE SE PUEDE PERMITIR NULL
            sumatoriaIsc: {
                type: DataTypes.DECIMAL(12, 2),
                allowNull: true,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: erroresMsg.getMessage("notEmpty", "sumatoriaIsc")
                    },
                    isFloat: {
                        args: true,
                        msg: erroresMsg.getMessage("isFloat", "sumatoriaIsc")
                    },
                    max: {
                        args: "999999999999.99",
                        msg: erroresMsg.getMessage("max", "sumatoriaIsc")
                    },
                    min: {
                        args: "0.00",
                        msg: erroresMsg.getMessage("min", "sumatoriaIsc")
                    }
                }
            },
            sumatoriaOtrosTributos: {
                type: DataTypes.DECIMAL(12, 2),
                allowNull: true,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: erroresMsg.getMessage("notEmpty", "sumatoriaOtros")
                    },
                    isFloat: {
                        args: true,
                        msg: erroresMsg.getMessage("isFloat", "sumatoriaOtros")
                    },
                    max: {
                        args: "999999999999.99",
                        msg: erroresMsg.getMessage("max", "sumatoriaOtros")
                    },
                    min: {
                        args: "0.00",
                        msg: erroresMsg.getMessage("min", "sumatoriaOtros")
                    }
                }
            },
            sumatoriaOtrosCargos: {
                type: DataTypes.DECIMAL(12, 2),
                allowNull: true,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: erroresMsg.getMessage("notEmpty", "sumatoriaOtrosCargos")
                    },
                    isFloat: {
                        args: true,
                        msg: erroresMsg.getMessage("isFloat", "sumatoriaOtrosCargos")
                    },
                    max: {
                        args: "999999999999.99",
                        msg: erroresMsg.getMessage("max", "sumatoriaOtrosCargos")
                    },
                    min: {
                        args: "0.00",
                        msg: erroresMsg.getMessage("min", "sumatoriaOtrosCargos")
                    }
                }
            },
            totalDescuento: {
                type: DataTypes.DECIMAL(12, 2),
                allowNull: true,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: erroresMsg.getMessage("notEmpty", "totalDescuento")
                    },
                    isFloat: {
                        args: true,
                        msg: erroresMsg.getMessage("isFloat", "totalDescuento")
                    },
                    max: {
                        args: "999999999999.99",
                        msg: erroresMsg.getMessage("max", "totalDescuento")
                    },
                    min: {
                        args: "0.00",
                        msg: erroresMsg.getMessage("min", "totalDescuento")
                    }
                }
            },
            totalVenta: {
                type: DataTypes.DECIMAL(12, 2),
                allowNull: false,
                defaultValue: 0,
                validate: {
                    notEmpty: {
                        args: true,
                        msg: erroresMsg.getMessage("notEmpty", "totalVenta")
                    },
                    isFloat: {
                        args: true,
                        msg: erroresMsg.getMessage("isFloat", "totalVenta")
                    },
                    max: {
                        args: "999999999999.99",
                        msg: erroresMsg.getMessage("max", "totalVenta")
                    },
                    min: {
                        args: "0.00",
                        msg: erroresMsg.getMessage("min", "totalVenta")
                    }
                }
            },
            tipoMoneda: {
                type: DataTypes.STRING,
                allowNull: false,
                defaultValue: "",
                validate: {
                    notEmpty: {
                        args: true,
                        msg: erroresMsg.getMessage("notEmpty", "tipoMoneda")
                    },
                    isAlphanumeric: {
                        args: true,
                        msg: erroresMsg.getMessage("isAlphanumeric", "tipoMoneda")
                    },
                    isIn: {
                        args: [catalogoTipoMoneda.codigos],
                        msg: erroresMsg.getMessage("isIn", "tipoMoneda")
                    }
                }
            },
            guiasRelacionada: {
                type: DataTypes.STRING,
                allowNull: true,
                get: function () {
                    if (this.getDataValue('guiasRelacionada')) {
                        return JSON.parse(this.getDataValue('guiasRelacionada'));
                    } else {
                        return [];
                    }
                },
                set: function (value) {
                    this.setDataValue('guiasRelacionada', JSON.stringify(value));
                },

                validate: {
                    guiaValidation: function (value) {

                        var posibleArray = JSON.parse(value);

                        if (!Array.isArray(posibleArray) || !posibleArray.length > 0) {
                            throw new Error("Guias Relacionada : Debe ser un array no vacio");
                        }

                        var values = [];

                        var types = {
                            "09": 0,
                            "31": 0
                        };

                        posibleArray.forEach(function (item) {

                            var serie  = item.serie || null;
                            var numero = item.numero || null;
                            var tipo   = item.tipo || null;

                            if (Object.keys(item).length != 3) {
                                throw new Error("Guias Relacionada : Solo estan permitidos los campos tipo,numero y serie");
                            }

                            if (!serie || !numero || !tipo) {
                                throw new Error("Guias Relacionada : Debe tener los campos serie,numero y tipo necesariamente");
                            }

                            if (!sequelize.Validator.isIn(tipo, catalogoTipoGuia)) {
                                throw new Error("Guias Relacionada : Solo se admiten guias de remision remitente o transportista")
                            }

                            if (!sequelize.Validator.is(serie, "^G?([A-Z0-9]){3}$")) {
                                throw new Error("Guias Relacionada : Serie no cumple con el estandar");
                            }

                            if (!sequelize.Validator.isNumeric(numero) || !sequelize.Validator.min(numero, 1) || !sequelize.Validator.max(numero, 99999999)) {
                                throw new Error("Guias Relacionada : Numero no cumple con el estandar");
                            }

                            var id = [serie, numero, tipo].join("-");

                            if (values.indexOf(id) == -1) {
                                values.push(id);
                            } else {
                                throw new Error("Guias Relacionada : No debe existir guias repetidas");
                            }

                            types[tipo]++;
                        });

                        if (types["09"] * types["31"] != 0) {
                            throw new Error("Guias Relacionada : No se puede indicar Guia de remision de remitente y Guia de remision de transportista en el mismo documento ");
                        }
                    }
                }
            },

            docRelacionada: {
                type: DataTypes.STRING,
                allowNull: true,
                get: function () {
                    if (this.getDataValue('docRelacionada')) {
                        return JSON.parse(this.getDataValue('docRelacionada'));
                    } else {
                        return [];
                    }

                },
                set: function (value) {
                    this.setDataValue('docRelacionada', JSON.stringify(value));
                },
                validate: {
                    docValidation: function (value) {
                        var posibleArray = JSON.parse(value);

                        if (!Array.isArray(posibleArray) || !posibleArray.length > 0) {
                            throw new Error("Documentos Relacionados : Debe ser un array no vacio");
                        }

                        var values = [];

                        posibleArray.forEach(function (item) {

                            var numero = item.numero || null;
                            var tipo   = item.tipo || null;

                            if (Object.keys(item).length != 2) {
                                throw new Error("Documentos Relacionados : Debe tener solo los campos de numero y tipo");
                            }

                            if (!numero || !tipo) {
                                throw new Error("Documentos Relacionados : Debe tener los campos numero y tipo necesariamente");
                            }

                            if (!sequelize.Validator.isIn(tipo, catalogoTipoDocumentoR)) {
                                throw new Error("Documentos Relacionados : Solo se pueden emitir los tipos 04,05,99 y 01")
                            }

                            if (!sequelize.Validator.is(numero, "^[A-Z0-9\-]+$")) {
                                throw new Error("Documentos Relacionados : Numero no cumple con el estandar");
                            }

                            var id = [numero, tipo].join("-");

                            if (values.indexOf(id) == -1) {
                                values.push(id);
                            } else {
                                throw new Error("Documentos Relacionados : No debe existir documentos repetidos");
                            }

                        });

                    }
                }
            },
            emitido: {
                type: DataTypes.INTEGER
            },
            enviado: {
                type: DataTypes.INTEGER,
                defaultValue: 0
            },
            rechazado: {
                type: DataTypes.INTEGER,
                defaultValue: 0
            },
            motivoRechazo: {
                type: DataTypes.STRING,
                defaultValue: null
            },
            digestValue: {
                type: DataTypes.STRING
            },
            signatureValue: {
                type: DataTypes.BLOB
            },
            xml: {
                type: DataTypes.BLOB
            },
            tipoComprobante: {
                type: DataTypes.VIRTUAL,
                get: function () {
                    return "notacredito"
                }
            },
            tipoComprobantePolite: {
                type: DataTypes.VIRTUAL,
                get: function () {
                    return "Nota de Credito";
                }
            },
            codigoComprobante: {
                type: DataTypes.VIRTUAL,
                get: function () {
                    return "07"
                }
            },
            importeLetras: {
                type: DataTypes.VIRTUAL,
                get: function () {
                    var self         = this;
                    var importeVenta = self.totalVenta;
                    if (!importeVenta) {
                        return null;
                    }
                    var words = [utils.numberToWords(importeVenta).toUpperCase(), "Y", utils.complementDecimal(importeVenta)].join(" ");
                    return words;
                }
            },
            tipoMonedaHuman: {
                type: DataTypes.VIRTUAL,
                get: function () {
                    var self       = this;
                    var tipoMoneda = self.tipoMoneda;
                    if (!tipoMoneda) {
                        return null;
                    }
                    return catalogoTipoMoneda.human[tipoMoneda].letras;
                }
            },
            tipoMonedaSimbolo: {
                type: DataTypes.VIRTUAL,
                get: function () {
                    var self       = this;
                    var tipoMoneda = self.tipoMoneda;
                    if (!tipoMoneda) {
                        return null;
                    }
                    return catalogoTipoMoneda.human[tipoMoneda].simbolo;
                }
            }

        },
        {
            tableName: 'clientes_notacredito',
            validate: {
                // difEmisorReceptor :    function() {
                // 	var comprobante = this;
                // 	if( comprobante.get( 'idEmisor' ) == comprobante.get( 'idReceptor' ) ) {
                // 		throw new Error( "No se puede enviar un comprobante a si mismo" );
                // 	}
                // } ,
                hasItems: function () {
                    var notacredito = this;
                    if (!(notacredito.get('items') && Array.isArray(notacredito.get('items')) && notacredito.get('items').length > 0)) {
                        throw new Error(erroresMsg.getMessage("items", "items"))
                    }
                },
                isValidFormat: function () {
                    var self = this;
                    if (self.serieAfectado[0] !== self.serie[0]) {
                        throw new Error("Las notas de credito deben pertenecer al mismo formato de los comprobantes originales ( F o B)");
                    }
                },
                // isValidDateIssue :     function() {
                // 	//Este caso es para los tipos 01 y 03 de notas de credito , que deben estar dentro de los 10 dias  habiles del mes siguiente
                // 	var self                    = this;
                // 	var tipoComprobanteAfectado = self.tipoComprobanteAfectado;
                // 	var tipoNotaCredito         = self.tipoNotaCredito;
                // 	var serieAfectado           = self.serieAfectado;
                // 	var numeroAfectado          = self.numeroAfectado;
                // 	var idEmisor                = self.idEmisor;
                //
                // 	if( tipoNotaCredito == "01" || tipoNotaCredito == "03" ) {
                // 		var comps = {
                // 			"01" : sequelize.models.Factura ,
                // 			"03" : sequelize.models.Boleta
                // 		};
                //
                // 		return comps[ tipoComprobanteAfectado ].findOneBySerieNumeroEmisor( serieAfectado , numeroAfectado , idEmisor )
                // 			.then( function( comprobante ) {
                // 				var fechaEmisionComprobante = moment( comprobante.fechaEmision );
                // 				var fechaIni                = fechaEmisionComprobante.add( 1 , 'months' ).startOf( 'month' );
                // 				var fechaFin                = self.fechaEmision;
                //
                // 				if( utils.diasHabiles( fechaIni , fechaFin ) > 10 ) {
                // 					throw new Error( "Para los tipos 01 y 03 de nota de credito , el plazo es hasta el decimo dia habil del mes siguiente" );
                // 				}
                // 			} )
                // 			.catch( function( error ) {
                // 				throw error;
                // 			} );
                // 	}
                // } ,
                isValidNoteCode: function () {
                    var self                    = this;
                    var tipoComprobanteAfectado = self.tipoComprobanteAfectado;
                    var tipoNotaCredito         = self.tipoNotaCredito;
                    var combinacionesNoValidas  = {
                        "04": "03",
                        "05": "03",
                        "08": "03"
                    };

                    if (combinacionesNoValidas[tipoNotaCredito] == tipoComprobanteAfectado) {
                        var msg = util.format("El tipo de nota de credito %s no es posible con el tipo de comprobante especificado", tipoNotaCredito);
                        throw new Error(msg);
                    }
                },
                calcTotales: function () {
                    var notacredito = this;

                    var totalVentaGravada   = parseFloat(notacredito.get('totalVentaGravada') || 0);
                    var totalVentaInafecta  = parseFloat(notacredito.get('totalVentaInafecta') || 0);
                    var totalVentaExonerada = parseFloat(notacredito.get('totalVentaExonerada') || 0);

                    var sumatoriaIgv           = parseFloat(notacredito.get('sumatoriaIgv') || 0);
                    var sumatoriaIsc           = parseFloat(notacredito.get('sumatoriaIsc') || 0);
                    var sumatoriaOtrosCargos   = parseFloat(notacredito.get('sumatoriaOtrosCargos') || 0);
                    var sumatoriaOtrosTributos = parseFloat(notacredito.get('sumatoriaOtrosTributos') || 0);

                    var totalVenta = parseFloat(notacredito.get('totalVenta') || 0.00);

                    var porcentajeDescuentoGlobal = parseFloat(notacredito.get("porcentajeDescuentoGlobal") || 0.00);
                    var descuentoGlobal           = parseFloat(notacredito.get("descuentoGlobal") || 0.00);

                    var totalDescuento = parseFloat(notacredito.get("totalDescuento") || 0.00);
                    // Calculo de la sumatoria de IGV

                    var calcTotalIgv = parseFloat((utils.igvPercent * (totalVentaGravada + sumatoriaIsc)).toFixed(2));

                    if (!utils.equalsDecimal(calcTotalIgv, sumatoriaIgv)) {
                        throw new Error("Error en el calculo del IGV  : " + sumatoriaIgv);
                    }

                    var calcTotalVenta = parseFloat((totalVentaGravada + totalVentaExonerada + totalVentaInafecta + sumatoriaIgv + sumatoriaIsc + sumatoriaOtrosCargos + sumatoriaOtrosTributos ).toFixed(2));

                    //Calculo del importe total de venta en base a los subtotales
                    if (!utils.equalsDecimal(calcTotalVenta, totalVenta)) {
                        throw new Error("Error en el calculo del total venta  : " + totalVenta);
                    }

                    //Calculo de subtotales
                    var objTotales = notacredito.get('items').reduce(function (objPrev, item) {
                        var tipoAfectacionIgv       = item.get('tipoAfectacionIgv') || 10;
                        var montoAfectacionIgv      = item.get('montoAfectacionIgv') || 0.00;
                        var montoAfectacionIsc      = item.get("montoAfectacionIsc") || 0.00;
                        var tipoPrecioVentaUnitario = item.get('tipoPrecioVentaUnitario') || "01";
                        var precioVentaUnitario     = item.get('precioVentaUnitario') || 0.00;
                        var cantidad                = item.get('cantidad') || 0.00;
                        var descuentoItem           = item.get("descuento") || 0.00;

                        objPrev.sumDescuentos += parseFloat(descuentoItem);
                        objPrev.sumIgv += parseFloat(montoAfectacionIgv);
                        objPrev.sumIsc += parseFloat(montoAfectacionIsc);

                        if (tipoPrecioVentaUnitario == '01') {
                            switch (true) {
                                case (tipoAfectacionIgv < 20 ):
                                    objPrev.calcTotalVentaGravada += parseFloat(item.get('valorVenta'));
                                    break;
                                case (tipoAfectacionIgv == 20 ):
                                    objPrev.calcTotalVentaExonerada += parseFloat(item.get('valorVenta'));
                                    break;
                                case (tipoAfectacionIgv < 40 ):
                                    objPrev.calcTotalVentaInafecta += parseFloat(item.get('valorVenta'));
                                    break;
                                case (tipoAfectacionIgv == 40 ):
                                    objPrev.calcTotalVentaInafecta += parseFloat(item.get('valorVenta'));
                                    break;
                            }
                        } else {
                            objPrev.calcTotalVentaGratuita += parseFloat(precioVentaUnitario * cantidad);
                        }

                        return objPrev;
                    }, {
                        calcTotalVentaGravada: 0.00,
                        calcTotalVentaInafecta: 0.00,
                        calcTotalVentaExonerada: 0.00,
                        calcTotalVentaGratuita: 0.00,
                        sumDescuentos: 0.00,
                        sumIgv: 0.00,
                        sumIsc: 0.00
                    });

                    objTotales.calcTotalVentaGravada -= (porcentajeDescuentoGlobal) * 0.01 * objTotales.calcTotalVentaGravada;
                    objTotales.calcTotalVentaExonerada -= (porcentajeDescuentoGlobal) * 0.01 * objTotales.calcTotalVentaExonerada;
                    objTotales.calcTotalVentaInafecta -= (porcentajeDescuentoGlobal) * 0.01 * objTotales.calcTotalVentaInafecta;
                    objTotales.sumIgv -= (porcentajeDescuentoGlobal) * 0.01 * objTotales.sumIgv;
                    objTotales.sumIsc -= (porcentajeDescuentoGlobal) * 0.01 * objTotales.sumIsc;

                    //Calc Sum IGV ITEM
                    if (!utils.equalsDecimal(objTotales.sumIgv, sumatoriaIgv)) {
                        throw new Error("Error en el calculo de IGV , sumatoria de Items no es la misma ");
                    }

                    //calc sum ISC ITEM
                    if (!utils.equalsDecimal(objTotales.sumIsc, sumatoriaIsc)) {
                        throw new Error("Error en el calculo de ISC , sumatoria de Items no es la misma ");
                    }

                    //calc sum totalVentaGravada
                    if (!utils.equalsDecimal(objTotales.calcTotalVentaGravada, totalVentaGravada)) {
                        throw new Error("Error en el calculo de totalVentaGravada, sumatoria de Items no es la misma ");
                    }

                    //calc sum totalVentaExonerada
                    if (!utils.equalsDecimal(objTotales.calcTotalVentaExonerada, totalVentaExonerada)) {

                        throw new Error("Error en el calculo de totalVentaExonerada , sumatoria de Items no es la misma ");
                    }

                    //calc sum totalVentaInafecta
                    if (!utils.equalsDecimal(objTotales.calcTotalVentaInafecta, totalVentaInafecta)) {
                        throw new Error("Error en el calculo de totalVentaInafecta , sumatoria de Items no es la misma ");
                    }

                    var calcTotalDescuentos = objTotales.sumDescuentos + descuentoGlobal;
                    if (!utils.equalsDecimal(calcTotalDescuentos, totalDescuento)) {
                        throw new Error("Error en el calculo de totalDescuento , sumatoria de descuento por item y descuento global no concuerda");
                    }
                },
                // isUnique :             function() {
                // 	var self = this;
                //
                // 	var serieReq      = self.get( 'serie' ) || null;
                // 	var idEmisorReq = self.get( 'idEmisor' ) || null;
                // 	var numeroReq   = self.get( "numero" ) || null;
                // 	var idNotaCredito = self.get( "idNotaCredito" ) || null;
                //
                // 	if( !idNotaCredito && numeroReq ) {
                // 		return NotaCredito.findOne( {
                // 				where : {
                // 					serie :    serieReq ,
                // 					idEmisor : idEmisorReq ,
                // 					numero :   numeroReq
                // 				}
                // 			} )
                // 			.then( function( lastnotadebito ) {
                // 				if( lastnotadebito ) {
                // 					throw new Error( "La notadebito " + serieReq + "-" + numeroReq + " ya ha sido emitida" );
                // 				}
                // 			} )
                // 			.catch( function( error ) {
                // 				throw error;
                // 			} );
                // 	}
                // } ,
                // afectadoNoEstaDeBaja : function() {
                //
                // 	var self            = this;
                // 	var tipoComprobante = self.tipoComprobanteAfectado;
                // 	var serieAfectado   = self.serieAfectado;
                // 	var numeroAfectado  = self.numeroAfectado;
                // 	var idEmisor        = self.idEmisor;
                //
                // 	var mapTypes = {
                // 		"01" : sequelize.models.Factura ,
                // 		"03" : sequelize.models.Boleta
                // 	};
                //
                // 	return mapTypes[ tipoComprobante ].findOne( {
                // 		where : {
                // 			idEmisor : idEmisor ,
                // 			serie :    serieAfectado ,
                // 			numero :   numeroAfectado
                // 		}
                // 	} ).then( function( comprobanteDeBaja ) {
                // 		if( comprobanteDeBaja && comprobanteDeBaja.estaDeBaja() ) {
                // 			throw new Error( "El comprobante afectado esta dado de baja" );
                // 		}
                // 	} ).catch( function( error ) {
                // 		throw error;
                // 	} );
                // }
            },
            hooks: {
                beforeCreate: []
            },
            instanceMethods: {
                getXmlField: function () {
                    var self = this;
                    var id   = self.idNotaCredito;

                    return NotaCredito.find({
                        where: {idNotaCredito: id},
                        attributes: {exlclude: ["xml"]}
                    }).then(function (comp) {
                            if (!comp || !comp.xml) {
                                return sequelize.Promise.resolve(null);
                            }
                            return sequelize.Promise.resolve(comp.xml);
                        })
                        .catch(function (err) {
                            return sequelize.Promise.reject(err);
                        });
                },
                pretty: function () {
                    var self = this;
                    self     = self.get({plain: true});
                    utils.deleteNullProperties(self);
                    return self;
                },
                estaEmitido: function () {
                    var self = this;
                    return self.emitido == 1;
                },
                getImporteLetras: function () {
                    var self         = this;
                    var importeVenta = self.totalVenta;
                    var words        = [utils.numberToWords(importeVenta).toUpperCase(), "Y", utils.complementDecimal(importeVenta)].join(" ");
                    return words;
                },
                getMonedaLetras: function () {
                    var self       = this;
                    var tipoMoneda = self.tipoMoneda;
                    return catalogoTipoMoneda.human[tipoMoneda].letras;
                },
                getMonedaSimbolo: function () {
                    var self       = this;
                    var tipoMoneda = self.tipoMoneda;
                    return catalogoTipoMoneda.human[tipoMoneda].simbolo;
                },
                getFechaFormatoSunat: function () {
                    var self = this;
                    return moment(self.fechaEmision).format("YYYY-MM-DD");
                },
                // getNombreBase :        function() {
                // 	var self = this;
                // 	if( !self.emisor ) {
                // 		throw new Error( "La nota de credito no tiene asociada un emisor" );
                // 	}
                // 	return [ self.get( 'idEmisor' ) , self.get( 'codigoComprobante' ) , self.get( 'serie' ) , self.get( 'numero' ) ].join( "-" );
                // } ,
                // getPdf417Code :        function() {
                // 	var self         = this;
                // 	var tipoReceptor = (self.receptor) ? self.receptor.tipo : "-";
                // 	var idReceptor   = (self.receptor) ? self.receptor.idReceptor : "-";
                // 	var text         = [
                // 		self.get( 'idEmisor' ) ,
                // 		self.get( 'codigoComprobante' ) ,
                // 		self.get( 'serie' ) ,
                // 		self.get( 'numero' ) ,
                // 		self.get( 'sumatoriaIgv' ) ,
                // 		self.get( 'totalVenta' ) ,
                // 		self.getFechaFormatoSunat() ,
                // 		tipoReceptor ,
                // 		idReceptor ,
                // 		self.get( 'digestValue' ) ,
                // 		self.get( 'signatureValue' ) ].join( "|" );
                //
                // 	return text;
                // } ,
                // getQrCode :            function() {
                // 	var self         = this;
                // 	var tipoReceptor = (self.receptor) ? self.receptor.tipo : "-";
                // 	var idReceptor   = (self.receptor) ? self.receptor.idReceptor : "-";
                // 	var text         = [
                // 		self.get( 'idEmisor' ) ,
                // 		self.get( 'codigoComprobante' ) ,
                // 		self.get( 'serie' ) ,
                // 		self.get( 'numero' ) ,
                // 		self.get( 'sumatoriaIgv' ) ,
                // 		self.get( 'totalVenta' ) ,
                // 		self.getFechaFormatoSunat() ,
                // 		tipoReceptor ,
                // 		idReceptor ,
                // 	].join( "|" );
                //
                // 	return text;
                // }
            },
            classMethods: {
                associate: function (models) {
                    NotaCredito.hasMany(models.NotaCreditoItem, {
                        foreignKey: 'idNotaCredito',
                        as: {
                            singular: 'item',
                            plural: 'items'
                        }
                    });
                    // NotaCredito.hasOne( models.NotaCreditoExtra , { foreignKey : "idNotaCredito" , as : "extra" } );
                    // NotaCredito.belongsTo( models.Usuario , { foreignKey : 'idUsuario' , as : "usuario" } );
                    // NotaCredito.belongsTo( models.Receptor , { foreignKey : 'idReceptor' , as : "receptor" } );
                    // NotaCredito.belongsTo( models.Emisor , { foreignKey : 'idEmisor' , as : "emisor" } );
                    // NotaCredito.belongsTo( models.Constancia , {
                    // 	foreignKey : 'idConstancia' ,
                    // 	as :         "constancia"
                    // } );
                },
                getFieldsNotSave: function () {
                    return ["idNotaCredito", "fechaEmitido", "emitido", "digestValue", "signatureValue", "xml"];
                },
                getFieldsToQuery: function () {
                    var notSave    = ["idNotaCredito", "leyendas", "digestValue", "signatureValue", "guiasRelacionada", "docRelacionada", "xml"];
                    var attr       = Object.keys(this.attributes);
                    var saveFields = attr.filter(function (attribute) {

                        if (notSave.indexOf(attribute) == -1) {
                            return true;
                        }
                    });
                    return saveFields;
                },
                findOneWithAllDataById: function (idNotaCredito) {
                    return NotaCredito.findOne({
                        where: {idNotaCredito: idNotaCredito},
                        attributes: {
                            exclude: ["xml"]
                        },
                        include: [
                            {
                                model: sequelize.models.NotaCreditoItem,
                                as: "items"
                            },
                            // {
                            // 	model : sequelize.models.NotaCreditoExtra ,
                            // 	as :    "extra"
                            // } ,
                            // {
                            // 	model :      sequelize.models.Emisor ,
                            // 	as :         "emisor" ,
                            // 	attributes : [ "idEmisor" , 'razonSocial' , "nombreComercial" ] ,
                            // 	include :    [
                            // 		{
                            // 			model :      sequelize.models.Domicilio ,
                            // 			as :         "domicilio" ,
                            // 			attributes : [ "departamento" , "provincia" , "distrito" , "ubigeo" , "direccion" ] ,
                            // 		} ,
                            // 		{
                            // 			model :      sequelize.models.UsuarioSunat ,
                            // 			as :         "usuariosunat" ,
                            // 			attributes : [ "username" , "password" ]
                            // 		} ,
                            // 		{
                            // 			model : sequelize.models.EmisorConfiguracion ,
                            // 			as :    "configuracion"
                            // 		}
                            // 	]
                            // } ,
                            // {
                            // 	model :      sequelize.models.Receptor ,
                            // 	attributes : [ "idReceptor" , 'tipo' , "razonSocial" ] ,
                            // 	as :         "receptor"
                            // }

                        ]
                    });
                },
                findOneNoEmitedWithAllData: function () {
                    return NotaCredito.findOne({
                        where: {enviado: false, rechazado: false},
                        include: [
                            {
                                model: sequelize.models.NotaCreditoItem,
                                as: "items"
                            }
                        ]
                    });
                },
                findAllNoEmitedWithAllData: function () {
                    return NotaCredito.findAll({
                        where: {enviado: true, rechazado: true},
                        include: [
                            {
                                model: sequelize.models.NotaCreditoItem,
                                as: "items"
                            }
                        ]
                    });
                },
                findAllTodayReportWithAllData: function () {
                    var startday = new Date();
                    startday.setHours(0,0,0,0);
                    return NotaCredito.findAll({
                        where :       ['fechaEmision > ?', startday] ,
                        // attributes : {
                        // 	// exclude : [ "xml" ]
                        // } ,
                        attributes: ['fechaEmision', 'serie', 'numero', 'emitido', 'enviado', 'rechazado', 'motivoRechazo']
                    });
                }
                // findOneBySerieNumeroEmisor : function( serie , numero , idEmisor ) {
                // 	return NotaCredito.findOne( {
                // 		where :      {
                // 			serie :    serie ,
                // 			numero :   numero ,
                // 			idEmisor : idEmisor
                // 		} ,
                // 		attributes : {
                // 			exclude : [ "xml" ]
                // 		} ,
                // 		include :    [
                // 			{
                // 				model : sequelize.models.NotaCreditoItem ,
                // 				as :    "items"
                // 			} ,
                // 			{
                // 				model : sequelize.models.NotaCreditoExtra ,
                // 				as :    "extra"
                // 			}
                // 		]
                // 	} );
                // } ,
                // findOneById :                function( idNotaCredito ) {
                // 	return NotaCredito.findOne( {
                // 		where :      {
                // 			idNotaCredito : idNotaCredito
                // 		} ,
                // 		attributes : {
                // 			exclude : [ "xml" ]
                // 		} ,
                // 		include :    [
                // 			{
                // 				model : sequelize.models.NotaCreditoExtra ,
                // 				as :    "extra"
                // 			} ,
                // 			{
                // 				model : sequelize.models.NotaCreditoItem ,
                // 				as :    "items"
                // 			} ,
                // 			{
                // 				model : sequelize.models.Receptor ,
                // 				as :    "receptor"
                // 			}
                // 		]
                // 	} );
                // } ,
                // findAllWithAddData :         function( idEmisor , query ) {
                // 	if( typeof query !== "object" ) {
                // 		throw new Error( "Los querys deben sere objetos" );
                // 	}
                //
                // 	if( !query.where ) {
                // 		throw new Error( "Los querys deben tener al menos el attribute where" );
                // 	}
                //
                // 	query.where.idEmisor = idEmisor;
                // 	query.attributes     = { exclude : [ "xml" ] };
                // 	query.include        = [
                // 		{
                // 			model : sequelize.models.NotaCreditoExtra ,
                // 			as :    "extra"
                // 		} ,
                // 		{
                // 			model : sequelize.models.NotaCreditoItem ,
                // 			as :    "items"
                // 		} ,
                // 		{
                // 			model : sequelize.models.Receptor ,
                // 			as :    "receptor"
                // 		}
                // 	];
                // 	return NotaCredito.findAll( query )
                // }
            }
        }
    );

    return NotaCredito;
};
