/**
 * Created by Luis on 09/08/2016.
 */
var erroresMsg             = require( "../error/index" ).validationMessage;
var utils                  = require( "../utils" );
var moment                 = require( "moment" );

module.exports = function( sequelize , DataTypes ) {
    var Storage = sequelize.define( 'Storage' , {
            idStorage :                 {
                type :          DataTypes.BIGINT ,
                primaryKey :    true ,
                autoIncrement : true
            } ,
            ultimoEmitido : {
                type :         DataTypes.DATE(3) ,
                allowNull :    false, 
                get :          function() {
                    var value = this.getDataValue( "ultimoEmitido" );
                    if( value ) {
                        return moment( value ).format( "YYYY-MM-DD HH:mm:ss.SSS" );
                    }
                }
            } ,
            tipoComprobante :        {
                type :         DataTypes.STRING
            } ,
            serie :        {
                type :         DataTypes.STRING
            } ,
            numero :       {
                type :      DataTypes.INTEGER
            },
        } ,
        {
            tableName :       'storage',
            classMethods:{
                buscarUno: function (tipo) {
                    return Storage.findOne({where:{tipoComprobante:tipo}});
                }
            }

        } );

    return Storage;
}
;
