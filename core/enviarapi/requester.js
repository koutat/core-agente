/** * Created by Luis on 08/04/2016.
*/
var request = require('request');
var moment  = require('moment');
var crypto = require('crypto');
var config  = require('../configuraciones/appConfig');


function configurar() {
   // Set the headers
   var data        = config.appConfig.accessKey+"|"+moment().unix();
   var hash        = crypto.createHmac("sha256",config.appConfig.secretKey);
   var signature   = hash.update(data).digest('hex');

   var headers = {
       'Content-Type'  :     'text/xml',
       'Authorization' :       "Fo "+config.appConfig.accessKey+":"+signature+":"+moment().unix()
   };
// Configure the request
   var options = {
       // url: 'http://192.168.1.29:3000/boleta/sync',
       rejectUnauthorized: false,
       method: 'POST',
       headers: headers,
       timeout: 5000,
       body: {'key1': 'xxx', 'key2': 'yyy'}
   };
   return options;
}

function  formatearRequest(comprobanteReq, callback) {
    var options = configurar();
    if(config.appConfig.proxy){
        options.proxy = config.appConfig.proxy;
        console.log(options);
    }    
   if(comprobanteReq.xml){
       options.body = comprobanteReq.xml.toString('utf8');
       switch (comprobanteReq.tipo){
           case "01": options.url = config.appConfig.server+"/factura/sync";break;
           case "03": options.url = config.appConfig.server+"/boleta/sync";break;
           case "07": options.url = config.appConfig.server+"/notaCredito/sync";break;
           case "08": options.url = config.appConfig.server+"/notaDebito/sync";break;
       }
       return callback(null,options);
   }else return callback("xml no encontrado");
}

Requester = function () {};


Requester.prototype.sendRequest = function (comprobanteReq,callback) {

   formatearRequest(comprobanteReq,function (err, options) {
       if(err){
           return callback(err,null);
       }
       console.log(options)
       request(options, function (error, response, body) {
           if(error){
               return callback("error de coneccion con el api: posiblemente no hay coneccion a internet "+error,null);
           }if(body){
               try{
                   body = JSON.parse(body);
                   if(response.statusCode  < 300)
                       return callback(null,body);
                   else
                       return callback(null,null,body.message+":"+body.description);
               }catch (err){
                   //lenin dehate de wanandas y aprende leer codigo  aqui es donde esta mandando ese mensaje le puse try catch para q no interrumpa el funcionamiento del agente sino lo mata
                   return callback(err+"real:"+body);
               }

           }else{
               return callback(response.body,null);
           }
       })
   })
};

module.exports = new Requester();
