/**
 * Created by Luis on 21/04/2016.
 */
var CronJob             = require('cron').CronJob;
var seeker              = require('./seeker');
var logger              = require('../logger/logger');
var async               = require('async');

function Worker() {}
/*
 worker
 */
Worker.prototype.workerStarT= function() {
    
    var i = 0;
    var job = new CronJob({
        cronTime: '*/5 * * * * *',
        onTick: function() {
            async.parallel([
                function () {
                    seeker.buscarVictimaFactura(function (err,respuesta) {
                        if(err) {
                            logger.log("error",err);
                        }else if(respuesta){
                            logger.log("info","la factura "+respuesta.serie+"-"+respuesta.numero+" ha sido enviada correctamente");
                        }
                    })
                },
                function () {
                    seeker.buscarVictimaBoleta(function (err,respuesta) {
                        if(err) {
                            logger.log("error",err);
                        }else if (respuesta){
                            logger.log("info","la boleta "+respuesta.serie+"-"+respuesta.numero+" ha sido enviada correctamente");
                        }
                    })
                },
                function () {
                    seeker.buscarVictimaNotaCredito(function (err,respuesta) {
                        if(err) {
                            logger.log("error",err);
                        }else if (respuesta){
                            logger.log("info","la nota de credito "+respuesta.serie+"-"+respuesta.numero+" ha sida enviado correctamente");
                        }
                    })
                },
                function () {
                    seeker.buscarVictimaNotaDebito(function (err,respuesta) {
                        if(err) {
                            logger.log("error",err);
                        }else if (respuesta){
                            logger.log("info","la nota de debito "+respuesta.serie+"-"+respuesta.numero+" ha sido enviada correctamente");
                        }
                    })
                }
            ], function () {
                console.log("u will never see this");
            });
        },
        start: false,
        timeZone: 'America/Los_Angeles'
    });
    job.start();
};

module.exports = new Worker();