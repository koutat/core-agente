/**
 * Created by Luis on 21/04/2016.
 */

var Factura                 = require( '../models/index' ).Factura;
var Boleta                  = require( '../models/index' ).Boleta;
var NotaCredito             = require( '../models/index' ).NotaCredito;
var NotaDebito              = require( '../models/index' ).NotaDebito;
var sequelize               = require( '../models/index' ).sequelize;
var Requester               = require('./requester');
var connection              = require('../consultardbcliente/especificacionCliente/core/connection');

var moment                  = require("moment");

function Seeker() {}
Seeker.prototype.buscarVictimaFactura = function(callback) {
    Factura.findOneNoEmitedWithAllData()
    .then(function (factura) {
        if(factura){
            factura.tipo = "01";
            Requester.sendRequest(factura,function (err,respuesta,badResponse) {
                if(err) return callback(err,null);
                if(respuesta){

                    factura.enviado = true;
                    factura.save({fields: ['enviado']})
                        .then(function () {
                            return callback(null,respuesta);
                        })
                        .catch(function (err) {
                            return callback(err,null);
                        });
                }
                else {
                    factura.enviado = true;
                    factura.rechazado = true;
                    factura.motivoRechazo = badResponse;
                    factura.save({fields: ['enviado','rechazado','motivoRechazo']})
                        .then(function () {
                            console.log(factura);
                            // var date = moment(factura.dataValues.fechaEmision).format('YYYY-MM-DD HH:mm:ss.SSS');
                            // var consulta = "INSERT INTO Fac.DocumentosRechazados"+
                            // " (TDO_ID,DTD_ID,DRE_SERIE,DRE_NUMERO,DRE_OBSERVACIONES,USU_ID,DRE_FEC_GRAB,DRE_ESTADO)"+
                            // " VALUES('02','04','"+factura.dataValues.serie+"','"+facturaReq.dataValues.numero+"','"+factura.motivoRechazo+"','ADMIN',"+"'"+date+"','1')";
                            //  connection.query(consulta) //conexion a la BD para traer la data del comprobante.
                            //     .then(function (data) {
                            //         console.log(data[0]);
                            //         //return callback(null, data[0]); // funcion de retorno con la data
                            //     })
                            //     .catch(function (error) {
                            //         console.log(error);
                            //         //return callback(error, null); // funcion de retorno con el error
                            //     })
                            return callback("Comprobante "+factura.serie+"-"+factura.numero+" rechazado",null);
                            //var consulta
                        })
                        .catch(function (err) {
                            return callback(err,null);
                        });
                    // return callback(err,null);
                    //console.log(factura);
                }

            })
        }
        else{
            console.log("no se encontro comprobantes Factura");
            // return callback("no se encontro comprobantes Boleta",null);
        }

    }).catch(function (err) {
        return callback(err,null);
    })
    
};
Seeker.prototype.buscarVictimaBoleta = function(callback) {
    Boleta.findOneNoEmitedWithAllData()
        .then(function (boleta) {
            if(boleta){
                boleta.tipo = "03";
                Requester.sendRequest(boleta,function (err,respuesta,badResponse) {
                    if(err) return callback(err,null);
                    if(respuesta){

                        boleta.enviado = true;
                        boleta.save({fields: ['enviado']})
                            .then(function () {
                                return callback(null,respuesta);
                            })
                            .catch(function (err) {
                                return callback(err,null);
                            });
                    }
                    else {
                        boleta.enviado = true;
                        boleta.rechazado = true;
                        boleta.motivoRechazo = badResponse;
                        boleta.save({fields: ['enviado','rechazado','motivoRechazo']})
                            .then(function () {
                                return callback("Comprobante "+boleta.serie+"-"+boleta.numero+" rechazado",null);
                            })
                            .catch(function (err) {
                                return callback(err,null);
                            });
                        return callback(err,null);
                    }

                })
            }
            else{
                console.log("no se encontro comprobantes Boleta");
                // return callback("no se encontro comprobantes Boleta",null);
            }

        }).catch(function (err) {
        return callback(err,null);
    })
};
Seeker.prototype.buscarVictimaNotaCredito = function(callback) {
    NotaCredito.findOneNoEmitedWithAllData()
        .then(function (nota) {
            if(nota){
                nota.tipo = "07";
                Requester.sendRequest(nota,function (err,respuesta,badResponse) {
                    if(err) return callback(err,null);
                    if(respuesta){

                        nota.enviado = true;
                        nota.save({fields: ['enviado']})
                            .then(function () {
                                return callback(null,respuesta);
                            })
                            .catch(function (err) {
                                return callback(err,null);
                            });
                    }
                    else {
                        nota.enviado = true;
                        nota.rechazado = true;
                        nota.motivoRechazo = badResponse;
                        nota.save({fields: ['enviado','rechazado','motivoRechazo']})
                            .then(function () {
                                return callback("Comprobante "+nota.serie+"-"+nota.numero+" rechazado",null);
                            })
                            .catch(function (err) {
                                return callback(err,null);
                            });
                        return callback(err,null);
                    }

                })
            }
            else{
                console.log("no se encontro comprobantes nota de credito");
                // return callback("no se encontro comprobantes Boleta",null);
            }

        }).catch(function (err) {
        return callback(err,null);
    })
};
Seeker.prototype.buscarVictimaNotaDebito = function(callback) {
    NotaDebito.findOneNoEmitedWithAllData()
        .then(function (nota) {
            if(nota){
                nota.tipo = "08";
                Requester.sendRequest(nota,function (err,respuesta,badResponse) {
                    if(err) return callback(err,null);
                    if(respuesta){

                        nota.enviado = true;
                        nota.save({fields: ['enviado']})
                            .then(function () {
                                return callback(null,respuesta);
                            })
                            .catch(function (err) {
                                return callback(err,null);
                            });
                    }
                    else {
                        nota.enviado = true;
                        nota.rechazado = true;
                        nota.motivoRechazo = badResponse;
                        nota.save({fields: ['enviado','rechazado','motivoRechazo']})
                            .then(function () {
                                return callback("Comprobante "+nota.serie+"-"+nota.numero+" rechazado",null);
                            })
                            .catch(function (err) {
                                return callback(err,null);
                            });
                        return callback(err,null);
                    }

                })
            }
            else{
                console.log("no se encontro comprobantes nota de debito");
                // return callback("no se encontro comprobantes Boleta",null);
            }

        }).catch(function (err) {
        return callback(err,null);
    })
};

module.exports = new Seeker();