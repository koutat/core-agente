/**
 * Created by Luis on 12/11/2016.
 */
/**
 * Created by Luis on 21/07/2016.
 */
/**
 * Created by Luis on 21/04/2016.
 */
var CronJob             = require('cron').CronJob;
var async               = require('async');
var logger              = require('../logger/logger');
const fs = require('fs');


function clearLog() {
    fs.truncate("./logs.log",function (err) {
        if(err)
            throw  err;
        console.log("cleared successfully");
    })
}

function LoggerCron() {}
/*
 worker
 */
LoggerCron.prototype.workerStarT = function() {
    var job = new CronJob({
        cronTime: '0 0 */3 * * *',//cada 3 horas
        onTick: function() {
            clearLog();
        },
        start: false,
        timeZone: 'America/Los_Angeles'
    });
    job.start();
};

module.exports = new LoggerCron();
