/**
 * Created by Luis on 01/08/2016.
 */

var winston = require('winston');
var Logger = function () {
    this.logger = winston;
    winston.add(winston.transports.File, { filename: 'logs.log' });
};
/*
  Tipos de log y prioridad:
 { error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5 }
 */
Logger.prototype.log = function (tipo,message) {
    this.logger.log(tipo,message);
};
Logger.prototype.limpiar = function (tipo,message) {
    this.logger.log.removeAll();
    // logger
    //     .add(winston.transports.File)
    //     .remove(winston.transports.Console);
};
module.exports = new Logger();



