/**
 * Created by Luis on 02/08/2016.
 */
var logger                 = require('./logger');
var Reader                 = function () {
};
Reader.prototype.consultar = function (callback) {
    var options = {
        from: new Date - 24 * 60 * 60 * 1000,
        until: new Date,
        limit: 20,
        start: 0,
        order: 'desc',
        fields: ['message',['timestamp']]
    };

    //
    // Find items logged between today and yesterday.
    //
    logger.logger.query(options, function (err, results) {
        if (err) {
            return callback(err,null)
        }
        return callback(null,results);
    });
};
module.exports             = new Reader();
