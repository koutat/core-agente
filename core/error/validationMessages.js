var util = require( "util" );

var facts = {
	"isAlphanumeric" : "%s : Debe ser alfanumerico" ,
	"notEmpty" :       "%s : El campo  es obligatorio" ,
	"isDate" :         "%s : Debe tener un formato de fecha" ,
	"isFloat" :        "%s : Debe ser numerico(%s,%s)" ,
	"isInt" :          "%s : Debe ser entero" ,
	"pattern" :        "%s : No cumple con el patron requerido" ,
	"max" :            "%s : Es invalido (limite superior)" ,
	"min" :            "%s : Es invalido (limite inferior)" ,
	"isIn" :           "%s : Debe pertenecer al estandar" ,
	"len" :            "%s : El tamanho debe ser entre %s y %s" ,
	"len2" :           "%s : El tamanho debe ser de %s" ,
	"items" :          "%s : Al menos debe tener un item",
};

module.exports = {
	facts :      facts ,
	getMessage : function( fact , campo , extra1 , extra2 ) {
		extra1 = extra1 || '';
		extra2 = extra2 || '';
		return util.format( facts[ fact ] , campo , extra1 , extra2 );
	}
}

