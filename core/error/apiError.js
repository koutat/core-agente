var apiErrorCatalog = require( "./apiError.json" );
var util            = require( "util" );

// Service Errors => 1000-1999
// Auth Errors => 2000-2999
// Comprobante Errors => 3000-3999
// Receptor Errors => 4000-4999
// 
/* Format
 {
 code : "type : message"
 }
 */

/**
 * Clase aplicada para los errores customizados
 * @param code Codigo de error segun el negocio
 * @param status  Codigo de error HTTP
 * @param message  Mensaje de error del negocio
 * @param description Descripcion Breve del error real
 * @param extra Informacion Extra del error real
 * @constructor
 */
function ApiError( code , status , description , extra ) {
	Error.captureStackTrace( this , this.constructor );

	this.code        = code || "1000";
	this.status      = status || "500";
	this.message     = apiErrorCatalog[ code ];
	this.description = description || undefined;
	this.extra       = extra || undefined;
}

util.inherits( ApiError , Error );

ApiError.prototype.setCode = function( code ) {
	this.code    = code;
	this.message = apiErrorCatalog[ code ]
};

ApiError.prototype.setDescription = function( description ) {
	this.description = description;
};

ApiError.prototype.setStatus = function( status ) {
	this.status = status;
};

ApiError.prototype.setExtra = function( extra ) {
	this.extra = extra;
};

module.exports = ApiError;