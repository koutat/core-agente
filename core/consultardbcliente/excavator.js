/**
 * Created by Luis on 10/06/2016.
 */
var dbCliente = require('./db/conection');
var consulta  = require('./db/query');
var Emiter    = require('../verificarconeccion');
var storager  = require("../consultardbcliente/storager");
var moment    = require("moment");

var Excavator = function () {};
var consultarComprobantes = function (tipo,queryComprobante, callback) {

    storager.getFechaUltimoRecibido(tipo,function (err,fecha) {
        var consulta = queryComprobante.comprobante + "'"+moment(fecha).format('YYYY-MM-DD HH-mm-ss')+"'";
        dbCliente.query(
            consulta
        ).then(function (data) {
            return callback(null, data);
        }).catch(function (error) {
            return callback("error al intentar consultar la base de datos del cliente , detalles:"+error, null)
        })
    });
};
var consultarItems        = function (queryComprobante, idComprobante, callback) {
    var consulta = queryComprobante.items + idComprobante;
    dbCliente.query(
        consulta
    ).then(function (data) {
        return callback(null, data[0]);
    }).catch(function (err) {
        return callback(err, null);
    });

};

var consultarReceptor = function (queryComprobante, idReceptor, callback) {
    var consulta = queryComprobante.receptor + idReceptor;
    dbCliente.query(
        consulta
    ).then(function (data) {
        return callback(null, data[0][0]);
    }).catch(function (err) {
        return callback(err, null);
    });
};

var obtenerComprobanteAConsultar = function (tipoComprobante, callback) {

    switch (tipoComprobante) {
        case 1:
            return callback(null, consulta.query.factura, "01");
            break;
        case 3:
            return callback(null, consulta.query.boleta, "03");
            break;
        case 7:
            return callback(null, consulta.query.notaCredito, "07");
            break;
        case 8:
            return callback(null, consulta.query.notaDebito, "08");
            break;   
    }
};
var obtenerComprobantes          = function (tipo,queryComprobate, callback) {

    consultarComprobantes(tipo,queryComprobate, function (err, data) {
        if (err) return callback(err, null);
        else {
            return callback(null, data[0])
        }
    })
};
var prepararJson                 = function (precomprobante, items, receptor, callback) {

    // var conprobante = {};
    // conprobante = precomprobante;
    precomprobante.receptor = receptor || null;
    precomprobante.items    = items;
    return callback(null, precomprobante);
};
var eliminarCamposNulos          = function (preComprobante, callback) {
    for (var i in preComprobante) {
        if (preComprobante[i] === null) {
            delete preComprobante[i];
        }
    }
    return callback(preComprobante);
};

Excavator.prototype.consultarBaseDatosCliente = function (tipoComprobante,callback) {
    obtenerComprobanteAConsultar(tipoComprobante, function (err, query, tipoComprobate) {
        if (err)return callback(err,null);
        else {
            obtenerComprobantes(tipoComprobate,query, function (err, data) {
                if (err)return callback(err,null);
                else {
                    data.forEach(function (preComprobante) {
                        switch (tipoComprobate){
                            case "01":idComprobante = preComprobante.idFactura;
                                break;
                            case "03":idComprobante = preComprobante.idBoleta;
                                break;
                            case "07":idComprobante = preComprobante.idNotaCredito;
                                break;
                            case "08":idComprobante = preComprobante.idNotaDebito;
                                break;
                        }
                        consultarItems(query, idComprobante, function (err, items) {
                            if (err)return callback(err,null);
                            else {
                                consultarReceptor(query, idComprobante, function (err, receptor) {
                                    if (err)return callback(err,null);
                                    else {
                                        prepararJson(preComprobante, items, receptor, function (err, comprobante) {

                                            if (err)return callback(err,null);
                                            else {
                                                var comprobante2             = {};
                                                comprobante2.tipoComprobante = tipoComprobate;
                                                comprobante2.body            = comprobante;
                                                comprobante2.body.tipoComprobante = tipoComprobate;
                                                eliminarCamposNulos(comprobante2.body, function (preComprobante) {
                                                    comprobante2.body = preComprobante;
                                                    comprobante2.body.receptor = JSON.stringify(comprobante2.body.receptor);
                                                    Emiter.connect(comprobante2, function (err, succes) {
                                                        if (err){
                                                            storager.actualizarUltimoRecibido(comprobante,function (err,success) {
                                                                return callback(err,null);
                                                            });
                                                        }else{
                                                            storager.actualizarUltimoRecibido(comprobante,function (err,success) {
                                                                if(err) return callback(err);
                                                                return callback(null,success)
                                                            });
                                                        }
                                                    })
                                                });
                                            }
                                        })
                                    }
                                })
                            }
                        })

                    });
                }
            })

        }
    });
};
module.exports = new Excavator();


