/**
 * Created by Luis on 10/06/2016.
 */

var clienteData = require('../../../config/client/private/dbClientData.json');

var camposEnComun = {
    comprobante:
        clienteData.campos.required.fechaEmision                +" AS fechaEmision, "+
        clienteData.campos.required.serie                       +" AS serie, "+
        clienteData.campos.required.numero                      +" AS numero, " +
        clienteData.campos.required.totalVentaGravada           +" AS totalVentaGravada, " +
        clienteData.campos.required.sumatoriaIgv                +" AS sumatoriaIgv, "+
        clienteData.campos.required.totalVenta                  +" AS totalVenta, "+
        clienteData.campos.required.tipoMoneda                  +" AS tipoMoneda, "+

        clienteData.campos.optional.totalVentaInafecta          +" AS totalVentaInafecta, " +
        clienteData.campos.optional.totalVentaExonerada         +" AS totalVentaExonerada, " +
        clienteData.campos.optional.totalVentaGratuita          +" AS totalVentaGratuita, "+
        clienteData.campos.optional.sumatoriaIsc                +" AS sumatoriaIsc, "+
        clienteData.campos.optional.sumatoriaOtrosTributos      +" AS sumatoriaOtrosTributos, "+
        clienteData.campos.optional.sumatoriaOtrosCargos        +" AS sumatoriaOtrosCargos, "+
        clienteData.campos.optional.guiasRelacionada            +" AS guiasRelacionada, "+
        clienteData.campos.optional.docRelacionada              +" AS docRelacionada, "+
        clienteData.campos.optional.leyendas                    +" AS leyendas, "+
        clienteData.campos.optional.porcentajePercepcion        +" AS porcentajePercepcion, "+
        clienteData.campos.optional.importePercepcion           +" AS importePercepcion, "+
        clienteData.campos.optional.descuentoGlobal             +" AS descuentoGlobal, "+
        clienteData.campos.optional.porcentajeDescuentoGlobal   +" AS porcentajeDescuentoGlobal, "+
        clienteData.campos.optional.totalDescuento              +" AS totalDescuento "
    ,
    comprobanteNota:
    clienteData.campos.required.fechaEmision                +" AS fechaEmision, "+
    clienteData.campos.required.serie                       +" AS serie, "+
    clienteData.campos.required.numero                      +" AS numero, " +
    clienteData.camposnota.numeroAfectado                   +" AS numeroAfectado, " +
    clienteData.camposnota.serieAfectado                    +" AS serieAfectado, " +
    clienteData.camposnota.tipoComprobanteAfectado          +" AS tipoComprobanteAfectado, " +
    clienteData.camposnota.descripcion                      +" AS descripcion, " +
    clienteData.campos.required.totalVentaGravada           +" AS totalVentaGravada, " +
    clienteData.campos.required.sumatoriaIgv                +" AS sumatoriaIgv, "+
    clienteData.campos.required.totalVenta                  +" AS totalVenta, "+
    clienteData.campos.required.tipoMoneda                  +" AS tipoMoneda, "+

    clienteData.campos.optional.totalVentaInafecta          +" AS totalVentaInafecta, " +
    clienteData.campos.optional.totalVentaExonerada         +" AS totalVentaExonerada, " +
    clienteData.campos.optional.sumatoriaIsc                +" AS sumatoriaIsc, "+
    clienteData.campos.optional.sumatoriaOtrosTributos      +" AS sumatoriaOtrosTributos, "+
    clienteData.campos.optional.sumatoriaOtrosCargos        +" AS sumatoriaOtrosCargos, "+
    clienteData.campos.optional.guiasRelacionada            +" AS guiasRelacionada, "+
    clienteData.campos.optional.docRelacionada              +" AS docRelacionada, "+
    clienteData.campos.optional.totalDescuento              +" AS totalDescuento, "
    ,
    items:
        clienteData.campos.item.required.unidadMedidaCantidad                +" AS unidadMedidaCantidad, "+
        clienteData.campos.item.required.cantidad                            +" AS cantidad, "+
        clienteData.campos.item.required.valorVenta                          +" AS valorVenta, " +
        clienteData.campos.item.required.montoAfectacionIgv                  +" AS montoAfectacionIgv, " +
        clienteData.campos.item.required.tipoAfectacionIgv                   +" AS tipoAfectacionIgv, " +
        clienteData.campos.item.required.precioVentaUnitario                 +" AS precioVentaUnitario, " +
        clienteData.campos.item.required.tipoPrecioVentaUnitario             +" AS tipoPrecioVentaUnitario, "+
        clienteData.campos.item.required.descripcion                         +" AS descripcion, "+
        clienteData.campos.item.required.valorUnitario                       +" AS valorUnitario, "+
    //opcionales
        clienteData.campos.item.optional.montoAfectacionIsc                  +" AS montoAfectacionIsc, " +
        clienteData.campos.item.optional.tipoAfectacionIsc                   +" AS tipoAfectacionIsc, " +
        clienteData.campos.item.optional.codigoProducto                      +" AS codigoProducto, "+
        clienteData.campos.item.optional.descuento                           +" AS descuento, "+
        clienteData.campos.item.optional.porcentajeDescuento                 +" AS porcentajeDescuento, "+
        clienteData.campos.item.optional.precioSugerido                      +" AS precioSugerido "
    ,
    itemsNotaDebito:
    clienteData.campos.item.required.unidadMedidaCantidad                +" AS unidadMedidaCantidad, "+
    clienteData.campos.item.required.cantidad                            +" AS cantidad, "+
    clienteData.campos.item.required.valorVenta                          +" AS valorVenta, " +
    clienteData.campos.item.required.montoAfectacionIgv                  +" AS montoAfectacionIgv, " +
    clienteData.campos.item.required.tipoAfectacionIgv                   +" AS tipoAfectacionIgv, " +
    clienteData.campos.item.required.precioVentaUnitario                 +" AS precioVentaUnitario, " +
    clienteData.campos.item.required.tipoPrecioVentaUnitario             +" AS tipoPrecioVentaUnitario, "+
    clienteData.campos.item.required.descripcion                         +" AS descripcion, "+
    clienteData.campos.item.required.valorUnitario                       +" AS valorUnitario, "+
    //opcionales
    clienteData.campos.item.optional.montoAfectacionIsc                  +" AS montoAfectacionIsc, " +
    clienteData.campos.item.optional.tipoAfectacionIsc                   +" AS tipoAfectacionIsc, " +
    clienteData.campos.item.optional.codigoProducto                      +" AS codigoProducto, "+
    clienteData.campos.item.optional.precioSugerido                      +" AS precioSugerido "
    ,

};

var query = {

    factura:{
        comprobante:
            "SELECT " +
            clienteData.campos.idComprobante.idFactura     +" AS idFactura, "+camposEnComun.comprobante+
            "FROM "+clienteData.tablas.factura.tablaname+
            " WHERE "+clienteData.tablas.factura.tablaname+"."+clienteData.campos.required.fechaEmision+" > ",
        items:
            "SELECT " +
            clienteData.campos.item.idComprobante.idFactura             +" AS idFactura, "+camposEnComun.items+
            "FROM "+clienteData.tablas.factura.items+
            " WHERE "+clienteData.campos.item.idComprobante.idFactura+" = ",//completar con la llave foranea del comprobante
        receptor:
            "SELECT " +
            clienteData.tablas.receptores.tablename+"."+clienteData.campos.required.receptor.idReceptor                           +" AS idReceptor,"+
            clienteData.tablas.receptores.tablename+"."+clienteData.campos.required.receptor.tipo                                 +" AS tipo, "+
            clienteData.tablas.receptores.tablename+"."+clienteData.campos.required.receptor.razonSocial                          +" AS razonSocial "+
            "FROM "         + clienteData.tablas.factura.tablaname+
            " LEFT JOIN "  + clienteData.tablas.receptores.tablename+
            " ON "       +clienteData.tablas.factura.tablaname+"."+ clienteData.campos.required.receptor.idReceptor+" = " + clienteData.tablas.receptores.tablename+"."+clienteData.campos.required.receptor.idReceptor+
            " WHERE "+clienteData.campos.idComprobante.idFactura+"  = "//completar con la llave foranea del receptor
    },
    boleta:{
        comprobante:
        "SELECT " +
        clienteData.campos.idComprobante.idBoleta     +" AS idBoleta, "+camposEnComun.comprobante+
        "FROM "+clienteData.tablas.boleta.tablename+
        " WHERE "+clienteData.tablas.boleta.tablename+"."+clienteData.campos.required.fechaEmision+" > ",
        items:
        "SELECT " +
        clienteData.campos.item.idComprobante.idBoleta             +" AS idBoleta, "+camposEnComun.items+
        "FROM "+clienteData.tablas.boleta.items+
        " WHERE "+clienteData.campos.item.idComprobante.idBoleta+" = ",//completar con la llave foranea del comprobante
        receptor:
        "SELECT " +
        clienteData.tablas.receptores.tablename+"."+clienteData.campos.required.receptor.idReceptor                           +" AS idReceptor,"+
        clienteData.tablas.receptores.tablename+"."+clienteData.campos.required.receptor.tipo                                 +" AS tipo, "+
        clienteData.tablas.receptores.tablename+"."+clienteData.campos.required.receptor.razonSocial                          +" AS razonSocial "+
        "FROM "         + clienteData.tablas.boleta.tablename+
        " LEFT JOIN "  + clienteData.tablas.receptores.tablename+
        " ON "       + clienteData.tablas.boleta.tablename+"."+ clienteData.campos.required.receptor.idReceptor+" = " + clienteData.tablas.receptores.tablename+"."+clienteData.campos.required.receptor.idReceptor+
        " WHERE "+clienteData.campos.idComprobante.idBoleta+"  = "//completar con la llave foranea del receptor
    },
    notaCredito:{
        comprobante:
        "SELECT " +
        clienteData.campos.idComprobante.idNotaCredito     +" AS idNotaCredito, "+camposEnComun.comprobanteNota+
        clienteData.camposnota.tipoNotaCredito                  +" AS tipoNotaCredito " +
        "FROM "+clienteData.tablas.notaCredito.tablename+
        " WHERE "+clienteData.tablas.notaCredito.tablename+"."+clienteData.campos.required.fechaEmision+" > ",
        items:
        "SELECT " +
        clienteData.campos.item.idComprobante.idNotaCredito             +" AS idNotaCredito, "+camposEnComun.items+
        "FROM "+clienteData.tablas.notaCredito.items+
        " WHERE "+clienteData.campos.item.idComprobante.idNotaCredito+" = ",//completar con la llave foranea del comprobante
        receptor:
        "SELECT " +
        clienteData.tablas.receptores.tablename+"."+clienteData.campos.required.receptor.idReceptor                           +" AS idReceptor,"+
        clienteData.tablas.receptores.tablename+"."+clienteData.campos.required.receptor.tipo                                 +" AS tipo, "+
        clienteData.tablas.receptores.tablename+"."+clienteData.campos.required.receptor.razonSocial                          +" AS razonSocial "+
        "FROM "         + clienteData.tablas.notaCredito.tablename+
        " LEFT JOIN "  + clienteData.tablas.receptores.tablename+
        " ON "       +clienteData.tablas.notaCredito.tablename+"."+ clienteData.campos.required.receptor.idReceptor+" = " + clienteData.tablas.receptores.tablename+"."+clienteData.campos.required.receptor.idReceptor+
        " WHERE "+clienteData.campos.idComprobante.idNotaCredito+"  = "//completar con la llave foranea del receptor
    },
    notaDebito:{
        comprobante:
        "SELECT " +
        clienteData.campos.idComprobante.idNotaDebito     +" AS idNotaDebito, "+camposEnComun.comprobanteNota+
        clienteData.camposnota.tipoNotaDebito             +" AS tipoNotaDebito " +
        "FROM "+clienteData.tablas.notaDebito.tablename+
        " WHERE "+clienteData.tablas.notaDebito.tablename+"."+clienteData.campos.required.fechaEmision+" > ",
        items:
        "SELECT " +
        clienteData.campos.item.idComprobante.idNotaDebito             +" AS idNotaDebito, "+camposEnComun.itemsNotaDebito+
        "FROM "+clienteData.tablas.notaDebito.items+
        " WHERE "+clienteData.campos.item.idComprobante.idNotaDebito+" = ",//completar con la llave foranea del comprobante
        receptor:
        "SELECT " +
        clienteData.tablas.receptores.tablename+"."+clienteData.campos.required.receptor.idReceptor                           +" AS idReceptor,"+
        clienteData.tablas.receptores.tablename+"."+clienteData.campos.required.receptor.tipo                                 +" AS tipo, "+
        clienteData.tablas.receptores.tablename+"."+clienteData.campos.required.receptor.razonSocial                          +" AS razonSocial "+
        "FROM "         + clienteData.tablas.notaDebito.tablename+
        " LEFT JOIN "  + clienteData.tablas.receptores.tablename+
        " ON "       +clienteData.tablas.notaDebito.tablename+"."+ clienteData.campos.required.receptor.idReceptor+" = " + clienteData.tablas.receptores.tablename+"."+clienteData.campos.required.receptor.idReceptor+
        " WHERE "+clienteData.campos.idComprobante.idNotaDebito+"  = "//completar con la llave foranea del receptor
    }
};

module.exports.query = query;