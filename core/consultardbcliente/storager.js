/**
 * Created by Luis on 08/08/2016.
 */
var Storage   = require( '../models' ).Storage;
var moment    = require('moment');

function crearUltimo(tipo,callback) {
    // store = Storage.create({
    // ultimoEmitido: 0,
    // tipoComprobante: null,
    // serie:null,
    // numero:null
    // }).then(function (success) {
    //     return callback(null,true);
    // }).catch(function (err) {
    //     return callback(err)
    // });
    //console.log(tipo);

    //var fecha = new Date();
    //var fechaActual = fecha.setHours(0,0,0,0,0);
    // var fechaActual = 1;
    var fechaActual = '2016-12-22 00:00:00.000';
   switch(tipo){
       case "01":
           store = Storage.create({
           ultimoEmitido: fechaActual,
           tipoComprobante: "01",
           serie:null,
           numero:null
       }).then(function (success) {
               return callback(null,true);
           }).catch(function (err) {
               return callback(err)
           });
           break;
       case "03":
           store = Storage.create({
               ultimoEmitido: fechaActual,
               tipoComprobante: "03",
               serie:null,
               numero:null
           }).then(function (success) {
               return callback(null,true);
           }).catch(function (err) {
               return callback(err)
           });
           break;
       case "07":
           store = Storage.create({
               ultimoEmitido: fechaActual,
               tipoComprobante: "07",
               serie:null,
               numero:null
           }).then(function (success) {
               return callback(null,true);
           }).catch(function (err) {
               return callback(err)
           });
           break;
       case "08":
           store = Storage.create({
               ultimoEmitido: fechaActual,
               tipoComprobante: "08",
               serie:null,
               numero:null
           }).then(function (success) {
               return callback(null,true);
           }).catch(function (err) {
               return callback(err)
           });
           break;
   }    
}
function actualizarUltimo(store, data, callback) {
    //console.log(store);
    //console.log("deberia mostrar");
    obtenerFechaUltimoEmitido(data.tipoComprobante,function(err, ultimo){
      if(err)
        return callback(err);
      else {
          //console.log("gg");
          store.update({
              ultimoEmitido: data.fechaEmision,
              tipoComprobante: data.tipoComprobante,
              serie:data.serie,
              numero:data.numero
          }).then(function (ultimoEmitido) {
              return callback(null,ultimoEmitido)
          }).catch(function (err) {
              return callback(err);
          })
      }
    })

}
function obtenerFechaUltimoEmitido(tipo,callback) {
    //console.log(tipo);
    //var tipo = tipo;
    Storage.buscarUno(tipo)
        .then(function (ultimo1) {
            //console.log(ultimo1);
            //console.log(tipo);
            if(ultimo1){
                //console.log("aqui estoy perro");
                //console.log(ultimo1);
                return callback(null,ultimo1);
            }else{ 
                //console.log("aqui estoy perro2");
                //console.log(tipo);
                crearUltimo(tipo,function (err,ultimo2) {
                    //console.log("aqui estoy perro");
                    if(err)
                        return callback(err);
                    return callback(null,ultimo2);
                })
            }
        })
        .catch(function (err) {
            return callback(err);
        });

};
var Storager = function () {

};
Storager.prototype.getFechaUltimoRecibido = function (tipo,callback) {
    obtenerFechaUltimoEmitido(tipo,function (err,ultimo) {
        if(err) return callback(err);
        return callback(null,ultimo.ultimoEmitido);
    })

};
Storager.prototype.actualizarUltimoRecibido = function (data,callback) {
    //console.log("actualizando ultimo recibido");
    //console.log(data);
    obtenerFechaUltimoEmitido(data.tipoComprobante,function (err,ultimo) {
        if(err){
            //console.log(err);
          return callback(err);  
        } 
        //console.log(ultimo);
        actualizarUltimo(ultimo,data,function (err, ultimoActualizado) {
            if(err){
                //console.log(err);
                return callback(err);
            }
            return callback(null,ultimoActualizado);
        })
    })

};
module.exports = new Storager();
