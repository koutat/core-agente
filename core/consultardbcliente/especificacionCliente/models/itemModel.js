var item = {
  "valorUnitario"           : null,
  "precioVentaUnitario"     : null,
  "tipoPrecioVentaUnitario" : "01",
  "cantidad"                : null,
  "valorVenta"              : null,
  "descuento"               : 0,
  "codigoProducto"          : null,
  "unidadMedidaCantidad"    : null,
  "descripcion"             : null,
  "tipoAfectacionIgv"       : null,
  "montoAfectacionIgv"      : 0,
  "tipoAfectacionIsc"       : null,
  "montoAfectacionIsc"      : 0,
  "precioSugerido"          : 0,
  "porcentajeIsc"           : 0,
}
module.exports = item;
