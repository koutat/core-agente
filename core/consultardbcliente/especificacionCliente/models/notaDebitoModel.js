var NotaDebito= {
  "tipoComprobanteAfectado" : null,
  "tipoNotaDebito"          : null,
  "serieAfectado"           : null,
  "numeroAfectado"          : null,
  "descripcion"             : null,
  "serie"                   : null,
  "numero"                  : null,
  "receptor"                : {tipo: null, razonSocial: null, idReceptor: null},
  "tipoMoneda"              : null,
  "fechaEmision"            : null,
  "items"                   : [],
  "sumatoriaIgv"            : 0,
  "sumatoriaIsc"            : 0,
  "sumatoriaOtrosCargos"    : 0,
  "totalDescuentos"         : 0,
  "totalVentaGravada"       : 0,
  "totalVentaExonerada"     : 0,
  "totalVentaInafecta"      : 0,
  "totalVenta"              : 0,
  "docRelacionada"          : null,
  "adicional"               : {}
}
module.exports = NotaDebito;
