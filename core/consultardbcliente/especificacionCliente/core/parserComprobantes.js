var factura     = require("../models/facturaModel.js");
var boleta      = require("../models/boletaModel.js");
var notacredito = require("../models/notaCreditoModel.js");
var notadebito  = require("../models/notaCreditoModel.js");
var item        = require("../models/itemModel.js");
var common      = require("../models/common.json");
/*
  Las siguientes funciones estan encargadas de la generacion de JSON con la
  informacion de los distintos comprobantes, ademas de hacer calculos de algunos
  campos que en su mayoria no son encontrados en las BD de los clientes.
*/
var parseFactura = function(queryRes, callback){
  //Se instancia una copia del modelo de la factura
  var comprobante = JSON.parse(JSON.stringify(factura));
  //Lista de campos extraidos directamente de la BD.
  //comprobante.indexComprobante      =queryRes.NUM;      
  comprobante.tipoComprobante       = '01';
  comprobante.serie                 = queryRes.serie;
  comprobante.numero                = parseInt(queryRes.numero);
  comprobante.tipoMoneda            = queryRes.tipoMoneda;
  comprobante.fechaEmision          = queryRes.fechaEmision;
  comprobante.receptor              = queryRes.receptor;
  comprobante.receptor.tipo         = 6;
  comprobante.totalVenta            = queryRes.totalVenta;
  comprobante.sumatoriaIgv          = 0;
  comprobante.sumatoriaIsc          = 0;
  comprobante.sumatoriaOtrosCargos  = 0;
  comprobante.totalDescuentos       = 0;
  comprobante.descuentoGlobal       = 0;
  comprobante.totalVentaGratuita    = 0;
  comprobante.totalVentaGravada     = 0;
  comprobante.totalVentaExonerada   = 0;
  comprobante.totalVentaInafecta    = 0;
  comprobante.totalAnticipo         = 0;
  //Se verifica si es Anticipo
  if(queryRes.pk_1 == "002" && queryRes.pk_2 == "005")
    comprobante.adicional.tipoOperacion = "04";
  //Se itera en los items para obetener las sumatorias totales.
  queryRes.items.forEach(function(element){
    if(element.montoAfectacionIgv < 0){
      var comprobanteAnticipo = {"serie": null,"numero":null,"monto":null};
      comprobanteAnticipo.monto = Math.abs(parseFloat(element.totalAnticipo));
      comprobanteAnticipo.serie = element.serieAnticipo;
      comprobanteAnticipo.numero = element.numeroAnticipo;
      comprobante.totalAnticipo  = comprobante.totalAnticipo + comprobanteAnticipo.monto;
      if(!comprobante.adicional.documentosPrepagados){
        comprobante.adicional.documentosPrepagados = [];
        comprobante.adicional.documentosPrepagados.push(comprobanteAnticipo);
      }else{
        comprobante.adicional.documentosPrepagados.push(comprobanteAnticipo);
      }
    }else{
      //Se instancia una copia del modelo del item
      var itemComprobante = JSON.parse(JSON.stringify(item));
      //lista de campos de items extraidos directamente de la BD.
      itemComprobante.unidadMedidaCantidad    = element.unidadMedidaCantidad;
      itemComprobante.unidadMedidaCantidadName= element.unidadMedidaCantidadName;
      itemComprobante.descripcion             = element.descripcion;
      itemComprobante.codigoProducto          = element.codigoProducto;
      itemComprobante.cantidad                = element.cantidad;
      itemComprobante.precioVentaUnitario     = element.precioVentaUnitario;
      //itemComprobante.valorUnitario           = element.precioVentaUnitario/common.varGlobal.igv.porcentaje;
      //itemComprobante.valorVenta              = (element.precioVentaUnitario/common.varGlobal.igv.porcentaje)*element.cantidad;
      itemComprobante.montoAfectacionIgv      = element.montoAfectacionIgv;
      itemComprobante.tipoAfectacionIgv       = element.tipoAfectacionIgv || "10";
      itemComprobante.tipoPrecioVentaUnitario = element.tipoPrecioVentaUnitario;

      //parseando unidadMedida si es servicio
      if(element.unidadMedidaCantidad == "NULL"){
        itemComprobante.unidadMedidaCantidad = 'NIU';
      }
      //calculo del valorUnitario
      itemComprobante.valorUnitario           = element.precioVentaUnitario/common.varGlobal.igv.valor;
      //calculo del valorVenta
      itemComprobante.valorVenta              = (element.precioVentaUnitario/common.varGlobal.igv.valor)*element.cantidad;
      //si el item tiene ISC se le agrega a la sumatoria total.
      if (element.montoAfectacionIsc) {
        comprobante.sumatoriaIsc += element.montoAfectacionIsc;
      }
      //si el item tiene descuento se le agrega a la suma total de descuentos.
      if (element.descuento) {
        comprobante.totalDescuentos += element.descuento;
      }
      //añadimos a la sumatoria de venta gratuita si el valor venta es 0
      if (element.valorVenta == 0) {
        comprobante.totalVentaGratuita += element.precioVentaUnitario * element.cantidad;
        itemComprobante.tipoPrecioVentaUnitario = "02";
        itemComprobante.tipoAfectacionIgv = "12"; 
      } else {
        //if (element.tiposAfectacionIgv.tipoAfectacion.codigo == 1) {
          comprobante.totalVentaGravada += itemComprobante.valorVenta;
          itemComprobante.tipoPrecioVentaUnitario = "01";
        //} else {
          //if (element.tiposAfectacionIgv.tipoAfectacion.codigo == 2) {
            //comprobante.totalVentaExonerada += element.valorVenta;
          //} else {
            //if (element.tiposAfectacionIgv.tipoAfectacion.codigo == 3 || element.tiposAfectacionIgv.tipoAfectacion.codigo == 4) {
              //comprobante.totalVentaInafecta += element.valorVenta;
            //}
          //}
        //}
      }
      //Redondear calculos items a 2 decimales
      itemComprobante.montoAfectacionIgv = itemComprobante.montoAfectacionIgv.toFixed(2);
      itemComprobante.montoAfectacionIsc = itemComprobante.montoAfectacionIsc.toFixed(2);
      itemComprobante.descuento          = itemComprobante.descuento.toFixed(2);
      itemComprobante.valorUnitario      = itemComprobante.valorUnitario.toFixed(2);
      itemComprobante.valorVenta         = itemComprobante.valorVenta.toFixed(2);
      //Agregamos al array de items del comprobante
      comprobante.items.push(itemComprobante);
    }
  });
  if (comprobante.porcentajeDescuentoGlobal) {
    if (comprobante.totalVentaGravada > 0) {
      dsctoGlobal = 0.01 * comprobante.totalVentaGravada * comprobante.porcentajeDescuentoGlobal;
      comprobante.totalVentaGravada -= dsctoGlobal;
      comprobante.descuentoGlobal += dsctoGlobal;
    }
    if (comprobante.totalVentaExonerada > 0) {
      dsctoGlobal = 0.01 * comprobante.totalVentaExonerada * comprobante.porcentajeDescuentoGlobal;
      comprobante.totalVentaExonerada -= dsctoGlobal;
      comprobante.descuentoGlobal += dsctoGlobal;
    }
    if (dataBill.totalVentaInafecta > 0) {
        dsctoGlobal = 0.01 * comprobante.totalVentaInafecta * comprobante.porcentajeDescuentoGlobal;
        comprobante.totalVentaInafecta -= dsctoGlobal;
        comprobante.descuentoGlobal += dsctoGlobal;
    }
  }
  comprobante.sumatoriaIgv = (comprobante.totalVentaGravada + comprobante.sumatoriaIsc) * common.varGlobal.igv.porcentaje;
  comprobante.totalVenta   = comprobante.totalVentaGravada + comprobante.totalVentaExonerada + comprobante.totalVentaInafecta + comprobante.sumatoriaIgv + comprobante.sumatoriaIsc +comprobante.sumatoriaOtrosCargos;
  comprobante.importePercepcion = comprobante.porcentajePercepcion * comprobante.totalVenta * 0.01;
  //se validan y aplican los descuentos
  if (comprobante.descuentoGlobal && comprobante.totalDescuentos) {
    comprobante.totalDescuentos += comprobante.descuentoGlobal;
  } else if (comprobante.descuentoGlobal && !comprobante.totalDescuentos) {
    comprobante.totalDescuentos = comprobante.descuentoGlobal;
  }
  //Anticipos
  if(comprobante.totalAnticipo > 0){
    comprobante.totalVentaGravada = parseFloat(comprobante.totalVentaGravada.toFixed(2)) - parseFloat((comprobante.totalAnticipo/common.varGlobal.igv.valor).toFixed(2));
    comprobante.sumatoriaIgv = parseFloat(comprobante.sumatoriaIgv.toFixed(2)) - parseFloat(((comprobante.totalAnticipo*common.varGlobal.igv.porcentaje)/common.varGlobal.igv.valor).toFixed(2));
    comprobante.totalVenta = comprobante.totalVenta - comprobante.totalAnticipo;
    console.log(comprobante);
    //process.exit(0);
  } 
  //Redondear a 2 decimales los calculos generales
  comprobante.sumatoriaIsc         = comprobante.sumatoriaIsc.toFixed(2);
  comprobante.sumatoriaIgv         = comprobante.sumatoriaIgv.toFixed(2);
  comprobante.totalDescuentos      = comprobante.totalDescuentos.toFixed(2);
  comprobante.descuentoGlobal      = comprobante.descuentoGlobal.toFixed(2);
  comprobante.totalVentaGratuita   = comprobante.totalVentaGratuita.toFixed(2);
  comprobante.totalVentaGravada    = comprobante.totalVentaGravada.toFixed(2);
  comprobante.totalVentaExonerada  = comprobante.totalVentaExonerada.toFixed(2);
  comprobante.totalVentaInafecta   = comprobante.totalVentaInafecta.toFixed(2);
  comprobante.totalVenta           = comprobante.totalVenta.toFixed(2);
  comprobante.sumatoriaOtrosCargos = comprobante.sumatoriaOtrosCargos.toFixed(2);
  comprobante.importePercepcion    = comprobante.importePercepcion.toFixed(2);
  console.log(comprobante);
  return callback(null, comprobante);
}

var parseBoleta = function(queryRes, callback){
  var comprobante = JSON.parse(JSON.stringify(boleta));
  //Lista de campos extraidos directamente de la BD.
  console.log(queryRes);
  comprobante.tipoComprobante       = '03';
  comprobante.serie                 = queryRes.serie;
  comprobante.numero                = parseInt(queryRes.numero);
  comprobante.tipoMoneda            = queryRes.tipoMoneda;
  comprobante.fechaEmision          = queryRes.fechaEmision;
  comprobante.receptor              = queryRes.receptor;
  comprobante.totalVenta            = queryRes.totalVenta;
  //calculos de los campos faltantes para la factura.
  comprobante.sumatoriaIgv          = 0;
  comprobante.sumatoriaIsc          = 0;
  comprobante.sumatoriaOtrosCargos  = 0;
  comprobante.totalDescuentos       = 0;
  comprobante.descuentoGlobal       = 0;
  comprobante.totalVentaGratuita    = 0;
  comprobante.totalVentaGravada     = 0;
  comprobante.totalVentaExonerada   = 0;
  comprobante.totalVentaInafecta    = 0;

  switch (queryRes.receptor.tipoDocumento) {
    case '01':
      comprobante.receptor.tipo = '0';
      break;

    case '02':
      comprobante.receptor.tipo = '1';
      break;
    
    case '03':
      comprobante.receptor.tipo = '4';  
      break;

    case '05':
      comprobante.receptor.tipo = '7';
      break;

    default:
      break;
  }

  queryRes.items.forEach(function(element){
    var itemComprobante = JSON.parse(JSON.stringify(item));
    //lista de campos extraidos directamente de la BD.
    itemComprobante.unidadMedidaCantidad    = element.unidadMedidaCantidad;
    itemComprobante.descripcion             = element.descripcion;
    itemComprobante.codigoProducto          = element.codigoProducto;
    itemComprobante.cantidad                = element.cantidad;
    itemComprobante.precioVentaUnitario     = element.precioVentaUnitario;
    //itemComprobante.valorUnitario           = element.precioVentaUnitario;
    //itemComprobante.valorVenta              = element.valorVenta;
    itemComprobante.montoAfectacionIgv      = element.montoAfectacionIgv;
    itemComprobante.tipoAfectacionIgv       = element.tipoAfectacionIgv || "10";
    itemComprobante.tipoPrecioVentaUnitario = element.tipoPrecioVentaUnitario;

    //parseando unidadMedida si es servicio
    if(element.unidadMedidaCantidad == "NULL"){
      itemComprobante.unidadMedidaCantidad = 'NIU';
    }
    //calculo del valorUnitario
    itemComprobante.valorUnitario           = element.precioVentaUnitario/common.varGlobal.igv.valor;
    //calculo del valorVenta
    itemComprobante.valorVenta              = (element.precioVentaUnitario/common.varGlobal.igv.valor)*element.cantidad;

    if (element.montoAfectacionIsc) {
      comprobante.sumatoriaIsc += element.montoAfectacionIsc;
    }
    if (element.descuento) {
      comprobante.totalDescuentos += element.descuento;
    }
    if (element.valorVenta == 0) {
        comprobante.totalVentaGratuita += element.precioVentaUnitario * element.cantidad;
        itemComprobante.tipoPrecioVentaUnitario = "02";
        itemComprobante.tipoAfectacionIgv = "12"; 
    } else {
      //if (element.tiposAfectacionIgv.tipoAfectacion.codigo == 1) {
        comprobante.totalVentaGravada += itemComprobante.valorVenta;
        itemComprobante.tipoPrecioVentaUnitario = "01";
      //} else {
          //if (item.tiposAfectacionIgv.tipoAfectacion.codigo == 2) {
            //comprobante.totalVentaExonerada += item.valorVenta;
          //} else {
              //if (item.tiposAfectacionIgv.tipoAfectacion.codigo == 3 || item.tiposAfectacionIgv.tipoAfectacion.codigo == 4) {
                //comprobante.totalVentaInafecta += item.valorVenta;
              //}
          //}
      //}
    }
    //Agregamos al array de items del comprobante
    comprobante.items.push(itemComprobante);
  });
  comprobante.sumatoriaIgv = (comprobante.totalVentaGravada + comprobante.sumatoriaIsc) * common.varGlobal.igv.porcentaje;
  //comprobante.totalVenta   = comprobante.totalVentaGravada + comprobante.totalVentaExonerada + comprobante.totalVentaInafecta + comprobante.sumatoriaIgv + comprobante.sumatoriaIsc +comprobante.sumatoriaOtrosCargos;
  comprobante.importePercepcion = comprobante.porcentajePercepcion * comprobante.totalVenta * 0.01;
  if (comprobante.descuentoGlobal && comprobante.totalDescuentos) {
    comprobante.totalDescuentos += comprobante.descuentoGlobal;
  } else if (comprobante.descuentoGlobal && !comprobante.totalDescuentos) {
    comprobante.totalDescuentos = comprobante.descuentoGlobal;
  }
  //Anticipos
  if(comprobante.totalAnticipo > 0){
    comprobante.totalVentaGravada = parseFloat(comprobante.totalVentaGravada.toFixed(2)) - parseFloat((comprobante.totalAnticipo/common.varGlobal.igv.valor).toFixed(2));
    comprobante.sumatoriaIgv = parseFloat(comprobante.sumatoriaIgv.toFixed(2)) - parseFloat(((comprobante.totalAnticipo*common.varGlobal.igv.porcentaje)/common.varGlobal.igv.valor).toFixed(2));
    comprobante.totalVenta = comprobante.totalVenta - comprobante.totalAnticipo;
  }
  //Redondear a 2 decimales los calculos generales
  comprobante.sumatoriaIsc         = comprobante.sumatoriaIsc.toFixed(2);
  comprobante.sumatoriaIgv         = comprobante.sumatoriaIgv.toFixed(2);
  comprobante.totalDescuentos      = comprobante.totalDescuentos.toFixed(2);
  comprobante.descuentoGlobal      = comprobante.descuentoGlobal.toFixed(2);
  comprobante.totalVentaGratuita   = comprobante.totalVentaGratuita.toFixed(2);
  comprobante.totalVentaGravada    = comprobante.totalVentaGravada.toFixed(2);
  comprobante.totalVentaExonerada  = comprobante.totalVentaExonerada.toFixed(2);
  comprobante.totalVentaInafecta   = comprobante.totalVentaInafecta.toFixed(2);
  comprobante.totalVenta           = comprobante.totalVenta.toFixed(2);
  comprobante.sumatoriaOtrosCargos = comprobante.sumatoriaOtrosCargos.toFixed(2);
  comprobante.importePercepcion    = comprobante.importePercepcion.toFixed(2);
  //console.log(comprobante);
  return callback(null, comprobante);
}

var parseNotaCredito = function(queryRes, callback){
  var comprobante = JSON.parse(JSON.stringify(notacredito));
  //Lista de campos extraidos directamente de la BD.
  //console.log(queryRes);
  comprobante.tipoComprobanteAfectado = null;
  comprobante.tipoNotaCredito         = null;
  comprobante.serieAfectado           = null;
  comprobante.numeroAfectado          = parseInt(queryRes.numeroAfectado);
  comprobante.descripcion             = queryRes.descripcion;
  comprobante.fechaEmisionAfectado    = queryRes.fechaEmisionAfectado;
  comprobante.tipoComprobante         = '07';
  comprobante.serie                   = null;
  comprobante.numero                  = parseInt(queryRes.numero);
  comprobante.tipoMoneda              = queryRes.tipoMoneda;
  comprobante.fechaEmision            = queryRes.fechaEmision;
  comprobante.receptor                = queryRes.receptor;
  comprobante.totalVenta              = queryRes.totalVenta;
  //calculos de los campos faltantes para la factura.
  comprobante.sumatoriaIgv            = 0;
  comprobante.sumatoriaIsc            = 0;
  comprobante.sumatoriaOtrosCargos    = 0;
  comprobante.totalDescuentos         = 0;
  comprobante.descuentoGlobal         = 0;
  comprobante.totalVentaGratuita      = 0;
  comprobante.totalVentaGravada       = 0;
  comprobante.totalVentaExonerada     = 0;
  comprobante.totalVentaInafecta      = 0;

  switch (queryRes.receptor.tipoDocumento) {
    case '01':
      comprobante.receptor.tipo = '0';
      break;

    case '02':
      comprobante.receptor.tipo = '1';
      break;
    
    case '03':
      comprobante.receptor.tipo = '4';  
      break;
    case '04':
      comprobante.receptor.tipo = '6';  
      break;
    case '05':
      comprobante.receptor.tipo = '7';
      break;

    default:
      break;
  }

  if((queryRes.tipoComprobanteAfectado_k1 == '004' && queryRes.tipoComprobanteAfectado_k2 == '009') || (queryRes.tipoComprobanteAfectado_k1 == '002' && queryRes.tipoComprobanteAfectado_k2 == '004') || (queryRes.tipoComprobanteAfectado_k1 == '002' && queryRes.tipoComprobanteAfectado_k2 == '005')){
    comprobante.tipoComprobanteAfectado = '01';
    comprobante.serie                   = queryRes.serie;
    comprobante.serieAfectado           = queryRes.serieAfectado;
  }else{
    if((queryRes.tipoComprobanteAfectado_k1 == '003' && queryRes.tipoComprobanteAfectado_k2 == '007') || (queryRes.tipoComprobanteAfectado_k1 == '001' && queryRes.tipoComprobanteAfectado_k2 == '001') || (queryRes.tipoComprobanteAfectado_k1 == '001' && queryRes.tipoComprobanteAfectado_k2 == '002')){
      comprobante.tipoComprobanteAfectado = '03';
      comprobante.serie                   = queryRes.serie;
      comprobante.serieAfectado           = queryRes.serieAfectado;
    }
  }
  switch (queryRes.tipoNotaCredito) {
    case 0:
      comprobante.tipoNotaCredito = '01';
      break;
    case 1:
      comprobante.tipoNotaCredito = '08';
      break;
    case 2:
      comprobante.tipoNotaCredito = '04';
      if(comprobante.tipoComprobanteAfectado == '03')
        comprobante.tipoNotaCredito = '09';
      break;
    case 3:
      comprobante.tipoNotaCredito = '06';
      break;
    case 4:
      comprobante.tipoNotaCredito = '04';
      if(comprobante.tipoComprobanteAfectado == '03')
         comprobante.tipoNotaCredito = '09';
      break;
    case 5:
      comprobante.tipoNotaCredito = '04';
      if(comprobante.tipoComprobanteAfectado == '03')
        comprobante.tipoNotaCredito = '09';
      break;
    default:
      break;
  }

  queryRes.items.forEach(function(element){
    var itemComprobante = JSON.parse(JSON.stringify(item));
    //lista de campos extraidos directamente de la BD.
    itemComprobante.unidadMedidaCantidad    = element.unidadMedidaCantidad;
    itemComprobante.descripcion             = element.descripcion;
    itemComprobante.codigoProducto          = element.codigoProducto;
    itemComprobante.cantidad                = element.cantidad;
    itemComprobante.precioVentaUnitario     = element.precioVentaUnitario;
    //itemComprobante.valorUnitario           = element.valorUnitario;
    //itemComprobante.valorVenta              = element.valorVenta;
    itemComprobante.montoAfectacionIgv      = element.montoAfectacionIgv;
    itemComprobante.tipoAfectacionIgv       = element.tipoAfectacionIgv || "10";
    itemComprobante.tipoPrecioVentaUnitario = element.tipoPrecioVentaUnitario;

    //parseando unidadMedida si es servicio
    if(element.unidadMedidaCantidad == "NULL"){
      itemComprobante.unidadMedidaCantidad = 'NIU';
    }
    //calculo del valorUnitario
    itemComprobante.valorUnitario           = element.precioVentaUnitario/common.varGlobal.igv.valor;
    //calculo del valorVenta
    itemComprobante.valorVenta              = (element.precioVentaUnitario/common.varGlobal.igv.valor)*element.cantidad;

    if (element.montoAfectacionIsc) {
      comprobante.sumatoriaIsc += element.montoAfectacionIsc;
    }
    if (element.descuento) {
      comprobante.totalDescuentos += element.descuento;
    }
    if (element.valorVenta == 0) {
        comprobante.totalVentaGratuita += element.precioVentaUnitario * element.cantidad;
        itemComprobante.tipoPrecioVentaUnitario = "02";
        itemComprobante.tipoAfectacionIgv = "12"; 
    } else {
      //if (element.tiposAfectacionIgv.tipoAfectacion.codigo == 1) {
        comprobante.totalVentaGravada += itemComprobante.valorVenta;
        itemComprobante.tipoPrecioVentaUnitario = "01";
      //} else {
          //if (item.tiposAfectacionIgv.tipoAfectacion.codigo == 2) {
            //comprobante.totalVentaExonerada += item.valorVenta;
          //} else {
              //if (item.tiposAfectacionIgv.tipoAfectacion.codigo == 3 || item.tiposAfectacionIgv.tipoAfectacion.codigo == 4) {
                //comprobante.totalVentaInafecta += item.valorVenta;
              //}
          //}
      //}
    }
    //Agregamos al array de items del comprobante
    comprobante.items.push(itemComprobante);
  });
  comprobante.sumatoriaIgv = (comprobante.totalVentaGravada + comprobante.sumatoriaIsc) * common.varGlobal.igv.porcentaje;
  //comprobante.totalVenta   = comprobante.totalVentaGravada + comprobante.totalVentaExonerada + comprobante.totalVentaInafecta + comprobante.sumatoriaIgv + comprobante.sumatoriaIsc +comprobante.sumatoriaOtrosCargos;
  comprobante.importePercepcion = comprobante.porcentajePercepcion * comprobante.totalVenta * 0.01;
  if (comprobante.descuentoGlobal && comprobante.totalDescuentos) {
    comprobante.totalDescuentos += comprobante.descuentoGlobal;
  } else if (comprobante.descuentoGlobal && !comprobante.totalDescuentos) {
    comprobante.totalDescuentos = comprobante.descuentoGlobal;
  }
  //Redondear a 2 decimales los calculos generales
  comprobante.sumatoriaIsc         = comprobante.sumatoriaIsc.toFixed(2);
  comprobante.sumatoriaIgv         = comprobante.sumatoriaIgv.toFixed(2);
  comprobante.totalDescuentos      = comprobante.totalDescuentos.toFixed(2);
  comprobante.descuentoGlobal      = comprobante.descuentoGlobal.toFixed(2);
  comprobante.totalVentaGratuita   = comprobante.totalVentaGratuita.toFixed(2);
  comprobante.totalVentaGravada    = comprobante.totalVentaGravada.toFixed(2);
  comprobante.totalVentaExonerada  = comprobante.totalVentaExonerada.toFixed(2);
  comprobante.totalVentaInafecta   = comprobante.totalVentaInafecta.toFixed(2);
  comprobante.totalVenta           = comprobante.totalVenta.toFixed(2);
  comprobante.sumatoriaOtrosCargos = comprobante.sumatoriaOtrosCargos.toFixed(2);
  comprobante.importePercepcion    = comprobante.importePercepcion.toFixed(2);
  return callback(null, comprobante);
}

var parseNotaDebito = function(queryRes, callback){
  var comprobante = JSON.parse(JSON.stringify(notadebito));
  //Lista de campos extraidos directamente de la BD.
  comprobante.tipoComprobanteAfectado = null;
  comprobante.tipoNotaDebito          = '02';
  comprobante.serieAfectado           = null;
  comprobante.numeroAfectado          = parseInt(queryRes.numeroAfectado);
  comprobante.descripcion             = queryRes.descripcion;
  comprobante.fechaEmisionAfectado    = queryRes.fechaEmisionAfectado;
  comprobante.tipoComprobante         = '08';
  comprobante.serie                   = null;
  comprobante.numero                  = parseInt(queryRes.numero);
  comprobante.tipoMoneda              = queryRes.tipoMoneda;
  comprobante.fechaEmision            = queryRes.fechaEmision;
  comprobante.receptor                = queryRes.receptor;
  comprobante.totalVenta              = queryRes.totalVenta;
  //calculos de los campos faltantes para la factura.
  comprobante.sumatoriaIgv            = 0;
  comprobante.sumatoriaIsc            = 0;
  comprobante.sumatoriaOtrosCargos    = 0;
  comprobante.totalDescuentos         = 0;
  comprobante.descuentoGlobal         = 0;
  comprobante.totalVentaGratuita      = 0;
  comprobante.totalVentaGravada       = 0;
  comprobante.totalVentaExonerada     = 0;
  comprobante.totalVentaInafecta      = 0;

  switch (queryRes.receptor.tipoDocumento) {
    case '01':
      comprobante.receptor.tipo = '0';
      break;

    case '02':
      comprobante.receptor.tipo = '1';
      break;
    
    case '03':
      comprobante.receptor.tipo = '4';  
      break;
    case '04':
      comprobante.receptor.tipo = '6';  
      break;
    case '05':
      comprobante.receptor.tipo = '7';
      break;

    default:
      break;
  }

  if((queryRes.tipoComprobanteAfectado_k1 == '004' && queryRes.tipoComprobanteAfectado_k2 == '009') || (queryRes.tipoComprobanteAfectado_k1 == '002' && queryRes.tipoComprobanteAfectado_k2 == '004') || (queryRes.tipoComprobanteAfectado_k1 == '002' && queryRes.tipoComprobanteAfectado_k2 == '005')){
    comprobante.tipoComprobanteAfectado = '01';
    comprobante.serie                   = queryRes.serie;
    comprobante.serieAfectado           = queryRes.serieAfectado;
  }else{
    if((queryRes.tipoComprobanteAfectado_k1 == '003' && queryRes.tipoComprobanteAfectado_k2 == '007') || (queryRes.tipoComprobanteAfectado_k1 == '001' && queryRes.tipoComprobanteAfectado_k2 == '001') || (queryRes.tipoComprobanteAfectado_k1 == '001' && queryRes.tipoComprobanteAfectado_k2 == '002')){
      comprobante.tipoComprobanteAfectado = '03';
      comprobante.serie                   = queryRes.serie;
      comprobante.serieAfectado           = queryRes.serieAfectado;
    }
  }
  // switch (queryRes.tipoNotaCredito) {
  //   case 0:
  //     comprobante.tipoNotaCredito = '01';
  //     break;
  //   case 1:
  //     comprobante.tipoNotaCredito = '08';
  //     break
  //   case 2:
  //     comprobante.tipoNotaCredito = '04';
  //     break
  //   case 3:
  //     comprobante.tipoNotaCredito = '06';
  //     break
  //   case 4:
  //     comprobante.tipoNotaCredito = '04';
  //     break
  //   case 5:
  //     comprobante.tipoNotaCredito = '04';  
  //     break      
  //   default:
  //     break;
  // }

  queryRes.items.forEach(function(element){
    var itemComprobante = JSON.parse(JSON.stringify(item));
    //lista de campos extraidos directamente de la BD.
    itemComprobante.unidadMedidaCantidad    = element.unidadMedidaCantidad;
    itemComprobante.descripcion             = element.descripcion;
    itemComprobante.codigoProducto          = element.codigoProducto;
    itemComprobante.cantidad                = element.cantidad;
    itemComprobante.precioVentaUnitario     = element.precioVentaUnitario;
    //itemComprobante.valorUnitario           = element.valorUnitario;
    //itemComprobante.valorVenta              = element.valorVenta;
    itemComprobante.montoAfectacionIgv      = element.montoAfectacionIgv;
    itemComprobante.tipoAfectacionIgv       = element.tipoAfectacionIgv || "10";
    itemComprobante.tipoPrecioVentaUnitario = element.tipoPrecioVentaUnitario;


    //parseando unidadMedida si es servicio
    if(element.unidadMedidaCantidad == "NULL"){
      itemComprobante.unidadMedidaCantidad = 'NIU';
    }
    //calculo del valorUnitario
    itemComprobante.valorUnitario           = element.precioVentaUnitario/common.varGlobal.igv.valor;
    //calculo del valorVenta
    itemComprobante.valorVenta              = (element.precioVentaUnitario/common.varGlobal.igv.valor)*element.cantidad;

    if (element.montoAfectacionIsc) {
      comprobante.sumatoriaIsc += element.montoAfectacionIsc;
    }
    if (element.descuento) {
      comprobante.totalDescuentos += element.descuento;
    }
    if (element.valorVenta == 0) {
        comprobante.totalVentaGratuita += element.precioVentaUnitario * element.cantidad;
        itemComprobante.tipoPrecioVentaUnitario = "02";
        itemComprobante.tipoAfectacionIgv = "12"; 
    } else {
      //if (element.tiposAfectacionIgv.tipoAfectacion.codigo == 1) {
        comprobante.totalVentaGravada += itemComprobante.valorVenta;
        itemComprobante.tipoPrecioVentaUnitario = "01";
      //} else {
          //if (item.tiposAfectacionIgv.tipoAfectacion.codigo == 2) {
            //comprobante.totalVentaExonerada += item.valorVenta;
          //} else {
              //if (item.tiposAfectacionIgv.tipoAfectacion.codigo == 3 || item.tiposAfectacionIgv.tipoAfectacion.codigo == 4) {
                //comprobante.totalVentaInafecta += item.valorVenta;
              //}
          //}
      //}
    }
    //Agregamos al array de items del comprobante
    comprobante.items.push(itemComprobante);
  });
  comprobante.sumatoriaIgv = (comprobante.totalVentaGravada + comprobante.sumatoriaIsc) * common.varGlobal.igv.porcentaje;
  //comprobante.totalVenta   = comprobante.totalVentaGravada + comprobante.totalVentaExonerada + comprobante.totalVentaInafecta + comprobante.sumatoriaIgv + comprobante.sumatoriaIsc +comprobante.sumatoriaOtrosCargos;
  comprobante.importePercepcion = comprobante.porcentajePercepcion * comprobante.totalVenta * 0.01;
  if (comprobante.descuentoGlobal && comprobante.totalDescuentos) {
    comprobante.totalDescuentos += comprobante.descuentoGlobal;
  } else if (comprobante.descuentoGlobal && !comprobante.totalDescuentos) {
    comprobante.totalDescuentos = comprobante.descuentoGlobal;
  }
  //Redondear a 2 decimales los calculos generales
  comprobante.sumatoriaIsc         = comprobante.sumatoriaIsc.toFixed(2);
  comprobante.sumatoriaIgv         = comprobante.sumatoriaIgv.toFixed(2);
  comprobante.totalDescuentos      = comprobante.totalDescuentos.toFixed(2);
  comprobante.descuentoGlobal      = comprobante.descuentoGlobal.toFixed(2);
  comprobante.totalVentaGratuita   = comprobante.totalVentaGratuita.toFixed(2);
  comprobante.totalVentaGravada    = comprobante.totalVentaGravada.toFixed(2);
  comprobante.totalVentaExonerada  = comprobante.totalVentaExonerada.toFixed(2);
  comprobante.totalVentaInafecta   = comprobante.totalVentaInafecta.toFixed(2);
  comprobante.totalVenta           = comprobante.totalVenta.toFixed(2);
  comprobante.sumatoriaOtrosCargos = comprobante.sumatoriaOtrosCargos.toFixed(2);
  comprobante.importePercepcion    = comprobante.importePercepcion.toFixed(2);

  return callback(null, comprobante);
}

module.exports = {
  parseFactura: parseFactura,
  parseBoleta: parseBoleta,
  parseNotaCredito : parseNotaCredito,
  parseNotaDebito: parseNotaDebito
}
