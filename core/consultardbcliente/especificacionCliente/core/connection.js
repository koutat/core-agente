var Sequelize = require( "sequelize" );
var dbConfig = require("../../../configuraciones/appConfig").appConfig.dbClient;
var config = {
  "host" : dbConfig.host ,
  "port" : dbConfig.port ,
  "username" : dbConfig.username ,
  "password" : dbConfig.password ,
  "dialect" : dbConfig.dialect ,
  "database" : dbConfig.dbname ,
  // "timezone" : "-05:00" ,
  "logging" : false,
  "charset":"utf8",
  "pool" : {
  "maxConnections" : 10 ,
  "minConnections" : 0 ,
  "maxIdleTime" : 10000
} ,
"define" : {
  "timestamps" : false
},
// storage: process.cwd()+'/../Config/DB/DBLocal.sqlite'

};
module.exports = new Sequelize( config.database , config.username , config.password , config );
