var connection  = require("./connection");
var db          = require("../../../../custom/dbClient.json");
var parser      = require("./parserComprobantes");
var storager    = require("../../storager");
var moment      = require("moment");
var Emiter      = require("../../../verificarconeccion");
var query       = require("../../../../custom/query");

//Funcion que recibe una consulta (1er parametro) elaborada para un tipo de comprobante.
var consultarComprobantes = function (queryComprobante, tipo,callback) {

  storager.getFechaUltimoRecibido(tipo,function (err,fecha) {
    //console.log(fecha);
    if(fecha || fecha == 0){
      //console.log(fecha);
      var date = moment(fecha).add(5,'hours').format('YYYY-MM-DD HH:mm:ss.SSS');
      //console.log(date);
      var consulta = queryComprobante + ") A WHERE fechaEmision > CONVERT(DATETIME, '"+date+"') AND serie = 'F022' and numero= 254 ORDER BY fechaEmision ASC" ;
      //var consulta = queryComprobante + ") A WHERE fechaEmision > '2016-12-10 15:00:00.000'"
      // if (tipo == '01'){
      //   var consulta = queryComprobante + ") A WHERE NUM > "+ultimo+" AND  LEFT(serie,1) = 'F'";
      // }else{
      //   if(tipo == '03'){
      //     var consulta = queryComprobante + ") A WHERE NUM > "+ultimo+" AND  LEFT(serie,1) = 'B'";
      //   }else{
      //      var consulta = queryComprobante + ") A WHERE NUM > "+ultimo+" AND  (LEFT(serie,1) = 'B' OR LEFT(serie,1) = 'F')";
      //   }
      // }
      console.log(consulta);
      connection.query(consulta) //conexion a la BD para traer la data del comprobante.
        .then(function (data) {
          console.log(data[0]);
          return callback(null, data[0]); // funcion de retorno con la data
        })
        .catch(function (error) {
          return callback(error, null); // funcion de retorno con el error
        })
      }else{
        return callback("Inicializando Agente",null);
      }
  });
};


//Funcion que recibe una consulta (1er parametro) elaborada para los items, ademas de el id de la factura (para este caso pk1, pk2, pk3, pk4).
var consultarItemsComprobantes = function (queryItems, pk1, pk2, pk3, pk4, callback) {
  var query = queryItems+pk1+" = "+db.campos.item.idComprobante.idFactura.pk_1+" AND "+ //el id de la factura se añade a la consulta de los items
  pk2+" = "+db.campos.item.idComprobante.idFactura.pk_2+" AND "+ // para relacionar los items y su comprobante
  "'"+pk3+"'"+" = "+db.campos.item.idComprobante.idFactura.pk_3+" AND "+
  pk4+" = "+db.campos.item.idComprobante.idFactura.pk_4;

  connection.query(query).then(function(data){ //Conexion a la BD para traer data de los items.
    return callback(null, data[0]); // funcion de retorno con la data
  }).catch(function(error){
    return callback(error, null); // funcion de retorno con el error
  })
};

//Funcion que recibe como parametro la consulta para el receptor del comprobante (1er parametro)
var consultarReceptor = function (queryReceptor, idCliente, callback){
  var query = queryReceptor+idCliente; // se añade el idCliente (2do parametro) para relacionar el cliente y la factura.
  connection.query(query)
  .then(function(data){
    //console.log(data[0][0]);
    return callback(null, data[0][0]); // funcion de retorno con la data
  })
  .catch(function(error) {
    return callback(error, null); // funcion de retorno con el error
  })
};

var obtenerTipoComprobanteQuery = function(tipo, callback){
  switch (tipo) {
      case 1:
          return callback(null, query.facturas, "01", query.receptorFactura);
          //break;
      case 3:
          return callback(null, query.boletas, "03", query.receptorBoleta);
          //break;
      case 7:
          return callback(null, query.notasCredito, "07",  null);
          //break;
      case 8:
          return callback(null, query.notasDebito, "08",  null);
          //break;
      default:
          break;    
  }
}
var eliminarCamposNulos          = function (preComprobante, callback) {
   for (var i in preComprobante) {
       if (preComprobante[i] === null) {
           delete preComprobante[i];
       }
   }
   return callback(preComprobante);
};
var Mining = function() {};
Mining.prototype.consultar = function(tipo, callback){
  obtenerTipoComprobanteQuery(tipo, function(err, queryComprobante, tipoComprobante, queryReceptor){
    if(err)
      return callback(err);
    else {
      consultarComprobantes(queryComprobante, tipoComprobante,function(err, data){
        if (err)
          return callback(err);
        else {
          var ultimo = data[data.length - 1];
          // console.log(ultimo);
          // process.exit();
          if(ultimo){
            storager.actualizarUltimoRecibido(ultimo,function (err,success) {
              if(err){
                return callback(err);
              }
              else{
                data.forEach(function(element){
                    if((element.pk_1 == "008" && element.pk_2 == "017") || (element.pk_1 == "009" && element.pk_2 == "018")) {
                      if(element.tipoComprobanteAfectado_k1 == '004' || element.tipoComprobanteAfectado_k1 == '002'){
                        //console.log("soy factura");
                        queryReceptor = query.receptorFacturaNota;
                      }else{
                        if(element.tipoComprobanteAfectado_k1 == '003' || element.tipoComprobanteAfectado_k1 == '001'){
                          queryReceptor = query.receptorBoletaNota;
                          //console.log("soy boleta");
                        }
                      }
                    }

                      consultarReceptor(queryReceptor, element.idCliente, function(err, receptor){
                        if(err){
                          return callback(err);
                        }else {
                          element.receptor = receptor;
                          consultarItemsComprobantes(query.items, element.pk_1, element.pk_2, element.pk_3, element.pk_4, function(err, items){

                            if(err){
                              return callback(err)
                            }else {
                              element.items = items;
                              switch (tipoComprobante) {
                                case '01':
                                  parser.parseFactura(element, function(err, comprobante){
                                    if (err) return callback(err);
                                    else {
                                      var comprobante2             = {};
                                      comprobante2.tipoComprobante = comprobante.tipoComprobante;
                                      comprobante2.body            = comprobante;
                                      comprobante2.body.tipoComprobante = comprobante.tipoComprobante;
                                      eliminarCamposNulos(comprobante2.body, function (preComprobante) {
                                        comprobante2.body = preComprobante;
                                        comprobante2.body.receptor = JSON.stringify(comprobante2.body.receptor);
                                        Emiter.connect(comprobante2, function (err, succes) {
                                          if (err){
                                            return callback(err,null);
                                          }else{
                                            return callback(null,succes)
                                          }
                                        })
                                      });
                                    }
                                  });
                                break;
                                case '03':
                                  parser.parseBoleta(element, function(err, comprobante){
                                    if (err) return callback(err);
                                    else {
                                      var comprobante2             = {};
                                      comprobante2.tipoComprobante = comprobante.tipoComprobante;
                                      comprobante2.body            = comprobante;
                                      comprobante2.body.tipoComprobante = comprobante.tipoComprobante;
                                      eliminarCamposNulos(comprobante2.body, function (preComprobante) {
                                        comprobante2.body = preComprobante;
                                        comprobante2.body.receptor = JSON.stringify(comprobante2.body.receptor);
                                        Emiter.connect(comprobante2, function (err, succes) {
                                          if (err){
                                            return callback(err,null);
                                          }else{
                                            return callback(null,succes)
                                          }
                                        })
                                      });
                                    }
                                  });
                                break;
                                case '07':
                                  parser.parseNotaCredito(element, function(err, comprobante){
                                    if (err) return callback(err);
                                    else {
                                      var comprobante2             = {};
                                      comprobante2.tipoComprobante = comprobante.tipoComprobante;
                                      comprobante2.body            = comprobante;
                                      comprobante2.body.tipoComprobante = comprobante.tipoComprobante;
                                      eliminarCamposNulos(comprobante2.body, function (preComprobante) {
                                        comprobante2.body = preComprobante;
                                        comprobante2.body.receptor = JSON.stringify(comprobante2.body.receptor);
                                        Emiter.connect(comprobante2, function (err, succes) {
                                          if (err){
                                            return callback(err,null);
                                          }else{
                                            return callback(null,succes)
                                          }
                                        })
                                      });
                                    }
                                  });
                                break;
                                case '08':
                                  parser.parseNotaDebito(element, function(err, comprobante){
                                    if (err) return callback(err);
                                    else {
                                      var comprobante2             = {};
                                      comprobante2.tipoComprobante = comprobante.tipoComprobante;
                                      comprobante2.body            = comprobante;
                                      comprobante2.body.tipoComprobante = comprobante.tipoComprobante;
                                      eliminarCamposNulos(comprobante2.body, function (preComprobante) {
                                        comprobante2.body = preComprobante;
                                        comprobante2.body.receptor = JSON.stringify(comprobante2.body.receptor);
                                        Emiter.connect(comprobante2, function (err, succes) {
                                          if (err){
                                            return callback(err,null);
                                          }else{
                                            return callback(null,succes)
                                          }
                                        })
                                      });
                                    }
                                  });
                                  break;
                              }
                            }
                          })
                        }
                      });

                });
              }
            });
          }else{
            return callback(err);
          }
        }
      })
    }
  })
}
module.exports = new Mining();
