/**
 * Created by Luis on 21/07/2016.
 */
/**
 * Created by Luis on 21/04/2016.
 */
var CronJob             = require('cron').CronJob;
//var Excavator           = require('./excavator');
var Mining              = require('./especificacionCliente/core/queryCliente');
var async               = require('async');
var logger              = require('../logger/logger');

function ExcavatorCron() {}
/*
 worker
 */
ExcavatorCron.prototype.workerStarT= function() {
    var i = 0;
    var job = new CronJob({
        cronTime: '*/15 * * * * *',
        onTick: function() {
            async.parallel([
                function () {
                    var req = {};
                    req.tipoComprobante = "01";
                    Mining.consultar(1,function (err,respuesta) {
                        if(err) {
                            logger.log("error",err);
                        }else if(respuesta){
                             logger.log("info","la factura "+respuesta.serie+"-"+respuesta.numero+" ha sido recibida por el agente registrada correctamente");
                            //console.log("la factura "+respuesta.serie+"-"+respuesta.numero+" ha sido recibida por el agente  correctamente");
                        }
                    });


                },
                function () {
                    var req = {};
                    req.tipoComprobante = "03";
                    Mining.consultar(3,function (err,respuesta) {
                        if(err) {
                            logger.log("error",err);
                        }else if(respuesta){
                            logger.log("info","la boleta "+respuesta.serie+"-"+respuesta.numero+" ha sido recibida por el agente registrada correctamente");
                            //console.log("la boleta "+respuesta.serie+"-"+respuesta.numero+" ha sido recibida por el agente  correctamente");
                        }
                    });
                },
                function () {
                    var req = {};
                    req.tipoComprobante = "07";
                    Mining.consultar(7,function (err,respuesta) {
                        if(err) {
                            logger.log("error",err);
                        }else if(respuesta){
                            logger.log("info","la nota de crédito "+respuesta.serie+"-"+respuesta.numero+" ha sido recibida por el agente registrada correctamente");
                            //console.log("la nota de crédito  "+respuesta.serie+"-"+respuesta.numero+" ha sido recibida por el agente  correctamente");
                        }
                    });
                },
                function () {
                    var req = {};
                    req.tipoComprobante = "08";
                    Mining.consultar(8,function (err,respuesta) {
                        if(err) {
                            logger.log("error",err);
                        }else if(respuesta){
                            logger.log("info","la nota de debito "+respuesta.serie+"-"+respuesta.numero+" ha sido recibida por el agente registrada correctamente");
                            //console.log("la nota de debito  "+respuesta.serie+"-"+respuesta.numero+" ha sido recibida por el agente  correctamente");
                        }
                    });
                }
            ], function () {

            });
        },
        start: false,
        timeZone: 'America/Los_Angeles'
    });
    job.start();
};

module.exports = new ExcavatorCron();
