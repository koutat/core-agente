var fs = require("fs");

module.exports = {
	crt : fs.readFileSync(__dirname + "/../../config/client/cert/cert.crt","utf-8"),
	key : fs.readFileSync(__dirname + "/../../config/client/cert/cert.key","utf-8")
};