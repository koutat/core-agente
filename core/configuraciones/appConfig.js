/**
 * Created by Luis on 08/04/2016.
 */
var clientData  = require("../../config/client/clientData.json");
var config = {
    emisor:{
        idEmisor:clientData.emisor.ruc,
        razonSocial:clientData.emisor.razonSocial,
        nombreComercial:clientData.emisor.nombreComercial,
        domicilio:{
            ubigeo:clientData.emisor.domicilio.ubigeo,
            direccion: clientData.emisor.domicilio.direccion,
            departamento: clientData.emisor.domicilio.departamento,
            provincia: clientData.emisor.domicilio.provincia,
            distrito: clientData.emisor.domicilio.distrito
        }
    },

    server: clientData.server,
    proxy : clientData.proxy,
    // server:"http://192.168.1.109:3000",

    accessKey       : clientData.accessKey,
    secretKey       : clientData.secretKey,

    dbClient        :{
        dbname      : clientData.dbClient.dbname,
        username    : clientData.dbClient.username,
        password    : clientData.dbClient.password,
        port        : clientData.dbClient.port,
        host        : clientData.dbClient.host,
        dialect     : clientData.dbClient.dialect
        // utcDiference: "+02:00",
        // utcDiferenceValue:"2"
    }
};

module.exports.appConfig = config;
