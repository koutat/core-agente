var Sequelize = require( "sequelize" );
var dbConfig  = require( "../../config/own/dbConfig.json").connection;
var config = {
	"host" :     dbConfig.host ,
	"port" :     dbConfig.port ,
	"username" : dbConfig.username ,
	"password" : dbConfig.password ,
	"dialect" :  "mysql" ,
	"database" : dbConfig.database ,
	"timezone" : dbConfig.utcDiference ,
	"logging" : false ,
	"charset":"utf8" ,
	"pool" :     {
		"maxConnections" : 10 ,
		"minConnections" : 0 ,
		"maxIdleTime" :    10000
	} ,
	"define" :   {
		"timestamps" : false
	}
};
module.exports = new Sequelize( config.database , config.username , config.password , config );
