/**
 * Created by Luis on 18/04/2016.
 */
var Factura             = require( '../../models' ).Factura;
var FacturaItem         = require( '../../models' ).FacturaItem;
// var FacturaExtra        = require( '../../models' ).FacturaExtra;
var sequelize           = require( '../../models' ).sequelize;
var Sequelize           = require( '../../models' ).Sequelize;
var async               = require("async");
var xmlhelper           = require( "../../sunatws/xmlhelper" );
// var pdfComponent        = require("../../pdf");
var utils               = require("../../utils");
var ApiError            = require("../../error/apiError");
var conf                = require("../../configuraciones/appConfig");
//var connection          = require("../../consultardbcliente/especificacioncliente/core/connection");
function FacturaService(){

}
function crearFactura(datos,callback){
  
    var facturaReq = datos.body || null;
    var factura = Factura.build( facturaReq , {
                include : [
                    {
                        model : FacturaItem ,
                        as :    "items"
                    }
                ]
            } );
    return callback(null,factura);
}
function guardarFactura( datos , callback ) {
    var transaction = sequelize.transaction(
        function( t ) {
            return datos.save( { transaction : t , validate : false } )
                .then( function( factura ) {
                    return factura;
                } )
                .catch( function( error ) {
                    throw error;
                } )
        }
    );

    transaction
        .then( function( factura ) {
            factura.reload()
                .then( function( freloaded ) {
                    return callback( null , freloaded );
                } )
                .catch( function( e ) {
                    throw e;
                } )
        } )
        .catch( function( error ) {
            return callback( error );
        } );
}
function cleanRequest( req , next ) {

    var facturaReq = req.body || {};

    if( !facturaReq ) {
        return next( null , req );
    }

    var filtered;
    var fieldsParentNotSave = Factura.getFieldsNotSave();

    filtered = utils.cleanObject( facturaReq , fieldsParentNotSave );

    var fieldsChildrenNotSave = FacturaItem.getFieldsNotSave();
    var items                 = facturaReq.items || [];
    var filteredItems         = [];

    if( Array.isArray( items ) && items.length > 0 ) {

        items.forEach( function( item ) {
            filteredItems.push( utils.cleanObject( item , fieldsChildrenNotSave ) );
        } );
        filtered.items = filteredItems;
    }

    req.body = filtered;

    return next( null , req );

}
function validarFormatoFactura( facturaReq , callback ) {

    var error;
    facturaReq.validate()
        .then( function( validationError ) {
            if( validationError ) {
                throw validationError;
            }
            return facturaReq;
        } )
        .then( function( facturaReq ) {
            return Sequelize.Promise.map( facturaReq.items , function( item ) {
                return item.validate()
                    .then( function( validError ) {
                        if( validError ) {
                            throw validError;
                        } else {
                            return item;
                        }
                    } )
                    .catch( function( error ) {
                        throw error;
                    } )
            } ).then( function( items ) {
                return facturaReq;
            } )
        } )
        .then( function( facturaValid ) {

            return callback( null , facturaValid );
        } )
        .catch( Sequelize.ValidationError , function( validationError ) {
            // console.log(validationError);

            error = new ApiError( "3000" , 400 , validationError.errors.shift().message );
            return callback(error,null);
        } )
        .catch( function( err ) {
            return callback( err );
        } );
}
function construirXml( datos , callback ) {
    Factura.findOneWithAllDataById( datos.idFactura )
        .then( function( facturaToBuild ) {
            facturaToBuild.receptor = JSON.parse(facturaToBuild.receptor);
            xmlhelper.buildXml( facturaToBuild , conf.appConfig.emisor , function( err , xmlObject ) {
                if( err ) {
                    throw err;
                }
                datos.set( "signatureValue" , xmlObject.signatureValue,{save:true} );
                datos.set( "digestValue" , xmlObject.digestValue,{save:false} );
                datos.set("xml",xmlObject.xmlString,{save:true});
                datos.save();

                return callback( null , datos );
            } );
        } )
        .catch( function( error ) {
            return callback( error );
        } );

}
FacturaService.prototype.buscarNoEmitidos = function(callback ) {
    Factura.findAllNoEmitedWithAllData( )
        .then( function( facturas ) {
                return callback( null , facturas );
            } )
        .catch( function( error ) {
            return callback( error );
        } );
}
FacturaService.prototype.buscarComprobantesDelDia = function(callback ) {
    Factura.findAllTodayReportWithAllData( )
        .then( function( facturas ) {
            return callback( null , facturas );
        } )
        .catch( function( error ) {
            return callback( error );
        } );
};
FacturaService.prototype.validarComprobante= function(req , callback ) {
    async.waterfall( [
        async.constant( req ) ,
        cleanRequest ,
        crearFactura ,
        validarFormatoFactura
    ] , function( err , facturaValid ) {
        if( err ) {
            return callback( err,null );
        }
        return callback( null , facturaValid );
    } );
};
FacturaService.prototype.saveComprobante = function(datos, callback){
    //console.log("hola fucking mundo!!");
    async.waterfall(
        [
            async.constant( datos ) ,
            // crearBoleta ,
            guardarFactura,
            construirXml // not working
        ] , function( err , factura ) {
            if( err ) {
                return callback( err )
            }
            return callback( null , factura.pretty() );
        } );
};
// BoletaService.prototype.generatePdf = function( req , callback ) {
//     var factura = req.comprobante;
//     var emisor  = req.emisor;
//
//     pdfComponent.generatePdf( emisor , factura , function( err , pdfbuffer ) {
//         if( err ) {
//             // var error = new ApiError( 3010 , 500 );
//             // error.setExtra( err.toString() );
//             console.log(err);
//             return callback( error );
//         } else {
//             return callback( null , pdfbuffer );
//         }
//     } );
// };
// BoletaService.prototype.generateXml = function( req , callback ) {
//     var comprobante = req.comprobante;
//     commonService.generateXml( comprobante , function( err , xml ) {
//         if( err ) {
//             return callback( err );
//         } else {
//             return callback( null , xml );
//         }
//     } );
// };

module.exports = new FacturaService();