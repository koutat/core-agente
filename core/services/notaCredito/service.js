/**
 * Created by Luis on 18/04/2016.
 */
var NotaCredito             = require( '../../models' ).NotaCredito;
var NotaCreditoItem         = require( '../../models' ).NotaCreditoItem;
// var FacturaExtra        = require( '../../../AronFiles/common/models' ).FacturaExtra;
var sequelize           = require( '../../models' ).sequelize;
var Sequelize           = require( '../../models' ).Sequelize;
var async               = require("async");
var xmlhelper           = require( "../../sunatws/xmlhelper" );
// var pdfComponent        = require("../../../AronFiles/common/pdf");
// var commonService       = require( "../../../AronFiles/components/api/common" );
var utils               = require("../../utils");
var ApiError            = require("../../error/apiError");

var conf                = require("../../configuraciones/appConfig");
function NotaCreditoService(){

}
function crearNotaCredito(datos, callback){
    var facturaReq = datos.body || null;
    var factura = NotaCredito.build( facturaReq , {
                include : [
                    {
                        model : NotaCreditoItem ,
                        as :    "items"
                    }
                ]
            } );
    return callback(null,factura);
}
function guardarNotaCredito(datos , callback ) {
    var transaction = sequelize.transaction(
        function( t ) {
            return datos.save( { transaction : t , validate : false } )
                .then( function( comprobante ) {
                    return comprobante;
                } )
                .catch( function( error ) {
                    throw error;
                } )
        }
    );

    transaction
        .then( function( comprobante ) {
            comprobante.reload()
                .then( function( freloaded ) {
                    return callback( null , freloaded );
                } )
                .catch( function( e ) {
                    throw e;
                } )
        } )
        .catch( function( error ) {
            return callback( error );
        } );
}
function cleanRequest( req , next ) {
    var comprobanteReq = req.body || {};

    if( !comprobanteReq ) {
        return next( null , req );
    }

    var filtered;
    var fieldsParentNotSave = NotaCredito.getFieldsNotSave();

    filtered = utils.cleanObject( comprobanteReq , fieldsParentNotSave );

    var fieldsChildrenNotSave = NotaCreditoItem.getFieldsNotSave();
    var items                 = comprobanteReq.items || [];
    var filteredItems         = [];

    if( Array.isArray( items ) && items.length > 0 ) {

        items.forEach( function( item ) {
            filteredItems.push( utils.cleanObject( item , fieldsChildrenNotSave ) );
        } );
        filtered.items = filteredItems;
    }

    req.body = filtered;
    return next( null , req );
}
function validarFormatoNotaCredito(comprobanteReq , callback ) {

    var error;

    comprobanteReq.validate()
        .then( function( validationError ) {
            if( validationError ) {
                throw validationError;
            }
            return comprobanteReq;
        } )
        .then( function( comprobante ) {
            return Sequelize.Promise.map( comprobante.items , function( item ) {
                return item.validate()
                    .then( function( validError ) {
                        if( validError ) {
                            throw validError;
                        } else {
                            return item;
                        }
                    } )
                    .catch( function( error ) {
                        throw error;
                    } )
            } ).then( function( items ) {
                return comprobante;
            } )
        } )
        .then( function( comprobanteValid ) {

            return callback( null , comprobanteValid );
        } )
        .catch( Sequelize.ValidationError , function( validationError ) {
            error = new ApiError( "3000" , 400 , validationError.errors.shift().message );
            return callback(error);
        } )
        .catch( function( err ) {
            return callback( err );
        } );
}
function construirXml( datos , callback ) {
    NotaCredito.findOneWithAllDataById( datos.idNotaCredito )
        .then( function( facturaToBuild ) {
            facturaToBuild.receptor = JSON.parse(facturaToBuild.receptor);
            xmlhelper.buildXml( facturaToBuild , conf.appConfig.emisor , function( err , xmlObject ) {
                if( err ) {
                    throw err;
                }
                datos.set( "signatureValue" , xmlObject.signatureValue,{save:true} );
                datos.set( "digestValue" , xmlObject.digestValue,{save:false} );
                datos.set("xml",xmlObject.xmlString,{save:true});
                datos.save();

                return callback( null , datos );
            } );
        } )
        .catch( function( error ) {
            return callback( error );
        } );

}
NotaCreditoService.prototype.buscarNoEmitidos = function(callback ) {
    NotaCredito.findAllNoEmitedWithAllData( )
        .then( function( notas ) {
            return callback( null , notas );
        } )
        .catch( function( error ) {
            return callback( error );
        } );
};
NotaCreditoService.prototype.buscarComprobantesDelDia = function(callback ) {
    NotaCredito.findAllTodayReportWithAllData( )
        .then( function( boletas ) {
            return callback( null , boletas );
        } )
        .catch( function( error ) {
            return callback( error );
        } );
}
NotaCreditoService.prototype.validarComprobante = function(req , callback ) {
    async.waterfall( [
        async.constant( req ) ,
        cleanRequest ,
        crearNotaCredito ,
        validarFormatoNotaCredito
    ] , function( err , comprobanteValid ) {
        if( err ) {
            return callback( err,null );
        }
        return callback( null , comprobanteValid );
    } );
};
NotaCreditoService.prototype.saveComprobante = function(datos, callback){
    async.waterfall(
        [
            async.constant( datos ) ,
            guardarNotaCredito,
            construirXml
        ] , function( err , comprobante ) {
            if( err ) {
                return callback( err )
            }
            return callback( null , comprobante.pretty() );
        } );
};
// BoletaService.prototype.generatePdf = function( req , callback ) {
//     var factura = req.comprobante;
//     var emisor  = req.emisor;
//
//     pdfComponent.generatePdf( emisor , factura , function( err , pdfbuffer ) {
//         if( err ) {
//             // var error = new ApiError( 3010 , 500 );
//             // error.setExtra( err.toString() );
//             return callback( error );
//         } else {
//             return callback( null , pdfbuffer );
//         }
//     } );
// };
// BoletaService.prototype.generateXml = function( req , callback ) {
//     var comprobante = req.comprobante;
//     commonService.generateXml( comprobante , function( err , xml ) {
//         if( err ) {
//             return callback( err );
//         } else {
//             return callback( null , xml );
//         }
//     } );
// };

module.exports = new NotaCreditoService();