/**
 * Created by Luis on 18/04/2016.
 */
var NotaDebito              = require( '../../models' ).NotaDebito;
var NotaDebitoItem          = require( '../../models' ).NotaDebitoItem;
var sequelize               = require( '../../models' ).sequelize;
var Sequelize               = require( '../../models' ).Sequelize;
var async                   = require("async");
var xmlhelper               = require( "../../sunatws/xmlhelper" );
var utils                   = require("../../utils");
var ApiError                = require("../../error/apiError");

var conf                = require("../../configuraciones/appConfig");
function NotaDebitoService(){

}
function crearNotaDebito(datos, callback){
    var comprobanteReq = datos.body || null;
    var notaDebito = NotaDebito.build( comprobanteReq , {
                include : [
                    {
                        model : NotaDebitoItem ,
                        as :    "items"
                    }
                ]
            } );
    return callback(null,notaDebito);
}
function guardarNotaDebito(datos , callback ) {
    var transaction = sequelize.transaction(
        function( t ) {
            return datos.save( { transaction : t , validate : false } )
                .then( function( comprobante ) {
                    return comprobante;
                } )
                .catch( function( error ) {
                    throw error;
                } )
        }
    );

    transaction
        .then( function( comprobante ) {
            comprobante.reload()
                .then( function( freloaded ) {
                    return callback( null , freloaded );
                } )
                .catch( function( e ) {
                    throw e;
                } )
        } )
        .catch( function( error ) {
            return callback( error );
        } );
}
function cleanRequest( req , next ) {
    var comprobanteReq = req.body || {};

    if( !comprobanteReq ) {
        return next( null , req );
    }

    var filtered;
    var fieldsParentNotSave = NotaDebito.getFieldsNotSave();

    filtered = utils.cleanObject( comprobanteReq , fieldsParentNotSave );

    var fieldsChildrenNotSave = NotaDebitoItem.getFieldsNotSave();
    var items                 = comprobanteReq.items || [];
    var filteredItems         = [];

    if( Array.isArray( items ) && items.length > 0 ) {

        items.forEach( function( item ) {
            filteredItems.push( utils.cleanObject( item , fieldsChildrenNotSave ) );
        } );
        filtered.items = filteredItems;
    }

    req.body = filtered;
    return next( null , req );
}
function validarFormatoNotaDebito(comprobanteReq , callback ) {

    var error;

    comprobanteReq.validate()
        .then( function( validationError ) {
            if( validationError ) {
                throw validationError;
            }
            return comprobanteReq;
        } )
        .then( function( comprobanteReq ) {
            return Sequelize.Promise.map( comprobanteReq.items , function( item ) {
                return item.validate()
                    .then( function( validError ) {
                        if( validError ) {
                            throw validError;
                        } else {
                            return item;
                        }
                    } )
                    .catch( function( error ) {
                        throw error;
                    } )
            } ).then( function( items ) {
                return comprobanteReq;
            } )
        } )
        .then( function( comprobanteValid ) {

            return callback( null , comprobanteValid );
        } )
        .catch( Sequelize.ValidationError , function( validationError ) {
            // console.log(validationError);
            error = new ApiError( "3000" , 400 , validationError.errors.shift().message );
            return callback(error,null);

        } )
        .catch( function( err ) {
            return callback( err );
        } );
}
function construirXml( datos , callback ) {
    NotaDebito.findOneWithAllDataById( datos.idNotaDebito )
        .then( function( comprobanteToBuild ) {
            comprobanteToBuild.receptor = JSON.parse(comprobanteToBuild.receptor);
            xmlhelper.buildXml( comprobanteToBuild , conf.appConfig.emisor , function( err , xmlObject ) {
                if( err ) {
                    throw err;
                }
                datos.set( "signatureValue" , xmlObject.signatureValue,{save:true} );
                datos.set( "digestValue" , xmlObject.digestValue,{save:false} );
                datos.set("xml",xmlObject.xmlString,{save:true});
                datos.save();

                return callback( null , datos );
            } );
        } )
        .catch( function( error ) {
            return callback( error );
        } );

}
NotaDebitoService.prototype.buscarNoEmitidos = function(callback ) {
    NotaDebito.findAllNoEmitedWithAllData( )
        .then( function( notas ) {
            return callback( null , notas );
        } )
        .catch( function( error ) {
            return callback( error );
        } );
};
NotaDebitoService.prototype.buscarComprobantesDelDia = function(callback ) {
    NotaDebito.findAllTodayReportWithAllData( )
        .then( function( boletas ) {
            return callback( null , boletas );
        } )
        .catch( function( error ) {
            return callback( error );
        } );
};
NotaDebitoService.prototype.validarComprobante = function(req , callback ) {
    async.waterfall( [
        async.constant( req ) ,
        cleanRequest ,
        crearNotaDebito ,
        validarFormatoNotaDebito
    ] , function( err , comprobanteValid ) {
        if( err ) {
            return callback( err,null );
        }
        return callback( null , comprobanteValid );
    } );
};
NotaDebitoService.prototype.saveComprobante = function(datos, callback){
    async.waterfall(
        [
            async.constant( datos ) ,
            guardarNotaDebito,
            construirXml
        ] , function( err , comprobante ) {
            if( err ) {
                return callback( err )
            }
            return callback( null , comprobante.pretty() );
        } );
};
// BoletaService.prototype.generatePdf = function( req , callback ) {
//     var factura = req.comprobante;
//     var emisor  = req.emisor;
//
//     pdfComponent.generatePdf( emisor , factura , function( err , pdfbuffer ) {
//         if( err ) {
//             // var error = new ApiError( 3010 , 500 );
//             // error.setExtra( err.toString() );
//             console.log(err);
//             return callback( error );
//         } else {
//             return callback( null , pdfbuffer );
//         }
//     } );
// };
// BoletaService.prototype.generateXml = function( req , callback ) {
//     var comprobante = req.comprobante;
//     commonService.generateXml( comprobante , function( err , xml ) {
//         if( err ) {
//             return callback( err );
//         } else {
//             return callback( null , xml );
//         }
//     } );
// };

module.exports = new NotaDebitoService();