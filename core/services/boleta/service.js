/**
 * Created by Luis on 18/04/2016.
 */
var Boleta             = require( '../../models' ).Boleta;
var BoletaItem         = require( '../../models' ).BoletaItem;
var sequelize           = require( '../../models' ).sequelize;
var Sequelize           = require( '../../models' ).Sequelize;
var async               = require("async");
var xmlhelper           = require( "../../sunatws/xmlhelper" );
var utils               = require("../../utils");
var ApiError            = require("../../error/apiError");

var conf                = require("../../configuraciones/appConfig");
function BoletaService(){

}
function crearBoleta(datos, callback){
    var boletaReq = datos.body || null;
    var boleta = Boleta.build( boletaReq , {
                include : [
                    {
                        model : BoletaItem ,
                        as :    "items"
                    }
                ]
            } );
    return callback(null,boleta);
}
function guardarBoleta(datos , callback ) {
    var transaction = sequelize.transaction(
        function( t ) {
            return datos.save( { transaction : t , validate : false } )
                .then( function( boleta ) {
                    return boleta;
                } )
                .catch( function( error ) {
                    throw error;
                } )
        }
    );

    transaction
        .then( function( boleta ) {
            boleta.reload()
                .then( function( freloaded ) {
                    return callback( null , freloaded );
                } )
                .catch( function( e ) {
                    throw e;
                } )
        } )
        .catch( function( error ) {
            return callback( error );
        } );
}
function cleanRequest( req , next ) {
    var boleraReq = req.body || {};

    if( !boleraReq ) {
        return next( null , req );
    }

    var filtered;
    var fieldsParentNotSave = Boleta.getFieldsNotSave();

    filtered = utils.cleanObject( boleraReq , fieldsParentNotSave );

    var fieldsChildrenNotSave = BoletaItem.getFieldsNotSave();
    var items                 = boleraReq.items || [];
    var filteredItems         = [];

    if( Array.isArray( items ) && items.length > 0 ) {

        items.forEach( function( item ) {
            filteredItems.push( utils.cleanObject( item , fieldsChildrenNotSave ) );
        } );
        filtered.items = filteredItems;
    }

    req.body = filtered;
    return next( null , req );
}
function validarFormatoBoleta(boletaReq , callback ) {

    var error;

    boletaReq.validate()
        .then( function( validationError ) {
            if( validationError ) {
                throw validationError;
            }
            return boletaReq;
        } )
        .then( function( boletaReq ) {
            return Sequelize.Promise.map( boletaReq.items , function( item ) {
                return item.validate()
                    .then( function( validError ) {
                        if( validError ) {
                            throw validError;
                        } else {
                            return item;
                        }
                    } )
                    .catch( function( error ) {
                        throw error;
                    } )
            } ).then( function( items ) {
                return boletaReq;
            } )
        } )
        .then( function( boletaValid ) {

            return callback( null , boletaValid );
        } )
        .catch( Sequelize.ValidationError , function( validationError ) {
            // console.log(validationError);
            error = new ApiError( "3000" , 400 , validationError.errors.shift().message );
            return callback(error,null);

        } )
        .catch( function( err ) {
            return callback( err );
        } );
}
function construirXml( datos , callback ) {
    Boleta.findOneWithAllDataById( datos.idBoleta )
        .then( function( boletaToBuild ) {
            boletaToBuild.receptor = JSON.parse(boletaToBuild.receptor);
            xmlhelper.buildXml( boletaToBuild , conf.appConfig.emisor , function( err , xmlObject ) {
                if( err ) {
                    throw err;
                }
                datos.set( "signatureValue" , xmlObject.signatureValue,{save:true} );
                datos.set( "digestValue" , xmlObject.digestValue,{save:false} );
                datos.set("xml",xmlObject.xmlString,{save:true});
                datos.save();

                return callback( null , datos );
            } );
        } )
        .catch( function( error ) {
            return callback( error );
        } );

}
BoletaService.prototype.buscarNoEmitidos = function(callback ) {
    Boleta.findAllNoEmitedWithAllData( )
        .then( function( boletas ) {
            return callback( null , boletas );
        } )
        .catch( function( error ) {
            return callback( error );
        } );
};
BoletaService.prototype.buscarComprobantesDelDia = function(callback ) {
    Boleta.findAllTodayReportWithAllData( )
        .then( function( boletas ) {
            return callback( null , boletas );
        } )
        .catch( function( error ) {
            return callback( error );
        } );
}
BoletaService.prototype.validarComprobante= function(req , callback ) {
    async.waterfall( [
        async.constant( req ) ,
        cleanRequest ,
        crearBoleta ,
        validarFormatoBoleta
    ] , function( err , boletaValid ) {
        if( err ) {
            return callback( err,null );
        }
        return callback( null , boletaValid );
    } );
};
BoletaService.prototype.saveComprobante = function(datos, callback){
    async.waterfall(
        [
            async.constant( datos ) ,
            // crearBoleta ,
            guardarBoleta,
            construirXml
        ] , function( err , boleta ) {
            if( err ) {
                return callback( err )
            }
            return callback( null , boleta.pretty() );
        } );
};
// BoletaService.prototype.generatePdf = function( req , callback ) {
//     var factura = req.comprobante;
//     var emisor  = req.emisor;
//
//     pdfComponent.generatePdf( emisor , factura , function( err , pdfbuffer ) {
//         if( err ) {
//             // var error = new ApiError( 3010 , 500 );
//             // error.setExtra( err.toString() );
//             console.log(err);
//             return callback( error );
//         } else {
//             return callback( null , pdfbuffer );
//         }
//     } );
// };
// BoletaService.prototype.generateXml = function( req , callback ) {
//     var comprobante = req.comprobante;
//     commonService.generateXml( comprobante , function( err , xml ) {
//         if( err ) {
//             return callback( err );
//         } else {
//             return callback( null , xml );
//         }
//     } );
// };

module.exports = new BoletaService();