/**
 * Created by Luis on 08/08/2016.
 */
/**
 * Created by Luis on 02/08/2016.
 */
var facturaService                  = require("../services/factura/service");
var boletaService                   = require("../services/boleta/service");
var notasCreditoService             = require("../services/notaCredito/service");
var notasDebitoService              = require("../services/notaDebito/service");
var async                           = require("async");
var Reporter                 = function () {
};
Reporter.prototype.consultar = function (callback) {


    var result = {};
    async.waterfall([
        function (next) {
            facturaService.buscarComprobantesDelDia( function (err, facturas) {
                result.facturas = facturas;
                next(null,result)
            });
        },
        function (result,next) {
            boletaService.buscarComprobantesDelDia(function (err,boletas) {
                result.boletas = boletas;
                next(null,result);
            });

        },
        function (result,next) {
            notasCreditoService.buscarComprobantesDelDia(function (err,notasCredito) {
                result.notasCredito = notasCredito;
                next(null,result);
            });

        },
        function (result,next) {
            notasDebitoService.buscarComprobantesDelDia(function (err,notasDebito) {
                result.notasDebito = notasDebito;
                next(null,result);
            });

        }
    ], function (err,success) {
        if(err)
                return callback(err);
        else return callback(null,JSON.stringify(success));
    });
};
module.exports             = new Reporter();
