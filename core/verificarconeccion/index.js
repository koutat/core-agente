/**
 * Created by Luis on 19/04/2016.
 */
var internetConection = require("is-online");
var Online = require("./online");
var Offline = require("./offline");

Emiter = function(){};
Emiter.prototype.connect = function(req,callback){
    internetConection(function(err,online){
        if(err)   {
            return callback(err,null)
        }
        if(online) {
            
            Offline.emitir(req,function (err,respuesta) {
                if(err) return callback(err,null);
                return callback(null,respuesta);
            })

        }
        else {
            Online.emitir(req,function (err,respuesta) {
                if(err) return callback(err,null);
                return callback(null,respuesta);
            })
        }
    });
};

module.exports = new Emiter();

