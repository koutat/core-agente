var facturaService          = require('../services/factura/service');
var boletaService           = require('../services/boleta/service');
var notaDebitoService       = require('../services/notaDebito/service');
var notaCreditoService      = require('../services/notaCredito/service');
online = function () {};
function switcher(req, callback) {
    switch (req.tipoComprobante){
        case "01":
            callback(null,facturaService);
            break;
        case "03":
            callback(null,boletaService);
            break;
        case "07":
            callback(null,notaCreditoService);
            break;
        case "08":
            callback(null,notaDebitoService);
            break;
    }
}
online.prototype.emitir = function(req,callback){
    switcher(req,function (error, service) {
        service.validarComprobante(req,function (err, facturaReq) {
            if(err) return callback(err,null);
            service.saveComprobante(facturaReq,function (err, respuesta) {
                if(err)
                    callback(err,null);
                return callback(null,respuesta);
            })
        })
    });
};

module.exports = new online();