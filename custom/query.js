var db          = require("./dbClient.json");
var query = {
  facturas:
    "SELECT TOP 10 * FROM ("+
    "SELECT "+

    //"ROW_NUMBER() OVER (ORDER BY getdate()) NUM,"+

    db.campos.idComprobante.idFactura.pk_1+" AS pk_1, "+
    db.campos.idComprobante.idFactura.pk_2+" AS pk_2, "+
    db.campos.idComprobante.idFactura.pk_3+" AS pk_3, "+
    db.campos.idComprobante.idFactura.pk_4+" AS pk_4, "+
    "'01' AS tipoComprobante, "+
    db.campos.idComprobante.idFactura.pk_1+"+''+"+db.campos.idComprobante.idFactura.pk_2+"+''+"+db.campos.idComprobante.idFactura.pk_3+"+''+"+db.campos.idComprobante.idFactura.pk_4+" AS idFactura, "+
    db.campos.required.fechaEmision+" AS fechaEmision, "+
    db.campos.required.serie+" AS serie, "+
    db.campos.required.numero+" AS numero, "+
    db.campos.required.tipoMoneda+" AS moneda, "+
    db.campos.required.totalVenta+" AS totalVenta, "+
    "PER_ID_CLI AS idCliente, "+
    "(SELECT CASE "+db.campos.required.tipoMoneda+
    " WHEN '001' THEN 'PEN'"+
    " WHEN '002' THEN 'USD'"+
    " WHEN '003' THEN 'EUR'"+
    " END) AS tipoMoneda"+
    " FROM Fac.Documentos"+
    " WHERE (("+db.campos.idComprobante.idFactura.pk_1+" = 002 AND "+db.campos.idComprobante.idFactura.pk_2+" = 004)"+
    " OR ("+db.campos.idComprobante.idFactura.pk_1+" = 002 AND "+db.campos.idComprobante.idFactura.pk_2+" = 005))"+
    " AND LEFT(DOC_SERIE_SUNAT,1) = 'F'",
  boletas:
    "SELECT TOP 10 * FROM ("+
    "SELECT "+

    //"ROW_NUMBER() OVER (ORDER BY getdate()) NUM,"+

    db.campos.idComprobante.idBoleta.pk_1+" AS pk_1, "+
    db.campos.idComprobante.idBoleta.pk_2+" AS pk_2, "+
    db.campos.idComprobante.idBoleta.pk_3+" AS pk_3, "+
    db.campos.idComprobante.idBoleta.pk_4+" AS pk_4, "+
    "'03' AS tipoComprobante, "+
    db.campos.idComprobante.idBoleta.pk_1+"+''+"+db.campos.idComprobante.idBoleta.pk_2+"+''+"+db.campos.idComprobante.idBoleta.pk_3+"+''+"+db.campos.idComprobante.idBoleta.pk_4+" AS idBoleta, "+
    db.campos.required.fechaEmision+" AS fechaEmision, "+
    db.campos.required.serie+" AS serie, "+
    db.campos.required.numero+" AS numero, "+
    db.campos.required.tipoMoneda+" AS moneda, "+
    db.campos.required.totalVenta+" AS totalVenta, "+
    "PER_ID_CLI AS idCliente, "+
    "(SELECT CASE "+db.campos.required.tipoMoneda+
    " WHEN '001' THEN 'PEN'"+
    " WHEN '002' THEN 'USD'"+
    " WHEN '003' THEN 'EUR'"+
    " END) AS tipoMoneda"+
    " FROM Fac.Documentos"+
    " WHERE (("+db.campos.idComprobante.idBoleta.pk_1+" = 001 AND "+db.campos.idComprobante.idBoleta.pk_2+"= 001)"+
    " OR ("+db.campos.idComprobante.idBoleta.pk_1+" = 001 AND "+db.campos.idComprobante.idBoleta.pk_2+"= 002))"+
    " AND LEFT(DOC_SERIE_SUNAT,1) = 'B'",
  notasCredito:
    "SELECT TOP 10 * FROM ("+
      "SELECT "+

      //"ROW_NUMBER() OVER (ORDER BY getdate()) NUM,"+

      db.campos.idComprobante.idNotaCredito.pk_1+" AS pk_1, "+
      db.campos.idComprobante.idNotaCredito.pk_2+" AS pk_2, "+
      db.campos.idComprobante.idNotaCredito.pk_3+" AS pk_3, "+
      db.campos.idComprobante.idNotaCredito.pk_4+" AS pk_4, "+
      "TDO_ID_AFE AS tipoComprobanteAfectado_k1, "+
      "DTD_ID_AFE AS tipoComprobanteAfectado_k2, "+
      "DOC_MOT_EMI AS tipoNotaCredito, "+
      "DOC_SERIE_AFE_SUNAT AS serieAfectado, "+
      "DOC_NUMERO_AFE AS numeroAfectado, "+
      "DOC_OBSERVACIONES AS descripcion, "+
      "(SELECT "+db.campos.required.fechaEmision+" FROM Fac.Documentos AS FD1 WHERE FD1.DOC_SERIE = FD2.DOC_SERIE_AFE AND FD1.DOC_NUMERO = FD2.DOC_NUMERO_AFE AND FD1.TDO_ID = FD2.TDO_ID_AFE AND FD1.DTD_ID = FD2.DTD_ID_AFE) AS fechaEmisionAfectado, "+
      "'07' AS tipoComprobante, "+
      db.campos.idComprobante.idNotaCredito.pk_1+"+''+"+db.campos.idComprobante.idNotaCredito.pk_2+"+''+"+db.campos.idComprobante.idNotaCredito.pk_3+"+''+"+db.campos.idComprobante.idNotaCredito.pk_4+" AS idNotaCredito, "+
      db.campos.required.fechaEmision+" AS fechaEmision, "+
      db.campos.required.serie+" AS serie, "+
      db.campos.required.numero+" AS numero, "+
      db.campos.required.tipoMoneda+" AS moneda, "+
      db.campos.required.totalVenta+" AS totalVenta, "+
      "PER_ID_CLI AS idCliente, "+
      "(SELECT CASE "+db.campos.required.tipoMoneda+
      " WHEN '001' THEN 'PEN'"+
      " WHEN '002' THEN 'USD'"+
      " WHEN '003' THEN 'EUR'"+
      " END) AS tipoMoneda"+
      " FROM Fac.Documentos AS FD2"+
      " WHERE ("+db.campos.idComprobante.idNotaCredito.pk_1+" = 008 AND "+db.campos.idComprobante.idNotaCredito.pk_2+" = 017)"+
      " AND (LEFT(DOC_SERIE_SUNAT,1) = 'F' OR LEFT(DOC_SERIE_SUNAT,1) = 'B')",
  notasDebito: 
    "SELECT TOP 10 * FROM ("+
      "SELECT "+

      //"ROW_NUMBER() OVER (ORDER BY getdate()) NUM,"+

      db.campos.idComprobante.idNotaDebito.pk_1+" AS pk_1, "+
      db.campos.idComprobante.idNotaDebito.pk_2+" AS pk_2, "+
      db.campos.idComprobante.idNotaDebito.pk_3+" AS pk_3, "+
      db.campos.idComprobante.idNotaDebito.pk_4+" AS pk_4, "+
      "TDO_ID_AFE AS tipoComprobanteAfectado_k1, "+
      "DTD_ID_AFE AS tipoComprobanteAfectado_k2, "+
      "DOC_MOT_EMI AS tipoNotaDebito, "+
      "DOC_SERIE_AFE_SUNAT AS serieAfectado, "+
      "DOC_NUMERO_AFE AS numeroAfectado, "+
      "DOC_OBSERVACIONES AS descripcion, "+
      "(SELECT "+db.campos.required.fechaEmision+" FROM Fac.Documentos AS FD1 WHERE DOC_SERIE_AFE = DOC_SERIE AND DOC_NUMERO_AFE = DOC_NUMERO AND TDO_ID = TDO_ID_AFE AND DTD_ID = DTD_ID_AFE) AS fechaEmisionAfectado, "+
      "'08' AS tipoComprobante, "+
      db.campos.idComprobante.idNotaDebito.pk_1+"+''+"+db.campos.idComprobante.idNotaDebito.pk_2+"+''+"+db.campos.idComprobante.idNotaDebito.pk_3+"+''+"+db.campos.idComprobante.idNotaDebito.pk_4+" AS idNotaDebito, "+
      db.campos.required.fechaEmision+" AS fechaEmision, "+
      db.campos.required.serie+" AS serie, "+
      db.campos.required.numero+" AS numero, "+
      db.campos.required.tipoMoneda+" AS moneda, "+
      db.campos.required.totalVenta+" AS totalVenta, "+
      "PER_ID_CLI AS idCliente, "+
      "(SELECT CASE "+db.campos.required.tipoMoneda+
      " WHEN '001' THEN 'PEN'"+
      " WHEN '002' THEN 'USD'"+
      " WHEN '003' THEN 'EUR'"+
      " END) AS tipoMoneda"+
      " FROM Fac.Documentos FD2"+
      " WHERE ("+db.campos.idComprobante.idNotaDebito.pk_1+" = 009 AND "+db.campos.idComprobante.idNotaDebito.pk_2+" = 018)"+
      " AND (LEFT(DOC_SERIE_SUNAT,1) = 'F' OR LEFT(DOC_SERIE_SUNAT,1) = 'B')",  
  receptorFactura:
    "SELECT "+
    "RTRIM(PER_APE_PAT +' '+ PER_APE_MAT +' '+PER_NOMBRES) AS razonSocial, "+
    "(SELECT LTRIM(RTRIM(DOP_NUMERO)) FROM mae.DocPersonas WHERE mae.Personas.PER_ID = mae.DocPersonas.PER_ID AND mae.DocPersonas.TDP_ID = '04') AS idReceptor "+
    "FROM mae.Personas "+
    "WHERE PER_ID = ",
  receptorBoleta:
    "SELECT "+
    "RTRIM(PER_APE_PAT +' '+ PER_APE_MAT +' '+PER_NOMBRES) AS razonSocial, "+
    "(SELECT TOP 1 TDP_ID FROM mae.DocPersonas WHERE mae.Personas.PER_ID = mae.DocPersonas.PER_ID AND TDP_ID != '04') AS tipoDocumento, "+
    "(SELECT LTRIM(RTRIM(DOP_NUMERO)) FROM mae.DocPersonas WHERE mae.Personas.PER_ID = mae.DocPersonas.PER_ID AND (mae.DocPersonas.TDP_ID = '01' OR mae.DocPersonas.TDP_ID = '02' OR mae.DocPersonas.TDP_ID = '03' OR mae.DocPersonas.TDP_ID = '05')) AS idReceptor "+
    "FROM mae.Personas "+
    "WHERE PER_ID = ",  
  receptorFacturaNota:
    "SELECT "+
    "RTRIM(PER_APE_PAT +' '+ PER_APE_MAT +' '+PER_NOMBRES) AS razonSocial, "+
    "'04' AS tipoDocumento, "+
    "(SELECT LTRIM(RTRIM(DOP_NUMERO)) FROM mae.DocPersonas WHERE mae.Personas.PER_ID = mae.DocPersonas.PER_ID AND mae.DocPersonas.TDP_ID = '04') AS idReceptor "+
    "FROM mae.Personas "+
    "WHERE PER_ID = ",
   receptorBoletaNota:
     "SELECT "+
    "RTRIM(PER_APE_PAT +' '+ PER_APE_MAT +' '+PER_NOMBRES) AS razonSocial, "+
    "(SELECT TOP 1 TDP_ID FROM mae.DocPersonas WHERE mae.Personas.PER_ID = mae.DocPersonas.PER_ID AND TDP_ID != '04') AS tipoDocumento, "+
    "(SELECT LTRIM(RTRIM(DOP_NUMERO)) FROM mae.DocPersonas WHERE mae.Personas.PER_ID = mae.DocPersonas.PER_ID AND (mae.DocPersonas.TDP_ID = '01' OR mae.DocPersonas.TDP_ID = '02' OR mae.DocPersonas.TDP_ID = '03' OR mae.DocPersonas.TDP_ID = '05')) AS idReceptor "+
    "FROM mae.Personas "+
    "WHERE PER_ID = ", 
  items:
    "SELECT "+
    db.campos.item.idComprobante.idFactura.pk_1+"+''+"+db.campos.item.idComprobante.idFactura.pk_2+"+''+"+db.campos.item.idComprobante.idFactura.pk_3+"+''+"+db.campos.item.idComprobante.idFactura.pk_4+" AS idComprobante, "+
    "(SELECT ART_DESCRIPCION FROM mae.ARTICULOS WHERE Fac.DetalleDocumentos.ART_ID_IMP = mae.ARTICULOS.ART_ID) AS descripcion, "+
    "(SELECT ART_Codigo FROM mae.ARTICULOS WHERE Fac.DetalleDocumentos.ART_ID_IMP = mae.ARTICULOS.ART_ID) AS codigoProducto, "+
    db.campos.item.required.cantidad+" AS cantidad, "+
    "(SELECT (SELECT RTRIM("+db.campos.item.required.unidadMedidaCantidad+") FROM mae.UnidadMedidaArticulos WHERE UM_ID = mae.Articulos.UM_ID) FROM Mae.Articulos WHERE ART_ID = fac.DetalleDocumentos.ART_ID_IMP) AS unidadMedidaCantidad,"+
    db.campos.item.required.precioVentaUnitario+" AS precioVentaUnitario,"+
    db.campos.item.required.montoAfectacionIgv+" AS montoAfectacionIgv,"+
    " DDO_PRE_TOTAL AS totalAnticipo,"+
    " DDO_SERIE_ANT_SUNAT AS serieAnticipo,"+
    " DDO_NUMERO_ANT AS numeroAnticipo"+
    " FROM Fac.DetalleDocumentos "+
    "WHERE ",
}

module.exports = query;
