/**
 * Created by Luis on 08/08/2016.
 */
var express = require('express');
var router = express.Router();
var reporter = require("../core/generarreportes/reporter");

router.get('/', function(req, res, next) {
    reporter.consultar(function (err,respuesta) {
        if(err)
            console.log(err);
        else res.send(respuesta);
    })
});


module.exports = router;
