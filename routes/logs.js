var express = require('express');
var router = express.Router();
var LogReader = require("../core/logger/logsReader");

router.get('/', function(req, res, next) {
  LogReader.consultar(function (err,respuesta) {
    if(err)
        res.send(err);
    else res.send(respuesta);
  })
});


module.exports = router;
